package com.ruoyi.framework.redis

import com.ruoyi.RuoYiApplication
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [RuoYiApplication::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RedisCacheTest {

    @Autowired
    private lateinit var redisCache: RedisCache


    @Test
    fun getCacheObject() {
        val cacheObject = redisCache.getCacheObject("sys_config:sys.account.captchaOnOff")
        println(cacheObject)
    }
}
