package com.ruoyi.project.system.controller

import com.ruoyi.RuoYiApplication
import com.ruoyi.common.utils.uuid.IdUtils
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.web.context.WebApplicationContext
import kotlin.contracts.contract


@WebAppConfiguration
class SysLoginControllerTest {

    private var mockMvc: MockMvc? = null

    @Test
    fun login() {
        val requestBody = "{\"username\": \"admin\", \"password\": \"admin123\"}"
        mockMvc?.perform(
            MockMvcRequestBuilders
                .post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody)
                .accept(MediaType.APPLICATION_JSON)
        )
            ?.andExpect(MockMvcResultMatchers.status().isOk)
            ?.andExpect(MockMvcResultMatchers.content().string("Hello Tom!"))
            ?.andDo(MockMvcResultHandlers.print())
    }

    @Test
    fun getInfo() {
        print(IdUtils.fastUUID())
    }

    @Test
    fun getRouters() {
    }
}
