package com.ruoyi.common.filter

import com.ruoyi.common.constant.Constants
import com.ruoyi.common.utils.http.HttpHelper
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStreamReader
import javax.servlet.ReadListener
import javax.servlet.ServletInputStream
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletRequestWrapper

/**
 * 构建可重复读取inputStream的request
 *
 * @author ruoyi
 */
class RepeatedlyRequestWrapper(request: HttpServletRequest, response: ServletResponse) :
    HttpServletRequestWrapper(request) {
    private val body: ByteArray

    init {
        request.characterEncoding = Constants.UTF8
        response.characterEncoding = Constants.UTF8
        body = HttpHelper.getBodyString(request).toByteArray(charset(Constants.UTF8))
    }

    @Throws(IOException::class)
    override fun getReader(): BufferedReader {
        return BufferedReader(InputStreamReader(inputStream))
    }

    @Throws(IOException::class)
    override fun getInputStream(): ServletInputStream {
        val bais = ByteArrayInputStream(body)
        return object : ServletInputStream() {
            @Throws(IOException::class)
            override fun read(): Int {
                return bais.read()
            }

            @Throws(IOException::class)
            override fun available(): Int {
                return body.size
            }

            override fun isFinished(): Boolean {
                return false
            }

            override fun isReady(): Boolean {
                return false
            }

            override fun setReadListener(readListener: ReadListener) {}
        }
    }
}
