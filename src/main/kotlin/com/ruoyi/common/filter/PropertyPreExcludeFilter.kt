package com.ruoyi.common.filter

import com.alibaba.fastjson2.filter.SimplePropertyPreFilter

/**
 * 排除JSON敏感属性
 *
 * @author ruoyi
 */
class PropertyPreExcludeFilter : SimplePropertyPreFilter() {
    fun addExcludes(vararg filters: String?): PropertyPreExcludeFilter {
        filters.forEach { filter ->
            excludes.add(filter)
        }
        return this
    }
}
