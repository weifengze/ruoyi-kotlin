package com.ruoyi.common.filter

import com.ruoyi.common.utils.html.EscapeUtil
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import java.io.ByteArrayInputStream
import java.io.IOException
import javax.servlet.ReadListener
import javax.servlet.ServletInputStream
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletRequestWrapper

/**
 * XSS过滤处理
 *
 * @author ruoyi
 */
class XssHttpServletRequestWrapper(request: HttpServletRequest?) : HttpServletRequestWrapper(request) {

    override fun getParameterValues(name: String): Array<out String?>? {
        val values = super.getParameterValues(name)
        if (values != null) {
            val length = values.size
            val escapseValues = arrayOfNulls<String>(length)
            for (i in 0 until length) {
                // 防xss攻击和过滤前后空格
                escapseValues[i] = EscapeUtil.clean(values[i]).trim()
            }
            return escapseValues
        }
        return super.getParameterValues(name)
    }

    @Throws(IOException::class)
    override fun getInputStream(): ServletInputStream {
        // 非json类型，直接返回
        if (!isJsonRequest) {
            return super.getInputStream()
        }

        // 为空，直接返回
        var json = IOUtils.toString(super.getInputStream(), "utf-8")
        if (com.ruoyi.common.utils.StringUtils.isEmpty(json)) {
            return super.getInputStream()
        }

        // xss过滤
        json = EscapeUtil.clean(json).trim()
        val jsonBytes = json.toByteArray(charset("utf-8"))
        val bis = ByteArrayInputStream(jsonBytes)
        return object : ServletInputStream() {
            override fun isFinished(): Boolean {
                return true
            }

            override fun isReady(): Boolean {
                return true
            }

            @Throws(IOException::class)
            override fun available(): Int {
                return jsonBytes.size
            }

            override fun setReadListener(readListener: ReadListener) {}

            @Throws(IOException::class)
            override fun read(): Int {
                return bis.read()
            }
        }
    }

    /**
     * 是否是Json请求
     *
     * @param request
     */
    val isJsonRequest: Boolean
        get() {
            val header = super.getHeader(HttpHeaders.CONTENT_TYPE)
            return StringUtils.startsWithIgnoreCase(header, MediaType.APPLICATION_JSON_VALUE)
        }
}
