package com.ruoyi.common.utils

import com.github.pagehelper.PageHelper
import com.ruoyi.common.utils.sql.SqlUtil
import com.ruoyi.framework.web.page.TableSupport

/**
 * 分页工具类
 *
 * @author ruoyi
 */
object PageUtils : PageHelper() {
    /**
     * 设置请求分页数据
     */
    fun startPage() {
        val pageDomain = TableSupport.buildPageRequest()
        val pageNum = pageDomain.pageNum!!
        val pageSize = pageDomain.pageSize!!
        val orderBy = SqlUtil.escapeOrderBySql(pageDomain.orderBy)
        val reasonable = pageDomain.reasonable
        startPage<Any>(pageNum, pageSize, orderBy).reasonable = reasonable
    }

    /**
     * 清理分页的线程变量
     */
    @JvmName("clearPage1")
    fun clearPage() {
        PageHelper.clearPage()
    }
}
