package com.ruoyi.common.utils

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.exception.ExceptionUtils
import java.io.PrintWriter
import java.io.StringWriter
/**
 * 错误信息处理类。
 *
 * @author ruoyi
 */
object ExceptionUtil {
    /**
     * 获取exception的详细错误信息。
     */
    fun getExceptionMessage(e: Throwable): String {
        val sw = StringWriter()
        e.printStackTrace(PrintWriter(sw, true))
        return sw.toString()
    }

    fun getRootErrorMessage(e: Exception?): String {
        var root = ExceptionUtils.getRootCause(e)
        root = root ?: e
        if (root == null) {
            return ""
        }
        val msg = root.message ?: return "null"
        return StringUtils.defaultString(msg)
    }
}
