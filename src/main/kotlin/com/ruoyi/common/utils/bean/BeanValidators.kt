package com.ruoyi.common.utils.bean

import javax.validation.*

/**
 * bean对象属性验证
 *
 * @author ruoyi
 */
object BeanValidators {
    @Throws(ConstraintViolationException::class)
    fun validateWithException(validator: Validator?, `object`: Any, vararg groups: Class<*>?) {
        val constraintViolations = validator!!.validate(`object`, *groups)
        if (constraintViolations.isNotEmpty()) {
            throw ConstraintViolationException(constraintViolations)
        }
    }
}
