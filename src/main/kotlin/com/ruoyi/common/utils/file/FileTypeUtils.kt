package com.ruoyi.common.utils.file

import org.apache.commons.lang3.StringUtils
import java.io.File
import java.util.*

/**
 * 文件类型工具类
 *
 * @author ruoyi
 */
object FileTypeUtils {
    /**
     * 获取文件类型
     *
     *
     * 例如: ruoyi.txt, 返回: txt
     *
     * @param file 文件名
     * @return 后缀（不含".")
     */
    fun getFileType(file: File?): String {
        return if (null == file) StringUtils.EMPTY else getFileType(file.name)
    }

    /**
     * 获取文件类型
     *
     *
     * 例如: ruoyi.txt, 返回: txt
     *
     * @param fileName 文件名
     * @return 后缀（不含".")
     */
    fun getFileType(fileName: String): String {
        val separatorIndex = fileName.lastIndexOf(".")
        return if (separatorIndex < 0) "" else fileName.substring(separatorIndex + 1).lowercase(Locale.getDefault())
    }

    /**
     * 获取文件类型
     *
     * @param photoByte 文件字节码
     * @return 后缀（不含".")
     */
    fun getFileExtendName(photoByte: ByteArray?): String {
        return when {
            (photoByte!![0].toInt() == 71) && (photoByte[1].toInt() == 73) && (photoByte[2].toInt() == 70) && (photoByte[3].toInt() == 56) && ((photoByte[4].toInt() == 55) || (photoByte[4].toInt() == 57)) && (photoByte[5].toInt() == 97) -> "GIF"
            photoByte[6].toInt() == 74 && photoByte[7].toInt() == 70 && photoByte[8].toInt() == 73 && photoByte[9].toInt() == 70 -> "JPG"
            photoByte[0].toInt() == 66 && photoByte[1].toInt() == 77 -> "BMP"
            photoByte[1].toInt() == 80 && photoByte[2].toInt() == 78 && photoByte[3].toInt() == 71 -> "PNG"
            else -> "JPG"
        }
    }
}
