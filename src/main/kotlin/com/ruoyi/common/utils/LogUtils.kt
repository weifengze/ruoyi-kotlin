package com.ruoyi.common.utils

/**
 * 处理并记录日志文件
 *
 * @author ruoyi
 */
object LogUtils {
    fun getBlock(message: Any?): String {
        var msg = message
        if (msg == null) {
            msg = ""
        }
        return "[$msg]"
    }
}
