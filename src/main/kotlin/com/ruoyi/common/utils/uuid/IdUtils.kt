package com.ruoyi.common.utils.uuid

/**
 * ID生成器工具类
 *
 * @author ruoyi
 */
class IdUtils {

    companion object {
        /**
         * 获取随机UUID
         *
         * @return 随机UUID
         */
        fun randomUUID(): String {
            return UUID.fastUUID().toString()
        }

        /**
         * 简化的UUID，去掉了横线
         *
         * @return 简化的UUID，去掉了横线
         */
        fun simpleUUID(): String {
            return UUID.fastUUID().toString(true)
        }

        /**
         * 获取随机UUID，使用性能更好的ThreadLocalRandom生成UUID
         *
         * @return 随机UUID
         */
        fun fastUUID(): String {
            return java.util.UUID.randomUUID().toString()
        }

        /**
         * 简化的UUID，去掉了横线，使用性能更好的ThreadLocalRandom生成UUID
         *
         * @return 简化的UUID，去掉了横线
         */
        fun fastSimpleUUID(): String {
            return UUID.fastUUID().toString(true)
        }
    }
}
