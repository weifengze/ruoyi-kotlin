package com.ruoyi.common.utils.ip

import org.apache.commons.lang3.StringUtils
import java.net.InetAddress
import java.net.UnknownHostException
import javax.servlet.http.HttpServletRequest

/**
 * 获取IP方法
 *
 * @author ruoyi
 */
object IpUtils {
    /**
     * 获取客户端IP
     *
     * @param request 请求对象
     * @return IP地址
     */
    fun getIpAddr(request: HttpServletRequest?): String {
        if (request == null) {
            return "unknown"
        }
        var ip = request.getHeader("x-forwarded-for")
        if (ip == null || ip.isEmpty() || "unknown".equals(ip, ignoreCase = true)) {
            ip = request.getHeader("Proxy-Client-IP")
        }
        if (ip == null || ip.isEmpty() || "unknown".equals(ip, ignoreCase = true)) {
            ip = request.getHeader("X-Forwarded-For")
        }
        if (ip == null || ip.isEmpty() || "unknown".equals(ip, ignoreCase = true)) {
            ip = request.getHeader("WL-Proxy-Client-IP")
        }
        if (ip == null || ip.isEmpty() || "unknown".equals(ip, ignoreCase = true)) {
            ip = request.getHeader("X-Real-IP")
        }
        if (ip == null || ip.isEmpty() || "unknown".equals(ip, ignoreCase = true)) {
            ip = request.remoteAddr
        }
        return if ("0:0:0:0:0:0:0:1" == ip) "127.0.0.1" else getMultistageReverseProxyIp(ip)!!
    }

    /**
     * 检查是否为内部IP地址
     *
     * @param ip IP地址
     * @return 结果
     */
    fun internalIp(ip: String?): Boolean {
        return internalIp(textToNumericFormatV4(ip)) || "127.0.0.1" == ip
    }

    /**
     * 检查是否为内部IP地址
     *
     * @param addr byte地址
     * @return 结果
     */
    private fun internalIp(addr: ByteArray?): Boolean {
        if (com.ruoyi.common.utils.StringUtils.isNull(addr) || addr!!.size < 2) {
            return true
        }
        val b0 = addr[0]
        val b1 = addr[1]
        // 10.x.x.x/8
        val SECTION_1: Byte = 0x0A
        // 172.16.x.x/12
        val SECTION_2 = 0xAC.toByte()
        val SECTION_3 = 0x10.toByte()
        val SECTION_4 = 0x1F.toByte()
        // 192.168.x.x/16
        val SECTION_5 = 0xC0.toByte()
        val SECTION_6 = 0xA8.toByte()
        return when (b0) {
            SECTION_1 -> true
            SECTION_2 -> {
                if (b1 in SECTION_3..SECTION_4) {
                    return true
                }
                when (b1) {
                    SECTION_6 -> return true
                }
                false
            }

            SECTION_5 -> {
                when (b1) {
                    SECTION_6 -> return true
                }
                false
            }

            else -> false
        }
    }

    /**
     * 将IPv4地址转换成字节
     *
     * @param text IPv4地址
     * @return byte 字节
     */
    fun textToNumericFormatV4(text: String?): ByteArray? {
        if (text!!.isEmpty()) {
            return null
        }
        val bytes = ByteArray(4)
        val elements = text.split("\\.".toRegex()).toTypedArray()
        try {
            var l: Long
            var i: Int
            when (elements.size) {
                1 -> {
                    l = elements[0].toLong()
                    if (l < 0L || l > 4294967295L) {
                        return null
                    }
                    bytes[0] = (l shr 24 and 0xFFL).toInt().toByte()
                    bytes[1] = (l and 0xFFFFFFL shr 16 and 0xFFL).toInt().toByte()
                    bytes[2] = (l and 0xFFFFL shr 8 and 0xFFL).toInt().toByte()
                    bytes[3] = (l and 0xFFL).toInt().toByte()
                }

                2 -> {
                    l = elements[0].toInt().toLong()
                    if (l < 0L || l > 255L) {
                        return null
                    }
                    bytes[0] = (l and 0xFFL).toInt().toByte()
                    l = elements[1].toInt().toLong()
                    if (l < 0L || l > 16777215L) {
                        return null
                    }
                    bytes[1] = (l shr 16 and 0xFFL).toInt().toByte()
                    bytes[2] = (l and 0xFFFFL shr 8 and 0xFFL).toInt().toByte()
                    bytes[3] = (l and 0xFFL).toInt().toByte()
                }

                3 -> {
                    i = 0
                    while (i < 2) {
                        l = elements[i].toInt().toLong()
                        if (l < 0L || l > 255L) {
                            return null
                        }
                        bytes[i] = (l and 0xFFL).toInt().toByte()
                        ++i
                    }
                    l = elements[2].toInt().toLong()
                    if (l < 0L || l > 65535L) {
                        return null
                    }
                    bytes[2] = (l shr 8 and 0xFFL).toInt().toByte()
                    bytes[3] = (l and 0xFFL).toInt().toByte()
                }

                4 -> {
                    i = 0
                    while (i < 4) {
                        l = elements[i].toInt().toLong()
                        if (l < 0L || l > 255L) {
                            return null
                        }
                        bytes[i] = (l and 0xFFL).toInt().toByte()
                        ++i
                    }
                }

                else -> return null
            }
        } catch (e: NumberFormatException) {
            return null
        }
        return bytes
    }

    /**
     * 获取IP地址
     *
     * @return 本地IP地址
     */
    val hostIp: String
        get() {
            try {
                return InetAddress.getLocalHost().hostAddress
            } catch (_: UnknownHostException) {
            }
            return "127.0.0.1"
        }

    /**
     * 获取主机名
     *
     * @return 本地主机名
     */
    val hostName: String
        get() {
            try {
                return InetAddress.getLocalHost().hostName
            } catch (_: UnknownHostException) {
            }
            return "未知"
        }

    /**
     * 从多级反向代理中获得第一个非unknown IP地址
     *
     * @param ip 获得的IP地址
     * @return 第一个非unknown IP地址
     */
    fun getMultistageReverseProxyIp(ipaddr: String?): String? {
        // 多级反向代理检测
        var ip = ipaddr
        if (ip != null && ip.indexOf(",") > 0) {
            val ips = ip.trim().split(",".toRegex())
            for (subIp in ips) {
                if (!isUnknown(subIp)) {
                    ip = subIp
                    break
                }
            }
        }
        return ip
    }

    /**
     * 检测给定字符串是否为未知，多用于检测HTTP请求相关
     *
     * @param checkString 被检测的字符串
     * @return 是否未知
     */
    fun isUnknown(checkString: String?): Boolean {
        return StringUtils.isBlank(checkString) || "unknown".equals(checkString, ignoreCase = true)
    }
}
