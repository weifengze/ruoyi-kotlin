package com.ruoyi.common.utils.poi

/**
 * Excel数据格式处理适配器
 *
 * @author ruoyi
 */
interface ExcelHandlerAdapter {
    /**
     * 格式化
     *
     * @param value 单元格数据值
     * @param args excel注解args参数组
     *
     * @return 处理后的值
     */
    fun format(value: Any?, args: Array<String?>?): Any?
}
