package com.ruoyi.common.utils.http

import org.apache.commons.lang3.exception.ExceptionUtils
import org.slf4j.LoggerFactory
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets
import javax.servlet.ServletRequest

/**
 * 通用http工具封装
 *
 * @author ruoyi
 */
object HttpHelper {
    private val LOGGER = LoggerFactory.getLogger(HttpHelper.toString())

    fun getBodyString(request: ServletRequest): String {
        val sb = StringBuilder()
        var reader: BufferedReader? = null
        try {
            request.inputStream.use { inputStream ->
                reader = BufferedReader(InputStreamReader(inputStream, StandardCharsets.UTF_8))
                var line: String?
                while (reader!!.readLine().also { line = it } != null) {
                    sb.append(line)
                }
            }
        } catch (e: IOException) {
            LOGGER.warn("getBodyString出现问题！")
        } finally {
            if (reader != null) {
                try {
                    reader!!.close()
                } catch (e: IOException) {
                    LOGGER.error(ExceptionUtils.getMessage(e))
                }
            }
        }
        return sb.toString()
    }
}
