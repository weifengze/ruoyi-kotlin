package com.ruoyi.common.utils.job

import com.ruoyi.project.monitor.domain.SysJob
import org.quartz.JobExecutionContext

/**
 * 定时任务处理（允许并发执行）
 *
 * @author ruoyi
 */
class QuartzJobExecution : AbstractQuartzJob() {
    @Throws(Exception::class)
    override fun doExecute(context: JobExecutionContext?, sysJob: SysJob) {
        JobInvokeUtil.invokeMethod(sysJob)
    }
}
