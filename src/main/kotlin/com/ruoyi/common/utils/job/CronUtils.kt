package com.ruoyi.common.utils.job

import org.quartz.CronExpression
import java.text.ParseException
import java.util.*

/**
 * cron表达式工具类
 *
 * @author ruoyi
 */
object CronUtils {
    /**
     * 返回一个布尔值代表一个给定的Cron表达式的有效性
     *
     * @param cronExpression Cron表达式
     * @return boolean 表达式是否有效
     */
    fun isValid(cronExpression: String?): Boolean {
        return CronExpression.isValidExpression(cronExpression)
    }

    /**
     * 返回一个字符串值,表示该消息无效Cron表达式给出有效性
     *
     * @param cronExpression Cron表达式
     * @return String 无效时返回表达式错误描述,如果有效返回null
     */
    fun getInvalidMessage(cronExpression: String?): String? {
        return try {
            CronExpression(cronExpression)
            null
        } catch (pe: ParseException) {
            pe.message
        }
    }

    /**
     * 返回下一个执行时间根据给定的Cron表达式
     *
     * @param cronExpression Cron表达式
     * @return Date 下次Cron表达式执行时间
     */
    fun getNextExecution(cronExpression: String?): Date {
        return try {
            val cron = CronExpression(cronExpression)
            cron.getNextValidTimeAfter(Date(System.currentTimeMillis()))
        } catch (e: ParseException) {
            throw IllegalArgumentException(e.message)
        }
    }
}
