package com.ruoyi.common.utils.job

import com.ruoyi.project.monitor.domain.SysJob
import org.quartz.DisallowConcurrentExecution
import org.quartz.JobExecutionContext

/**
 * 定时任务处理（禁止并发执行）
 *
 * @author ruoyi
 */
@DisallowConcurrentExecution
class QuartzDisallowConcurrentExecution : AbstractQuartzJob() {
    @Throws(Exception::class)
    override fun doExecute(context: JobExecutionContext?, sysJob: SysJob) {
        JobInvokeUtil.invokeMethod(sysJob)
    }
}
