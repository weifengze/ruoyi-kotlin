package com.ruoyi.common.utils

import com.alibaba.fastjson2.JSON
import com.ruoyi.common.constant.CacheConstants
import com.ruoyi.common.utils.StringUtils.isNotNull
import com.ruoyi.common.utils.spring.SpringUtils
import com.ruoyi.framework.redis.RedisCache
import com.ruoyi.project.system.domain.SysDictData

/**
 * 字典工具类
 *
 * @author ruoyi
 */
object DictUtils {
    /**
     * 分隔符
     */
    private const val SEPARATOR = ","

    /**
     * 设置字典缓存
     *
     * @param key 参数键
     * @param dictDatas 字典数据列表
     */
    fun setDictCache(key: String, dictDatas: List<SysDictData?>) {
        SpringUtils.getBean(RedisCache::class.java).setCacheObject(getCacheKey(key), dictDatas)
    }

    /**
     * 获取字典缓存
     *
     * @param key 参数键
     * @return dictDatas 字典数据列表
     */
    fun getDictCache(key: String): List<SysDictData>? {
        val arrayCache: Any? = SpringUtils.getBean(RedisCache::class.java).getCacheObject(getCacheKey(key))
        return if (isNotNull(arrayCache)) {
            JSON.parseArray(arrayCache.toString(), SysDictData::class.java).toList()
        } else null
    }

    /**
     * 根据字典类型和字典值获取字典标签
     *
     * @param dictType 字典类型
     * @param dictValue 字典值
     * @return 字典标签
     */
    fun getDictLabel(dictType: String, dictValue: String): String {
        return getDictLabel(dictType, dictValue, SEPARATOR)
    }

    /**
     * 根据字典类型和字典标签获取字典值
     *
     * @param dictType 字典类型
     * @param dictLabel 字典标签
     * @return 字典值
     */
    fun getDictValue(dictType: String, dictLabel: String): String {
        return getDictValue(dictType, dictLabel, SEPARATOR)
    }

    /**
     * 根据字典类型和字典值获取字典标签
     *
     * @param dictType 字典类型
     * @param dictValue 字典值
     * @param separator 分隔符
     * @return 字典标签
     */
    fun getDictLabel(dictType: String, dictValue: String, separator: String): String {
        val propertyString = StringBuilder()
        val datas = getDictCache(dictType)
        if (StringUtils.isNotEmpty(datas)) {
            if (org.apache.commons.lang3.StringUtils.containsAny(separator, dictValue)) {
                for (dict in datas!!) {
                    for (value in dictValue.split(separator.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                        if (value == dict.dictValue) {
                            propertyString.append(dict.dictLabel).append(separator)
                            break
                        }
                    }
                }
            } else {
                for (dict in datas!!) {
                    if (dictValue == dict.dictValue) {
                        return dict.dictLabel!!
                    }
                }
            }
        }
        return org.apache.commons.lang3.StringUtils.stripEnd(propertyString.toString(), separator)
    }

    /**
     * 根据字典类型和字典标签获取字典值
     *
     * @param dictType 字典类型
     * @param dictLabel 字典标签
     * @param separator 分隔符
     * @return 字典值
     */
    fun getDictValue(dictType: String, dictLabel: String, separator: String): String {
        val propertyString = StringBuilder()
        val datas = getDictCache(dictType)
        if (org.apache.commons.lang3.StringUtils.containsAny(separator, dictLabel) && StringUtils.isNotEmpty(datas)) {
            for (dict in datas!!) {
                for (label in dictLabel.split(separator.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                    if (label == dict.dictLabel) {
                        propertyString.append(dict.dictValue).append(separator)
                        break
                    }
                }
            }
        } else {
            for (dict in datas!!) {
                if (dictLabel == dict.dictLabel) {
                    return dict.dictValue!!
                }
            }
        }
        return org.apache.commons.lang3.StringUtils.stripEnd(propertyString.toString(), separator)
    }

    /**
     * 删除指定字典缓存
     *
     * @param key 字典键
     */
    fun removeDictCache(key: String) {
        SpringUtils.getBean(RedisCache::class.java).deleteObject(getCacheKey(key))
    }

    /**
     * 清空字典缓存
     */
    fun clearDictCache() {
        val keys = SpringUtils.getBean(RedisCache::class.java).keys(CacheConstants.SYS_DICT_KEY + "*")
        SpringUtils.getBean(RedisCache::class.java).deleteObject(keys)
    }

    /**
     * 设置cache key
     *
     * @param configKey 参数键
     * @return 缓存键key
     */
    private fun getCacheKey(configKey: String): String {
        return CacheConstants.SYS_DICT_KEY + configKey
    }
}
