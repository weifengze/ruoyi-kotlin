package com.ruoyi.common.exception.base

import com.ruoyi.common.utils.MessageUtils
import com.ruoyi.common.utils.StringUtils

/**
 * 基础异常
 *
 * @author ruoyi
 */
open class BaseException @JvmOverloads constructor(
    /**
     * 所属模块
     */
    val module: String?,
    /**
     * 错误码
     */
    val code: String?,
    /**
     * 错误码对应的参数
     */
    val args: Array<Any?>?,
    /**
     * 错误消息
     */
    val defaultMessage: String? = null
) : RuntimeException() {

    constructor(module: String?, defaultMessage: String?) : this(module, null, null, defaultMessage)
    constructor(code: String?, args: Array<Any?>?) : this(null, code, args, null)
    constructor(defaultMessage: String?) : this(null, null, null, defaultMessage)

    override val message: String
        get() {
            var message: String? = null
            if (!StringUtils.isEmpty(code)) {
                message = MessageUtils.message(code, *args!!)
            }
            if (message == null) {
                message = defaultMessage
            }
            return message!!
        }

    companion object {
        private const val serialVersionUID = 1L
    }
}
