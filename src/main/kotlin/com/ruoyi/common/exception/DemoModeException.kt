package com.ruoyi.common.exception

/**
 * 演示模式异常
 *
 * @author ruoyi
 */
object DemoModeException : RuntimeException() {
    private const val serialVersionUID = 1L
}
