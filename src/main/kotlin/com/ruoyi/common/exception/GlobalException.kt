package com.ruoyi.common.exception

class GlobalException : RuntimeException {
    /**
     * 错误提示
     */
    override var message: String? = null

    /**
     * 错误明细，内部调试错误
     *
     *
     * 和 [CommonResult.getDetailMessage] 一致的设计
     */
    var detailMessage: String? = null
        private set

    /**
     * 空构造方法，避免反序列化问题
     */
    constructor() {}
    constructor(message: String?) {
        this.message = message
    }

    fun setDetailMessage(detailMessage: String?): GlobalException {
        this.detailMessage = detailMessage
        return this
    }

    @JvmName("getMessage1")
    fun getMessage(): String {
        return message!!
    }

    fun setMessage(message: String?): GlobalException {
        this.message = message
        return this
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
