package com.ruoyi.common.exception

/**
 * 工具类异常
 *
 * @author ruoyi
 */
class UtilException : RuntimeException {
    constructor(e: Throwable) : super(e.message, e) {}
    constructor(message: String?) : super(message) {}
    constructor(message: String?, throwable: Throwable?) : super(message, throwable) {}

    companion object {
        private const val serialVersionUID = 8247610319171014183L
    }
}
