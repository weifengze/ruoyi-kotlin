package com.ruoyi.common.exception.file

import com.ruoyi.common.exception.base.BaseException

/**
 * 文件信息异常类
 *
 * @author ruoyi
 */
open class FileException(code: String?, args: Array<Any?>?) : BaseException("file", code, args, null) {
    companion object {
        private const val serialVersionUID = 1L
    }
}
