package com.ruoyi.common.exception.file

import org.apache.commons.fileupload.FileUploadException

/**
 * 文件上传 误异常类
 *
 * @author ruoyi
 */
open class InvalidExtensionException(allowedExtension: Array<String>?, extension: String?, filename: String?) :
    FileUploadException(
        "文件[$filename]后缀[$extension]不正确，请上传" + allowedExtension.contentToString() + "格式"
    ) {

    class InvalidImageExtensionException(allowedExtension: Array<String>?, extension: String?, filename: String?) :
        InvalidExtensionException(allowedExtension, extension, filename) {
        companion object {
            private const val serialVersionUID = 1L
        }
    }

    class InvalidFlashExtensionException(allowedExtension: Array<String>?, extension: String?, filename: String?) :
        InvalidExtensionException(allowedExtension, extension, filename) {
        companion object {
            private const val serialVersionUID = 1L
        }
    }

    class InvalidMediaExtensionException(allowedExtension: Array<String>?, extension: String?, filename: String?) :
        InvalidExtensionException(allowedExtension, extension, filename) {
        companion object {
            private const val serialVersionUID = 1L
        }
    }

    class InvalidVideoExtensionException(allowedExtension: Array<String>?, extension: String?, filename: String?) :
        InvalidExtensionException(allowedExtension, extension, filename) {
        companion object {
            private const val serialVersionUID = 1L
        }
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
