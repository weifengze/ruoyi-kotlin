package com.ruoyi.common.exception.file

/**
 * 文件名大小限制异常类
 *
 * @author ruoyi
 */
class FileSizeLimitExceededException(defaultMaxSize: Long) :
    FileException("upload.exceed.maxSize", arrayOf(arrayOf<Any>(defaultMaxSize))) {
    companion object {
        private const val serialVersionUID = 1L
    }
}
