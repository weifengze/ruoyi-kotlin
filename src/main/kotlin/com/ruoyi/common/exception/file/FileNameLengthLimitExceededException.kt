package com.ruoyi.common.exception.file

/**
 * 文件名称超长限制异常类
 *
 * @author ruoyi
 */
class FileNameLengthLimitExceededException(defaultFileNameLength: Int) :
    FileException("upload.filename.exceed.length", arrayOf(arrayOf<Any>(defaultFileNameLength))) {
    companion object {
        private const val serialVersionUID = 1L
    }
}
