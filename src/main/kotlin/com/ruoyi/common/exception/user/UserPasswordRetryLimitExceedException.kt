package com.ruoyi.common.exception.user

/**
 * 用户错误最大次数异常类
 *
 * @author ruoyi
 */
class UserPasswordRetryLimitExceedException(retryLimitCount: Int, lockTime: Int) :
    UserException("user.password.retry.limit.exceed", arrayOf(arrayOf<Any>(retryLimitCount, lockTime))) {
    companion object {
        private const val serialVersionUID = 1L
    }
}
