package com.ruoyi.common.exception.user

import com.ruoyi.common.exception.base.BaseException

/**
 * 用户信息异常类
 *
 * @author ruoyi
 */
open class UserException(code: String?, args: Array<Any?>?) : BaseException("user", code, args, null) {
    companion object {
        private const val serialVersionUID = 1L
    }
}
