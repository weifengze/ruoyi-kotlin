package com.ruoyi.common.exception.user

/**
 * 验证码失效异常类
 *
 * @author ruoyi
 */
class CaptchaExpireException : UserException("user.jcaptcha.expire", null)
