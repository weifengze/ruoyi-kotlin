package com.ruoyi.common.exception.user

/**
 * 验证码错误异常类
 *
 * @author ruoyi
 */
class CaptchaException : UserException("user.jcaptcha.error", null)
