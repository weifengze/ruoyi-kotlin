package com.ruoyi.common.exception.user

class UserPasswordNotMatchException : UserException("user.password.not.match", null)
