package com.ruoyi.common.exception

/**
 * 业务异常
 *
 * @author ruoyi
 */
class ServiceException : RuntimeException {
    /**
     * 错误码
     */
    var code: Int? = null
        private set

    /**
     * 错误提示
     */
    override var message: String? = null

    /**
     * 错误明细，内部调试错误
     *
     * 和 [CommonResult.getDetailMessage] 一致的设计
     */
    var detailMessage: String? = null
        private set

    /**
     * 空构造方法，避免反序列化问题
     */
    constructor() {}
    constructor(message: String?) {
        this.message = message
    }

    constructor(message: String?, code: Int?) {
        this.message = message
        this.code = code
    }

    @JvmName("getMessage1")
    fun getMessage(): String {
        return message!!
    }

    fun setMessage(message: String?): ServiceException {
        this.message = message
        return this
    }

    fun setDetailMessage(detailMessage: String?): ServiceException {
        this.detailMessage = detailMessage
        return this
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
