package com.ruoyi.common.enums

import org.jetbrains.annotations.Nullable

/**
 * 请求方式
 *
 * @author ruoyi
 */
enum class HttpMethod {
    GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE;

    fun matches(method: String?): Boolean {
        return this == HttpMethod.resolve(method)
    }

    companion object {
        private val mappings = HashMap<String, HttpMethod>(16)

        init {
            values().forEach { v ->
                mappings[v.name] = v
            }
        }

        @Nullable
        fun resolve(@Nullable method: String?): HttpMethod? {
            return if (method != null) mappings[method] else null
        }
    }
}
