package com.ruoyi.common.enums

/**
 * 用户状态
 *
 * @author ruoyi
 */
enum class UserStatus(val code: String, val info: String) {
    OK("0", "正常"), DISABLE("1", "停用"), DELETED("2", "删除");
}
