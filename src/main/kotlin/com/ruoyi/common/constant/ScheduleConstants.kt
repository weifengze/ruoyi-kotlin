package com.ruoyi.common.constant

/**
 * 任务调度通用常量
 *
 * @author ruoyi
 */
object ScheduleConstants {
    const val TASK_CLASS_NAME = "TASK_CLASS_NAME"

    /** 执行目标key  */
    const val TASK_PROPERTIES = "TASK_PROPERTIES"

    /** 默认  */
    const val MISFIRE_DEFAULT = "0"

    /** 立即触发执行  */
    const val MISFIRE_IGNORE_MISFIRES = "1"

    /** 触发一次执行  */
    const val MISFIRE_FIRE_AND_PROCEED = "2"

    /** 不触发立即执行  */
    const val MISFIRE_DO_NOTHING = "3"

    enum class Status(val value: String) {
        /**
         * 正常
         */
        NORMAL("0"),

        /**
         * 暂停
         */
        PAUSE("1");

    }
}
