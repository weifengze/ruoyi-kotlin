package com.ruoyi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.runApplication

/**
 * 启动程序
 *
 * @author ruoyi
 */
@SpringBootApplication(exclude = [DataSourceAutoConfiguration::class])
open class RuoYiApplication

fun main(args: Array<String>) {
    // System.setProperty("spring.devtools.restart.enabled", "false");
    runApplication<RuoYiApplication>(*args)
    println(
        """(♥◠‿◠)ﾉﾞ  RuoYi-Kotlin启动成功   ლ(´ڡ`ლ)ﾞ  
            .-------.       ____     __        
            |  _ _   \      \   \   /  /    
            | ( ' )  |       \  _. /  '       
            |(_ o _) /        _( )_ .'         
            | (_,_).' __  ___(_ o _)'          
            |  |\ \  |  ||   |(_,_)'         
            |  | \ `'   /|   `-'  /           
            |  |  \    /  \      /           
            ''-'   `'-'    `-..-'              
        """
    )
}

