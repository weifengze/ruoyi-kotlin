package com.ruoyi.framework.task

import com.ruoyi.common.utils.StringUtils
import org.springframework.stereotype.Component

/**
 * 定时任务调度测试
 *
 * @author ruoyi
 */
@Component("ryTask")
class RyTask {
    fun ryMultipleParams(s: String?, b: Boolean?, l: Long?, d: Double?, i: Int?) {
        println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i))
    }

    fun ryParams(params: String) {
        println("执行有参方法：$params")
    }

    fun ryNoParams() {
        println("执行无参方法")
    }
}
