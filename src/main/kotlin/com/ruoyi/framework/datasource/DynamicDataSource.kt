package com.ruoyi.framework.datasource

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource


/**
 * 动态数据源
 *
 * @author ruoyi
 */
class DynamicDataSource(defaultTargetDataSource: Any?, targetDataSources: Map<Any, Any>) : AbstractRoutingDataSource() {
    init {
        super.setDefaultTargetDataSource(defaultTargetDataSource!!)
        super.setTargetDataSources(targetDataSources)
        super.afterPropertiesSet()
    }

    override fun determineCurrentLookupKey(): Any? {
        return DynamicDataSourceContextHolder.getDataSourceType()
    }
}
