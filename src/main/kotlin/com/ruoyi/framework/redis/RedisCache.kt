package com.ruoyi.framework.redis

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.redis.core.BoundSetOperations
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.core.ValueOperations
import org.springframework.stereotype.Component
import java.util.concurrent.*

/**
 * spring redis 工具类
 *
 * @author ruoyi
 */
@Component
class RedisCache {

    @Autowired
    private lateinit var redisTemplate: RedisTemplate<Any, Any>

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key 缓存的键值
     * @param value 缓存的值
     */
    fun <T> setCacheObject(key: String, value: T) {
        redisTemplate.opsForValue().set(key, value!!)
    }

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key 缓存的键值
     * @param value 缓存的值
     * @param timeout 时间
     * @param timeUnit 时间颗粒度
     */
    fun <T> setCacheObject(key: String, value: T, timeout: Int, timeUnit: TimeUnit) {
        redisTemplate.opsForValue().set(key, value!!, timeout.toLong(), timeUnit)
    }

    /**
     * 设置有效时间
     *
     * @param key Redis键
     * @param timeout 超时时间
     * @return true=设置成功；false=设置失败
     */
    @JvmOverloads
    fun expire(key: String, timeout: Long, unit: TimeUnit = TimeUnit.SECONDS): Boolean {
        return redisTemplate.expire(key, timeout, unit)
    }

    /**
     * 获取有效时间
     *
     * @param key Redis键
     * @return 有效时间
     */
    fun getExpire(key: String): Long {
        return redisTemplate.getExpire(key)
    }

    /**
     * 判断 key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    fun hasKey(key: String): Boolean {
        return redisTemplate.hasKey(key)
    }

    /**
     * 获得缓存的基本对象。
     *
     * @param key 缓存键值
     * @return 缓存键值对应的数据
     */
    fun getCacheObject(key: String): Any? {
        val operation = redisTemplate.opsForValue()
        return operation[key]
    }

    /**
     * 删除单个对象
     *
     * @param key
     */
    fun deleteObject(key: String): Boolean {
        return redisTemplate.delete(key)
    }

    /**
     * 删除集合对象
     *
     * @param collection 多个对象
     * @return
     */
    fun deleteObject(collection: Collection<*>): Long {
        return redisTemplate.delete(collection)
    }

    /**
     * 缓存List数据
     *
     * @param key 缓存的键值
     * @param dataList 待缓存的List数据
     * @return 缓存的对象
     */
    fun <T> setCacheList(key: String, dataList: List<T>): Long {
        val count = redisTemplate.opsForList().rightPushAll(key, dataList)
        return count ?: 0
    }

    /**
     * 获得缓存的list对象
     *
     * @param key 缓存的键值
     * @return 缓存键值对应的数据
     */
    fun <T> getCacheList(key: String): List<Any>? {
        return redisTemplate.opsForList().range(key, 0, -1)
    }

    /**
     * 缓存Set
     *
     * @param key 缓存键值
     * @param dataSet 缓存的数据
     * @return 缓存数据的对象
     */
    fun <T> setCacheSet(key: String, dataSet: Set<T>): BoundSetOperations<Any, Any> {
        val setOperation: BoundSetOperations<Any, Any> = redisTemplate.boundSetOps(key)
        val it = dataSet.iterator()
        while (it.hasNext()) {
            setOperation.add(it.next())
        }
        return setOperation
    }

    /**
     * 获得缓存的set
     *
     * @param key
     * @return
     */
    fun <T> getCacheSet(key: String): MutableSet<Any>? {
        return redisTemplate.opsForSet().members(key)
    }

    /**
     * 缓存Map
     *
     * @param key
     * @param dataMap
     */
    fun <T> setCacheMap(key: String, dataMap: Map<String?, T>?) {
        if (dataMap != null) {
            redisTemplate.opsForHash<String, T>().putAll(key, dataMap)
        }
    }

    /**
     * 获得缓存的Map
     *
     * @param key
     * @return
     */
    fun <T> getCacheMap(key: String): Map<String, T> {
        return redisTemplate.opsForHash<String, T>().entries(key)
    }

    /**
     * 往Hash中存入数据
     *
     * @param key Redis键
     * @param hKey Hash键
     * @param value 值
     */
    fun <T> setCacheMapValue(key: String, hKey: String, value: T) {
        redisTemplate.opsForHash<String, T>().put(key, hKey, value)
    }

    /**
     * 获取Hash中的数据
     *
     * @param key Redis键
     * @param hKey Hash键
     * @return Hash中的对象
     */
    fun <T> getCacheMapValue(key: String, hKey: String): T? {
        return redisTemplate.opsForHash<String, T>().get(key, hKey)
    }

    /**
     * 删除Hash中的数据
     *
     * @param key
     * @param hKey
     */
    fun delCacheMapValue(key: String, hKey: String) {
        redisTemplate.opsForHash<Any, Any>().delete(key, hKey)
    }

    /**
     * 获取多个Hash中的数据
     *
     * @param key Redis键
     * @param hKeys Hash键集合
     * @return Hash对象集合
     */
    fun <T> getMultiCacheMapValue(key: String, hKeys: Collection<String>): List<Any> {
        return redisTemplate.opsForHash<String, Any>().multiGet(key, hKeys)
    }

    /**
     * 删除Hash中的某条数据
     *
     * @param key Redis键
     * @param hKey Hash键
     * @return 是否成功
     */
    fun deleteCacheMapValue(key: String, hKey: String): Boolean {
        return redisTemplate.opsForHash<String, Any>().delete(key, hKey) > 0
    }

    /**
     * 获得缓存的基本对象列表
     *
     * @param pattern 字符串前缀
     * @return 对象列表
     */
    fun keys(pattern: String): MutableSet<Any> {
        return redisTemplate.keys(pattern)
    }
}
