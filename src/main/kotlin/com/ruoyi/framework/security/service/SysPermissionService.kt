package com.ruoyi.framework.security.service

import com.ruoyi.project.system.domain.SysUser
import com.ruoyi.project.system.service.ISysMenuService
import com.ruoyi.project.system.service.ISysRoleService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * 用户权限处理
 *
 * @author ruoyi
 */
@Component
class SysPermissionService {
    @Autowired
    private val roleService: ISysRoleService? = null

    @Autowired
    private val menuService: ISysMenuService? = null

    /**
     * 获取角色数据权限
     *
     * @param user 用户信息
     * @return 角色权限信息
     */
    fun getRolePermission(user: SysUser): Set<String> {
        val roles: MutableSet<String> = HashSet()
        // 管理员拥有所有权限
        if (user.isAdmin) {
            roles.add("admin")
        } else {
            roles.addAll(roleService!!.selectRolePermissionByUserId(user.userId))
        }
        return roles
    }

    /**
     * 获取菜单数据权限
     *
     * @param user 用户信息
     * @return 菜单权限信息
     */
    fun getMenuPermission(user: SysUser): Set<String> {
        val perms: MutableSet<String> = HashSet()
        // 管理员拥有所有权限
        if (user.isAdmin) {
            perms.add("*:*:*")
        } else {
            val roles = user.roles!!
            if (roles.isNotEmpty() && roles.size > 1) {
                // 多角色设置permissions属性，以便数据权限匹配权限
                roles.forEach { role ->
                    val rolePerms = menuService!!.selectMenuPermsByRoleId(role.roleId)
                    role.permissions = rolePerms
                    perms.addAll(rolePerms)
                }
            } else {
                perms.addAll(menuService!!.selectMenuPermsByUserId(user.userId!!))
            }
        }
        return perms
    }
}
