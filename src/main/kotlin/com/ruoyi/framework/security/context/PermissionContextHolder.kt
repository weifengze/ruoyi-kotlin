package com.ruoyi.framework.security.context

import com.ruoyi.common.core.text.Convert.toStr
import org.springframework.web.context.request.RequestAttributes
import org.springframework.web.context.request.RequestContextHolder

/**
 * 权限信息
 *
 * @author ruoyi
 */
object PermissionContextHolder {
    private const val PERMISSION_CONTEXT_ATTRIBUTES = "PERMISSION_CONTEXT"
    fun setContext(permission: String) {
        RequestContextHolder.currentRequestAttributes().setAttribute(
            PERMISSION_CONTEXT_ATTRIBUTES,
            permission,
            RequestAttributes.SCOPE_REQUEST
        )
    }

    fun getContext(): String? {
        return toStr(
            RequestContextHolder.currentRequestAttributes().getAttribute(
                PERMISSION_CONTEXT_ATTRIBUTES,
                RequestAttributes.SCOPE_REQUEST
            )
        )
    }
}
