package com.ruoyi.framework.security.context

import org.springframework.security.core.Authentication

/**
 * 身份验证信息
 *
 * @author ruoyi
 */
object AuthenticationContextHolder {
    private val contextHolder = ThreadLocal<Authentication>()
    fun getContext(): Authentication {
        return contextHolder.get()
    }

    fun setContext(context: Authentication?) {
        contextHolder.set(context)
    }

    fun clearContext() {
        contextHolder.remove()
    }
}
