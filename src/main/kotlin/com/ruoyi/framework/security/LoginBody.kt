package com.ruoyi.framework.security

/**
 * 用户登录对象
 *
 * @author ruoyi
 */
open class LoginBody {
    /**
     * 用户名
     */
    @get:JvmName("username")
    @set:JvmName("username")
    var username: String? = null

    /**
     * 用户密码
     */
    @get:JvmName("password")
    @set:JvmName("password")
    var password: String? = null

    /**
     * 验证码
     */
    @get:JvmName("code")
    @set:JvmName("code")
    var code: String? = null

    /**
     * 唯一标识
     */
    @get:JvmName("uuid")
    @set:JvmName("uuid")
    var uuid: String? = null

    fun getUsername(): String? {
        return username
    }

    fun setUsername(username: String?) {
        this.username = username
    }

    fun getPassword(): String? {
        return password
    }

    fun setPassword(password: String?) {
        this.password = password
    }

    fun getCode(): String? {
        return code
    }

    fun setCode(code: String?) {
        this.code = code
    }

    fun getUuid(): String? {
        return uuid
    }

    fun setUuid(uuid: String?) {
        this.uuid = uuid
    }
}
