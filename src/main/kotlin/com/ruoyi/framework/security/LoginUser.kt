package com.ruoyi.framework.security

import com.alibaba.fastjson2.annotation.JSONField
import com.ruoyi.project.system.domain.SysUser
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

/**
 * 登录用户身份权限
 *
 * @author ruoyi
 */
class LoginUser : UserDetails {
    /**
     * 用户ID
     */
    @get:JvmName("userId")
    @set:JvmName("userId")
    var userId: Long? = null

    /**
     * 部门ID
     */
    @get:JvmName("deptId")
    @set:JvmName("deptId")
    var deptId: Long? = null

    /**
     * 用户唯一标识
     */
    @get:JvmName("token")
    @set:JvmName("token")
    var token: String? = null

    /**
     * 登录时间
     */
    @get:JvmName("loginTime")
    @set:JvmName("loginTime")
    var loginTime: Long? = null

    /**
     * 过期时间
     */
    @get:JvmName("expireTime")
    @set:JvmName("expireTime")
    var expireTime: Long? = null

    /**
     * 登录IP地址
     */
    @get:JvmName("ipaddr")
    @set:JvmName("ipaddr")
    var ipaddr: String? = null

    /**
     * 登录地点
     */
    @get:JvmName("loginLocation")
    @set:JvmName("loginLocation")
    var loginLocation: String? = null

    /**
     * 浏览器类型
     */
    @get:JvmName("browser")
    @set:JvmName("browser")
    var browser: String? = null

    /**
     * 操作系统
     */
    @get:JvmName("os")
    @set:JvmName("os")
    var os: String? = null

    /**
     * 权限列表
     */
    @get:JvmName("permissions")
    @set:JvmName("permissions")
    var permissions: Set<String>? = null

    /**
     * 用户信息
     */
    @get:JvmName("user")
    @set:JvmName("user")
    var user: SysUser? = null

    fun getUserId(): Long? {
        return userId
    }

    fun setUserId(userId: Long?) {
        this.userId = userId
    }

    fun getDeptId(): Long? {
        return deptId
    }

    fun setDeptId(deptId: Long?) {
        this.deptId = deptId
    }

    fun getToken(): String? {
        return token
    }

    fun setToken(token: String?) {
        this.token = token
    }

    constructor()
    constructor(user: SysUser?, permissions: Set<String>?) {
        this.user = user
        this.permissions = permissions
    }

    constructor(userId: Long?, deptId: Long?, user: SysUser?, permissions: Set<String>?) {
        this.userId = userId
        this.deptId = deptId
        this.user = user
        this.permissions = permissions
    }

    @JSONField(serialize = false)
    override fun getPassword(): String {
        return user!!.password!!
    }

    override fun getUsername(): String {
        return user!!.userName!!
    }

    /**
     * 账户是否未过期,过期无法验证
     */
    @JSONField(serialize = false)
    override fun isAccountNonExpired(): Boolean {
        return true
    }

    /**
     * 指定用户是否解锁,锁定的用户无法进行身份验证
     *
     * @return
     */
    @JSONField(serialize = false)
    override fun isAccountNonLocked(): Boolean {
        return true
    }

    /**
     * 指示是否已过期的用户的凭据(密码),过期的凭据防止认证
     *
     * @return
     */
    @JSONField(serialize = false)
    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    /**
     * 是否可用 ,禁用的用户不能身份验证
     *
     * @return
     */
    @JSONField(serialize = false)
    override fun isEnabled(): Boolean {
        return true
    }

    fun getLoginTime(): Long? {
        return loginTime
    }

    fun setLoginTime(loginTime: Long?) {
        this.loginTime = loginTime
    }

    fun getIpaddr(): String? {
        return ipaddr
    }

    fun setIpaddr(ipaddr: String?) {
        this.ipaddr = ipaddr
    }

    fun getLoginLocation(): String? {
        return loginLocation
    }

    fun setLoginLocation(loginLocation: String?) {
        this.loginLocation = loginLocation
    }

    fun getBrowser(): String? {
        return browser
    }

    fun setBrowser(browser: String?) {
        this.browser = browser
    }

    fun getOs(): String? {
        return os
    }

    fun setOs(os: String?) {
        this.os = os
    }

    fun getExpireTime(): Long? {
        return expireTime
    }

    fun setExpireTime(expireTime: Long?) {
        this.expireTime = expireTime
    }

    fun getPermissions(): Set<String>? {
        return permissions
    }

    fun setPermissions(permissions: Set<String>?) {
        this.permissions = permissions
    }

    fun getUser(): SysUser? {
        return user
    }

    fun setUser(user: SysUser?) {
        this.user = user
    }

    override fun getAuthorities(): Collection<GrantedAuthority?>? {
        return null
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
