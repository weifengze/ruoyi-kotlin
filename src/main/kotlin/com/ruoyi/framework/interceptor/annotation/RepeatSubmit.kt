package com.ruoyi.framework.interceptor.annotation

import java.lang.annotation.Inherited

/**
 * 自定义注解防止表单重复提交
 *
 * @author ruoyi
 */
@Inherited
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@kotlin.annotation.Retention
@MustBeDocumented
annotation class RepeatSubmit(
    /**
     * 间隔时间(ms)，小于此时间视为重复提交
     */
    val interval: Int = 5000,
    /**
     * 提示消息
     */
    val message: String = "不允许重复提交，请稍后再试"
)
