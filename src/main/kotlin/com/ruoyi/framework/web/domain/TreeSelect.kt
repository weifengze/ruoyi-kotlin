package com.ruoyi.framework.web.domain

import com.alibaba.fastjson2.JSON
import com.fasterxml.jackson.annotation.JsonInclude
import com.ruoyi.project.system.domain.SysDept
import com.ruoyi.project.system.domain.SysMenu
import java.io.Serializable
import java.util.stream.Collectors

/**
 * Treeselect树结构实体类
 *
 * @author ruoyi
 */
class TreeSelect : Serializable {
    /** 节点ID  */
    var id: Long? = null

    /** 节点名称  */
    var label: String? = null

    /** 子节点  */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    var children: List<TreeSelect>? = null

    constructor(dept: SysDept) {
        id = dept.deptId
        label = dept.deptName
//        children = dept.children.stream().map { dept: SysDept? -> TreeSelect(dept!!) }.collect(Collectors.toList())
        children = dept.children.map { TreeSelect(it) }.toList()
    }

    constructor(menu: SysMenu) {
        id = menu.menuId
        label = menu.menuName
//        children = menu.children.stream().map { menu: SysMenu? -> TreeSelect(menu!!) }.collect(Collectors.toList())
//        println(JSON.toJSONString(menu.children.stream().map { menu: SysMenu? -> TreeSelect(menu!!) }.collect(Collectors.toList())))
        children = menu.children.map { TreeSelect(it) }.toList()
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
