package com.ruoyi.framework.web.domain

/**
 * Tree基类
 *
 * @author ruoyi
 */
class TreeEntity : BaseEntity() {
    /** 父菜单名称  */
    var parentName: String? = null

    /** 父菜单ID  */
    var parentId: Long? = null

    /** 显示顺序  */
    var orderNum: Int? = null

    /** 祖级列表  */
    var ancestors: String? = null

    /** 子部门  */
    var children: List<*> = ArrayList<Any>()

    companion object {
        private const val serialVersionUID = 1L
    }
}
