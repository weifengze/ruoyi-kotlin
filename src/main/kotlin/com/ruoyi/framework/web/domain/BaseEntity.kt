package com.ruoyi.framework.web.domain

import com.fasterxml.jackson.annotation.JsonFormat
import java.io.Serializable
import java.util.*
import kotlin.collections.HashMap

/**
 * Entity基类
 *
 * @author ruoyi
 */
open class BaseEntity : Serializable {
    /** 搜索值  */
    var searchValue: String? = null

    /** 创建者  */
    var createBy: String? = null

    /** 创建时间  */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    var createTime: Date? = null

    /** 更新者  */
    var updateBy: String? = null

    /** 更新时间  */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    var updateTime: Date? = null

    /** 备注  */
    var remark: String? = null

    /** 请求参数  */
    var params: MutableMap<String, Any>? = null
        get() {
            if (field == null) {
                field = HashMap()
            }
            return field
        }

    companion object {
        private const val serialVersionUID = 1L
    }
}
