package com.ruoyi.framework.web.domain.server

import com.ruoyi.common.utils.Arith

/**
 * CPU相关信息
 *
 * @author ruoyi
 */
class Cpu {
    /**
     * 核心数
     */
    var cpuNum = 0

    /**
     * CPU总的使用率
     */
    var total = 0.0

    /**
     * CPU系统使用率
     */
    var sys = 0.0
        get() = Arith.round(Arith.mul(field / total, 100.0), 2)

    /**
     * CPU用户使用率
     */
    var used = 0.0
        get() = Arith.round(Arith.mul(field / total, 100.0), 2)

    /**
     * CPU当前等待率
     */
    var wait = 0.0
        get() = Arith.round(Arith.mul(field / total, 100.0), 2)

    /**
     * CPU当前空闲率
     */
    var free = 0.0
        get() = Arith.round(Arith.mul(field / total, 100.0), 2)

    @JvmName("getTotal1")
    fun getTotal(): Double {
        return Arith.round(Arith.mul(total, 100.0), 2)
    }

    @JvmName("setTotal1")
    fun setTotal(total: Double) {
        this.total = total
    }
}
