package com.ruoyi.framework.web.domain

import com.ruoyi.common.constant.HttpStatus
import java.io.Serializable

/**
 * 响应信息主体
 *
 * @author ruoyi
 */
class R<T> : Serializable {
    var code = 0
    var msg: String = ""
    var data: Any? = null
        private set

    fun setData(data: T) {
        this.data = data
    }

    companion object {
        private const val serialVersionUID = 1L

        /** 成功  */
        const val SUCCESS = HttpStatus.SUCCESS

        /** 失败  */
        const val FAIL = HttpStatus.ERROR
        fun <T> ok(): R<T> {
            return restResult<T>(null, SUCCESS, "操作成功")
        }

        fun <T> ok(data: T): R<T> {
            return restResult<T>(data, SUCCESS, "操作成功")
        }

        fun <T> ok(data: T, msg: String): R<T> {
            return restResult<T>(data, SUCCESS, msg)
        }

        fun <T> fail(): R<T> {
            return restResult<T>(null, FAIL, "操作失败")
        }

        fun <T> fail(msg: String): R<T> {
            return restResult<T>(null, FAIL, msg)
        }

        fun <T> fail(data: T): R<T> {
            return restResult<T>(data, FAIL, "操作失败")
        }

        fun <T> fail(data: T, msg: String): R<T> {
            return restResult<T>(data, FAIL, msg)
        }

        fun <T> fail(code: Int, msg: String): R<T> {
            return restResult<T>(null, code, msg)
        }

        private fun <T> restResult(data: Any?, code: Int, msg: String): R<T> {
            val apiResult = R<T>()
            apiResult.code = code
            apiResult.data = data
            apiResult.msg = msg
            return apiResult
        }
    }
}
