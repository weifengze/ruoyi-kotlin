package com.ruoyi.framework.web.domain.server

/**
 * 系统相关信息
 *
 * @author ruoyi
 */
class Sys {
    /**
     * 服务器名称
     */
    var computerName: String? = null

    /**
     * 服务器Ip
     */
    var computerIp: String? = null

    /**
     * 项目路径
     */
    var userDir: String? = null

    /**
     * 操作系统
     */
    var osName: String? = null

    /**
     * 系统架构
     */
    var osArch: String? = null
}
