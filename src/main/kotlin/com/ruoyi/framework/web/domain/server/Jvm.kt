package com.ruoyi.framework.web.domain.server

import com.ruoyi.common.utils.Arith
import com.ruoyi.common.utils.DateUtils
import java.lang.management.ManagementFactory

/**
 * JVM相关信息
 *
 * @author ruoyi
 */
class Jvm {
    /**
     * 当前JVM占用的内存总数(M)
     */
    var total = 0.0

    /**
     * JVM最大可用内存总数(M)
     */
    var max = 0.0
        get() = Arith.div(field, (1024 * 1024).toDouble(), 2)

    /**
     * JVM空闲内存(M)
     */
    var free = 0.0

    /**
     * JDK版本
     */
    var version: String? = null

    /**
     * JDK路径
     */
    var home: String? = null

    @JvmName("getTotal1")
    fun getTotal(): Double {
        return Arith.div(total, (1024 * 1024).toDouble(), 2)
    }

    @JvmName("setTotal1")
    fun setTotal(total: Double) {
        this.total = total
    }

    @JvmName("getFree1")
    fun getFree(): Double {
        return Arith.div(free, (1024 * 1024).toDouble(), 2)
    }

    @JvmName("setFree1")
    fun setFree(free: Double) {
        this.free = free
    }

    val used: Double
        get() = Arith.div(total - free, (1024 * 1024).toDouble(), 2)
    val usage: Double
        get() = Arith.mul(Arith.div(total - free, total, 4), 100.0)

    /**
     * 获取JDK名称
     */
    val name: String
        get() = ManagementFactory.getRuntimeMXBean().vmName

    /**
     * JDK启动时间
     */
    val startTime: String
        get() = DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, DateUtils.serverStartDate)

    /**
     * JDK运行时间
     */
    val runTime: String
        get() = DateUtils.getDatePoor(DateUtils.nowDate, DateUtils.serverStartDate)

    /**
     * 运行参数
     */
    val inputArgs: String
        get() = ManagementFactory.getRuntimeMXBean().inputArguments.toString()
}
