package com.ruoyi.framework.web.domain

import com.ruoyi.common.constant.HttpStatus
import com.ruoyi.common.utils.StringUtils

/**
 * 操作消息提醒
 *
 * @author ruoyi
 */
class AjaxResult : HashMap<String?, Any?> {
    /**
     * 初始化一个新创建的 AjaxResult 对象，使其表示一个空消息。
     */
    constructor()

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param msg 返回内容
     */
    constructor(code: Int, msg: String?) {
        super.put(CODE_TAG, code)
        super.put(MSG_TAG, msg!!)
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param msg 返回内容
     * @param data 数据对象
     */
    constructor(code: Int, msg: String?, data: Any?) {
        super.put(CODE_TAG, code)
        super.put(MSG_TAG, msg)
        if (StringUtils.isNotNull(data)) {
            super.put(DATA_TAG, data!!)
        }
    }

    /**
     * 是否为成功消息
     *
     * @return 结果
     */
    val isSuccess: Boolean
        get() = !isError

    /**
     * 是否为错误消息
     *
     * @return 结果
     */
    val isError: Boolean
        get() = HttpStatus.ERROR == this[CODE_TAG]

    /**
     * 方便链式调用
     *
     * @param key 键
     * @param value 值
     * @return 数据对象
     */
    override fun put(key: String?, value: Any?): Any {
        super.put(key, value)
        return this
    }

    companion object {
        private const val serialVersionUID = 1L

        /** 状态码  */
        const val CODE_TAG = "code"

        /** 返回内容  */
        const val MSG_TAG = "msg"

        /** 数据对象  */
        const val DATA_TAG = "data"

        /**
         * 返回成功消息
         *
         * @return 成功消息
         */
        fun success(): AjaxResult {
            return success("操作成功")
        }

        /**
         * 返回成功数据
         *
         * @return 成功消息
         */
        fun success(data: Any?): AjaxResult {
            return success("操作成功", data)
        }

        /**
         * 返回成功消息
         *
         * @param msg 返回内容
         * @return 成功消息
         */
        fun success(msg: String?): AjaxResult {
            return success(msg, null)
        }

        /**
         * 返回成功消息
         *
         * @param msg 返回内容
         * @param data 数据对象
         * @return 成功消息
         */
        fun success(msg: String?, data: Any?): AjaxResult {
            return AjaxResult(HttpStatus.SUCCESS, msg, data)
        }

        /**
         * 返回错误消息
         *
         * @return
         */
        fun error(): AjaxResult {
            return error("操作失败")
        }

        /**
         * 返回错误消息
         *
         * @param msg 返回内容
         * @return 警告消息
         */
        fun error(msg: String?): AjaxResult {
            return error(msg, null)
        }

        /**
         * 返回错误消息
         *
         * @param msg 返回内容
         * @param data 数据对象
         * @return 警告消息
         */
        fun error(msg: String?, data: Any?): AjaxResult {
            return AjaxResult(HttpStatus.ERROR, msg, data)
        }

        /**
         * 返回错误消息
         *
         * @param code 状态码
         * @param msg 返回内容
         * @return 警告消息
         */
        fun error(code: Int, msg: String?): AjaxResult {
            return AjaxResult(code, msg, null)
        }
    }
}
