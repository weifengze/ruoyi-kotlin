package com.ruoyi.framework.web.page

import com.ruoyi.common.utils.StringUtils
import java.lang.Boolean
import kotlin.Int
import kotlin.String
import kotlin.plus

/**
 * 分页数据
 *
 * @author ruoyi
 */
class PageDomain {
    /** 当前记录起始索引  */
    var pageNum: Int? = null

    /** 每页显示记录数  */
    var pageSize: Int? = null

    /** 排序列  */
    var orderByColumn: String? = null

    /** 排序的方向desc或者asc  */
    var isAsc: String? = "asc"
        get() = if (StringUtils.isNotEmpty(field)) {
            // 兼容前端排序类型
            when (field) {
                "ascending" -> {
                    isAsc = "asc"
                }

                "descending" -> {
                    isAsc = "desc"
                }
            }
            "asc"
        } else ""

    /** 分页参数合理化  */
    var reasonable: kotlin.Boolean? = true
        get() = if (StringUtils.isNull(field)) {
            Boolean.TRUE
        } else Boolean.FALSE

    val orderBy: String
        get() = if (StringUtils.isEmpty(orderByColumn)) {
            ""
        } else StringUtils.toUnderScoreCase(orderByColumn) + " " + isAsc

    fun setIsAsc(isAsc: String) {
        var newIsAsc = isAsc
        if (StringUtils.isNotEmpty(newIsAsc)) {
            // 兼容前端排序类型
            if ("ascending" == newIsAsc) {
                newIsAsc = "asc"
            } else if ("descending" == newIsAsc) {
                newIsAsc = "desc"
            }
            this.isAsc = newIsAsc
        }
    }
}
