package com.ruoyi.framework.web.page

import com.ruoyi.common.core.text.Convert
import com.ruoyi.common.utils.ServletUtils

/**
 * 表格数据处理
 *
 * @author ruoyi
 */
object TableSupport {
    /**
     * 当前记录起始索引
     */
    const val PAGE_NUM = "pageNum"

    /**
     * 每页显示记录数
     */
    const val PAGE_SIZE = "pageSize"

    /**
     * 排序列
     */
    const val ORDER_BY_COLUMN = "orderByColumn"

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    const val IS_ASC = "isAsc"

    /**
     * 分页参数合理化
     */
    const val REASONABLE = "reasonable"

    /**
     * 封装分页对象
     */
    val pageDomain: PageDomain
        get() {
            val pageDomain = PageDomain()
            pageDomain.pageNum = Convert.toInt(ServletUtils.getParameter(PAGE_NUM), 1)
            pageDomain.pageSize = Convert.toInt(ServletUtils.getParameter(PAGE_SIZE), 10)
            pageDomain.orderByColumn = ServletUtils.getParameter(ORDER_BY_COLUMN)
            pageDomain.isAsc = ServletUtils.getParameter(IS_ASC)
            pageDomain.reasonable = ServletUtils.getParameterToBool(REASONABLE)
            return pageDomain
        }

    fun buildPageRequest(): PageDomain {
        return pageDomain
    }
}
