package com.ruoyi.framework.web.page

import java.io.Serializable

/**
 * 表格分页数据对象
 *
 * @author ruoyi
 */
class TableDataInfo : Serializable {
    /** 总记录数  */
    var total: Long = 0

    /** 列表数据  */
    var rows: List<*>? = null

    /** 消息状态码  */
    var code = 0

    /** 消息内容  */
    var msg: String? = null

    /**
     * 表格数据对象
     */
    constructor() {}

    /**
     * 分页
     *
     * @param list 列表数据
     * @param total 总记录数
     */
    constructor(list: List<*>, total: Int) {
        rows = list
        this.total = total.toLong()
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
