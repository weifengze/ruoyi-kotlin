package com.ruoyi.framework.web.controller

import com.github.pagehelper.PageHelper
import com.github.pagehelper.PageInfo
import com.ruoyi.common.constant.HttpStatus
import com.ruoyi.common.utils.DateUtils
import com.ruoyi.common.utils.PageUtils
import com.ruoyi.common.utils.SecurityUtils
import com.ruoyi.common.utils.StringUtils
import com.ruoyi.common.utils.sql.SqlUtil
import com.ruoyi.framework.security.LoginUser
import com.ruoyi.framework.web.domain.AjaxResult
import com.ruoyi.framework.web.page.TableDataInfo
import com.ruoyi.framework.web.page.TableSupport
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.InitBinder
import java.beans.PropertyEditorSupport
import java.util.*

/**
 * web层通用数据处理
 *
 * @author ruoyi
 */
open class BaseController {
    protected val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    fun initBinder(binder: WebDataBinder) {
        // Date 类型转换
        binder.registerCustomEditor(Date::class.java, object : PropertyEditorSupport() {
            override fun setAsText(text: String?) {
                value = DateUtils.parseDate(text)
            }
        })
    }

    /**
     * 设置请求分页数据
     */
    protected fun startPage() {
        PageUtils.startPage()
    }

    /**
     * 设置请求排序数据
     */
    protected fun startOrderBy() {
        val pageDomain = TableSupport.buildPageRequest()
        if (StringUtils.isNotEmpty(pageDomain.orderBy)) {
            val orderBy = SqlUtil.escapeOrderBySql(pageDomain.orderBy)
            PageHelper.orderBy(orderBy)
        }
    }

    /**
     * 清理分页的线程变量
     */
    protected fun clearPage() {
        PageUtils.clearPage()
    }

    /**
     * 响应请求分页数据
     */
    protected fun getDataTable(list: List<*>?): TableDataInfo {
        val rspData = TableDataInfo()
        rspData.code = HttpStatus.SUCCESS
        rspData.msg = "查询成功"
        rspData.rows = list
        rspData.total = PageInfo(list).total
        return rspData
    }

    /**
     * 返回成功
     */
    fun success(): AjaxResult {
        return AjaxResult.success()
    }

    /**
     * 返回失败消息
     */
    fun error(): AjaxResult {
        return AjaxResult.error()
    }

    /**
     * 返回成功消息
     */
    fun success(message: String?): AjaxResult {
        return AjaxResult.success(message)
    }

    /**
     * 返回失败消息
     */
    fun error(message: String?): AjaxResult {
        return AjaxResult.error(message)
    }

    /**
     * 响应返回结果
     *
     * @param rows 影响行数
     * @return 操作结果
     */
    protected fun toAjax(rows: Int): AjaxResult {
        return if (rows > 0) AjaxResult.success() else AjaxResult.error()
    }

    /**
     * 响应返回结果
     *
     * @param result 结果
     * @return 操作结果
     */
    protected fun toAjax(result: Boolean): AjaxResult {
        return if (result) success() else error()
    }

    /**
     * 获取用户缓存信息
     */
    val loginUser: LoginUser
        get() = SecurityUtils.getLoginUser()

    /**
     * 获取登录用户id
     */
    val userId: Long?
        get() = loginUser.userId

    /**
     * 获取登录部门id
     */
    val deptId: Long?
        get() = loginUser.deptId

    /**
     * 获取登录用户名
     */
    val username: String
        get() = loginUser.username
}
