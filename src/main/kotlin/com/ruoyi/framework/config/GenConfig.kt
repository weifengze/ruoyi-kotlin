package com.ruoyi.framework.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

/**
 * 读取代码生成相关配置
 *
 * @author ruoyi
 */
@Component
@ConfigurationProperties(prefix = "gen")
open class GenConfig {
    fun setAuthor(author: String) {
        GenConfig.Companion.author = author
    }

    fun setPackageName(packageName: String) {
        GenConfig.Companion.packageName = packageName
    }

    fun setAutoRemovePre(autoRemovePre: Boolean) {
        GenConfig.Companion.autoRemovePre = autoRemovePre
    }

    fun setTablePrefix(tablePrefix: String) {
        GenConfig.Companion.tablePrefix = tablePrefix
    }

    companion object {
        /** 作者  */
        var author: String? = null

        /** 生成包路径  */
        var packageName: String? = null

        /** 自动去除表前缀，默认是true  */
        var autoRemovePre = false

        /** 表前缀(类名不会包含表前缀)  */
        var tablePrefix: String? = null

        @JvmName("getAuthor1")
        fun getAuthor(): String? {
            return author
        }

        @JvmName("getPackageName1")
        fun getPackageName(): String? {
            return packageName
        }

        @JvmName("getAutoRemovePre1")
        fun getAutoRemovePre(): Boolean {
            return autoRemovePre
        }

        @JvmName("getTablePrefix1")
        fun getTablePrefix(): String? {
            return tablePrefix
        }
    }
}
