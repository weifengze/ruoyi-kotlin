package com.ruoyi.framework.config

import com.ruoyi.framework.config.properties.PermitAllUrlProperties
import com.ruoyi.framework.security.filter.JwtAuthenticationTokenFilter
import com.ruoyi.framework.security.handle.AuthenticationEntryPointImpl
import com.ruoyi.framework.security.handle.LogoutSuccessHandlerImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.authentication.logout.LogoutFilter
import org.springframework.web.filter.CorsFilter
import java.util.function.Consumer

/**
 * spring security配置
 *
 * @author ruoyi
 */
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
open class SecurityConfig : WebSecurityConfigurerAdapter() {
    /**
     * 自定义用户认证逻辑
     */
    @Autowired
    private val userDetailsService: UserDetailsService? = null

    /**
     * 认证失败处理类
     */
    @Autowired
    private val unauthorizedHandler: AuthenticationEntryPointImpl? = null

    /**
     * 退出处理类
     */
    @Autowired
    private val logoutSuccessHandler: LogoutSuccessHandlerImpl? = null

    /**
     * token认证过滤器
     */
    @Autowired
    private val authenticationTokenFilter: JwtAuthenticationTokenFilter? = null

    /**
     * 跨域过滤器
     */
    @Autowired
    private val corsFilter: CorsFilter? = null

    /**
     * 允许匿名访问的地址
     */
    @Autowired
    private val permitAllUrl: PermitAllUrlProperties? = null

    /**
     * 解决 无法直接注入 AuthenticationManager
     *
     * @return
     * @throws Exception
     */
    @Bean
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    /**
     * anyRequest          |   匹配所有请求路径
     * access              |   SpringEl表达式结果为true时可以访问
     * anonymous           |   匿名可以访问
     * denyAll             |   用户不能访问
     * fullyAuthenticated  |   用户完全认证可以访问（非remember-me下自动登录）
     * hasAnyAuthority     |   如果有参数，参数表示权限，则其中任何一个权限可以访问
     * hasAnyRole          |   如果有参数，参数表示角色，则其中任何一个角色可以访问
     * hasAuthority        |   如果有参数，参数表示权限，则其权限可以访问
     * hasIpAddress        |   如果有参数，参数表示IP地址，如果用户IP和参数匹配，则可以访问
     * hasRole             |   如果有参数，参数表示角色，则其角色可以访问
     * permitAll           |   用户可以任意访问
     * rememberMe          |   允许通过remember-me登录的用户访问
     * authenticated       |   用户登录后可访问
     */
    @Throws(Exception::class)
    override fun configure(httpSecurity: HttpSecurity) {
        // 注解标记允许匿名访问的url
        val registry = httpSecurity.authorizeRequests()
        permitAllUrl!!.urls.forEach(Consumer { url: String? -> registry.antMatchers(url).permitAll() })
        httpSecurity // CSRF禁用，因为不使用session
            .csrf().disable() // 认证失败处理类
            .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and() // 基于token，所以不需要session
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and() // 过滤请求
            .authorizeRequests() // 对于登录login 注册register 验证码captchaImage 允许匿名访问
            .antMatchers("/login", "/register", "/captchaImage").permitAll() // 静态资源，可匿名访问
            .antMatchers(HttpMethod.GET, "/", "/*.html", "/**/*.html", "/**/*.css", "/**/*.js", "/profile/**")
            .permitAll()
            .antMatchers("/swagger-ui.html", "/swagger-resources/**", "/webjars/**", "/*/api-docs", "/druid/**")
            .permitAll() // 除上面外的所有请求全部需要鉴权认证
            .anyRequest().authenticated()
            .and()
            .headers().frameOptions().disable()
        // 添加Logout filter
        httpSecurity.logout().logoutUrl("/logout").logoutSuccessHandler(logoutSuccessHandler)
        // 添加JWT filter
        httpSecurity.addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter::class.java)
        // 添加CORS filter
        httpSecurity.addFilterBefore(corsFilter, JwtAuthenticationTokenFilter::class.java)
        httpSecurity.addFilterBefore(corsFilter, LogoutFilter::class.java)
    }

    /**
     * 强散列哈希加密实现
     */
    @Bean
    open fun bCryptPasswordEncoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder()
    }

    /**
     * 身份认证接口
     */
    @Throws(Exception::class)
    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder())
    }
}
