package com.ruoyi.framework.config

import com.ruoyi.common.utils.ServletUtils
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest

/**
 * 服务相关配置
 *
 * @author ruoyi
 */
@Component
class ServerConfig {
    /**
     * 获取完整的请求路径，包括：域名，端口，上下文访问路径
     *
     * @return 服务地址
     */
    val url: String
        get() {
            val request = ServletUtils.request
            return getDomain(request)
        }

    companion object {
        fun getDomain(request: HttpServletRequest): String {
            val url = request.requestURL
            val contextPath = request.servletContext.contextPath
            return url.delete(url.length - request.requestURI.length, url.length).append(contextPath).toString()
        }
    }
}
