package com.ruoyi.framework.aspectj.lang.enums

/**
 * 数据源
 *
 * @author ruoyi
 */
enum class DataSourceType {
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
