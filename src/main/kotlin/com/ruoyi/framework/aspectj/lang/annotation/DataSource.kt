package com.ruoyi.framework.aspectj.lang.annotation

import com.ruoyi.framework.aspectj.lang.enums.DataSourceType
import java.lang.annotation.Inherited

/**
 * 自定义多数据源切换注解
 *
 * 优先级：先方法，后类，如果方法覆盖了类上的数据源类型，以方法的为准，否则以类上的为准
 *
 * @author ruoyi
 */
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.CLASS
)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Inherited
annotation class DataSource(
    /**
     * 切换数据源名称
     */
    val value: DataSourceType = DataSourceType.MASTER
)
