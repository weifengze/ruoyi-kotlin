package com.ruoyi.framework.aspectj.lang.annotation

import com.ruoyi.common.utils.poi.ExcelHandlerAdapter
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.ss.usermodel.IndexedColors
import java.math.BigDecimal
import kotlin.reflect.KClass

/**
 * 自定义导出Excel数据注解
 *
 * @author ruoyi
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD)
annotation class Excel(
    /**
     * 导出时在excel中排序
     */
    val sort: Int = Int.MAX_VALUE,
    /**
     * 导出到Excel中的名字.
     */
    val name: String = "",
    /**
     * 日期格式, 如: yyyy-MM-dd
     */
    val dateFormat: String = "",
    /**
     * 如果是字典类型，请设置字典的type值 (如: sys_user_sex)
     */
    val dictType: String = "",
    /**
     * 读取内容转表达式 (如: 0=男,1=女,2=未知)
     */
    val readConverterExp: String = "",
    /**
     * 分隔符，读取字符串组内容
     */
    val separator: String = ",",
    /**
     * BigDecimal 精度 默认:-1(默认不开启BigDecimal格式化)
     */
    val scale: Int = -1,
    /**
     * BigDecimal 舍入规则 默认:BigDecimal.ROUND_HALF_EVEN
     */
    val roundingMode: Int = BigDecimal.ROUND_HALF_EVEN,
    /**
     * 导出时在excel中每个列的高度 单位为字符
     */
    val height: Double = 14.0,
    /**
     * 导出时在excel中每个列的宽 单位为字符
     */
    val width: Double = 16.0,
    /**
     * 文字后缀,如% 90 变成90%
     */
    val suffix: String = "",
    /**
     * 当值为空时,字段的默认值
     */
    val defaultValue: String = "",
    /**
     * 提示信息
     */
    val prompt: String = "",
    /**
     * 设置只能选择不能输入的列内容.
     */
    val combo: Array<String> = [],
    /**
     * 是否需要纵向合并单元格,应对需求:含有list集合单元格)
     */
    val needMerge: Boolean = false,
    /**
     * 是否导出数据,应对需求:有时我们需要导出一份模板,这是标题需要但内容需要用户手工填写.
     */
    val isExport: Boolean = true,
    /**
     * 另一个类中的属性名称,支持多级获取,以小数点隔开
     */
    val targetAttr: String = "",
    /**
     * 是否自动统计数据,在最后追加一行统计数据总和
     */
    val isStatistics: Boolean = false,
    /**
     * 导出类型（0数字 1字符串 2图片）
     */
    val cellType: ColumnType = ColumnType.STRING,
    /**
     * 导出列头背景色
     */
    val headerBackgroundColor: IndexedColors = IndexedColors.GREY_50_PERCENT,
    /**
     * 导出列头字体颜色
     */
    val headerColor: IndexedColors = IndexedColors.WHITE,
    /**
     * 导出单元格背景色
     */
    val backgroundColor: IndexedColors = IndexedColors.WHITE,
    /**
     * 导出单元格字体颜色
     */
    val color: IndexedColors = IndexedColors.BLACK,
    /**
     * 导出字段对齐方式
     */
    val align: HorizontalAlignment = HorizontalAlignment.CENTER,
    /**
     * 自定义数据处理器
     */
    val handler: KClass<*> = ExcelHandlerAdapter::class,
    /**
     * 自定义数据处理器参数
     */
    val args: Array<String> = [],
    /**
     * 字段类型（0：导出导入；1：仅导出；2：仅导入）
     */
    val type: Excel.Type = Excel.Type.ALL
) {
    enum class Type(private val value: Int) {
        ALL(0), EXPORT(1), IMPORT(2);

        fun value(): Int {
            return value
        }
    }

    enum class ColumnType(private val value: Int) {
        NUMERIC(0), STRING(1), IMAGE(2);

        fun value(): Int {
            return value
        }
    }
}
