package com.ruoyi.framework.aspectj.lang.annotation

import com.ruoyi.common.constant.CacheConstants
import com.ruoyi.framework.aspectj.lang.enums.LimitType

/**
 * 限流注解
 *
 * @author ruoyi
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class RateLimiter(
    /**
     * 限流key
     */
    val key: String = CacheConstants.RATE_LIMIT_KEY,
    /**
     * 限流时间,单位秒
     */
    val time: Int = 60,
    /**
     * 限流次数
     */
    val count: Int = 100,
    /**
     * 限流类型
     */
    val limitType: LimitType = LimitType.DEFAULT
)
