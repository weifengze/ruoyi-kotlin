package com.ruoyi.framework.aspectj.lang.annotation

/**
 * Excel注解集
 *
 * @author ruoyi
 */
@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class Excels(vararg val value: Excel)
