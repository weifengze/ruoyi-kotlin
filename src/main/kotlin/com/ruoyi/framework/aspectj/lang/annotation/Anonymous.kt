package com.ruoyi.framework.aspectj.lang.annotation

/**
 * 匿名访问不鉴权注解
 *
 * @author ruoyi
 */
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.CLASS
)
@Retention(
    AnnotationRetention.RUNTIME
)
@MustBeDocumented
annotation class Anonymous
