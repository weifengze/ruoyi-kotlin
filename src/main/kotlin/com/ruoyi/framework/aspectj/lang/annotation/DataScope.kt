package com.ruoyi.framework.aspectj.lang.annotation

/**
 * 数据权限过滤注解
 *
 * @author ruoyi
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class DataScope(
    /**
     * 部门表的别名
     */
    val deptAlias: String = "",
    /**
     * 用户表的别名
     */
    val userAlias: String = "",
    /**
     * 权限字符（如不填默认会自动根据注解获取）
     */
    val permission: String = ""
)
