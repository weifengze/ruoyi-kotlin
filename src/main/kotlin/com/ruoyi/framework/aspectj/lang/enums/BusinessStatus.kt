package com.ruoyi.framework.aspectj.lang.enums

/**
 * 操作状态
 *
 * @author ruoyi
 */
enum class BusinessStatus {
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL
}
