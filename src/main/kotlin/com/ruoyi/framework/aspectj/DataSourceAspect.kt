package com.ruoyi.framework.aspectj

import com.ruoyi.common.utils.*
import com.ruoyi.framework.aspectj.lang.annotation.*
import com.ruoyi.framework.datasource.DynamicDataSourceContextHolder
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.*
import org.aspectj.lang.reflect.MethodSignature
import org.slf4j.LoggerFactory
import org.springframework.core.annotation.AnnotationUtils
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import java.util.*

/**
 * 多数据源处理
 *
 * @author ruoyi
 */
@Aspect
@Order(1)
@Component
open class DataSourceAspect {
    private var logger = LoggerFactory.getLogger(javaClass)

    @Pointcut(
        "@annotation(com.ruoyi.framework.aspectj.lang.annotation.DataSource)"
                + "|| @within(com.ruoyi.framework.aspectj.lang.annotation.DataSource)"
    )
    fun dsPointCut() {
    }

    @Around("dsPointCut()")
    @Throws(Throwable::class)
    fun around(point: ProceedingJoinPoint): Any {
        val dataSource = getDataSource(point)
        if (StringUtils.isNotNull(dataSource)) {
            DynamicDataSourceContextHolder.setDataSourceType(dataSource.value.name)
        }
        return try {
            point.proceed()
        } finally {
            // 销毁数据源 在执行方法之后
            DynamicDataSourceContextHolder.clearDataSourceType()
        }
    }

    /**
     * 获取需要切换的数据源
     */
    private fun getDataSource(point: ProceedingJoinPoint): DataSource {
        val signature = point.signature as MethodSignature
        val dataSource = AnnotationUtils.findAnnotation(signature.method, DataSource::class.java)!!
        return if (Objects.nonNull(dataSource)) {
            dataSource
        } else AnnotationUtils.findAnnotation(signature.declaringType, DataSource::class.java)!!
    }
}
