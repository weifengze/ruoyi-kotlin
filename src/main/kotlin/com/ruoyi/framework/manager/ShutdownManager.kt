package com.ruoyi.framework.manager

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import javax.annotation.PreDestroy

/**
 * 确保应用退出时能关闭后台线程
 *
 * @author ruoyi
 */
@Component
class ShutdownManager {

    companion object {
        private val logger = LoggerFactory.getLogger("sys-user")
    }

    @PreDestroy
    fun destroy() {
        shutdownAsyncManager()
    }

    /**
     * 停止异步执行任务
     */
    private fun shutdownAsyncManager() {
        try {
            logger.info("====关闭后台任务任务线程池====")
            AsyncManager.me().shutdown()
        } catch (e: Exception) {
            logger.error(e.message, e)
        }
    }

}
