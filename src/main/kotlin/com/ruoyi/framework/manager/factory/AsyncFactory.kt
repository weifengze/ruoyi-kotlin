package com.ruoyi.framework.manager.factory

import com.ruoyi.common.constant.*
import com.ruoyi.common.utils.*
import com.ruoyi.common.utils.ip.AddressUtils
import com.ruoyi.common.utils.ip.IpUtils
import com.ruoyi.common.utils.spring.SpringUtils
import com.ruoyi.project.monitor.domain.SysLogininfor
import com.ruoyi.project.monitor.domain.SysOperLog
import com.ruoyi.project.monitor.service.ISysLogininforService
import com.ruoyi.project.monitor.service.ISysOperLogService
import eu.bitwalker.useragentutils.UserAgent
import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import java.util.*

/**
 * 异步工厂（产生任务用）
 *
 * @author ruoyi
 */
object AsyncFactory {
    private val sys_user_logger = LoggerFactory.getLogger("sys-user")

    /**
     * 记录登录信息
     *
     * @param username 用户名
     * @param status 状态
     * @param message 消息
     * @param args 列表
     * @return 任务task
     */
    fun recordLogininfor(
        username: String?, status: String, message: String?,
        vararg args: Any?
    ): TimerTask {
        val userAgent = UserAgent.parseUserAgentString(ServletUtils.request.getHeader("User-Agent"))
        val ip = IpUtils.getIpAddr(ServletUtils.request)
        return object : TimerTask() {
            override fun run() {
                val address = AddressUtils.getRealAddressByIP(ip)
                val s = StringBuilder()
                s.append(LogUtils.getBlock(ip))
                s.append(address)
                s.append(LogUtils.getBlock(username))
                s.append(LogUtils.getBlock(status))
                s.append(LogUtils.getBlock(message))
                // 打印信息到日志
                AsyncFactory.sys_user_logger.info(s.toString(), *args)
                // 获取客户端操作系统
                val os = userAgent.operatingSystem.getName()
                // 获取客户端浏览器
                val browser = userAgent.browser.getName()
                // 封装对象
                val logininfor = SysLogininfor()
                logininfor.userName = username
                logininfor.ipaddr = ip
                logininfor.loginLocation = address
                logininfor.browser = browser
                logininfor.os = os
                logininfor.msg = message
                // 日志状态
                if (StringUtils.equalsAny(status, Constants.LOGIN_SUCCESS, Constants.LOGOUT, Constants.REGISTER)) {
                    logininfor.status = Constants.SUCCESS
                } else if (Constants.LOGIN_FAIL == status) {
                    logininfor.status = Constants.FAIL
                }
                // 插入数据
                SpringUtils.getBean(ISysLogininforService::class.java)
                    .insertLogininfor(logininfor)
            }
        }
    }

    /**
     * 操作日志记录
     *
     * @param operLog 操作日志信息
     * @return 任务task
     */
    fun recordOper(operLog: SysOperLog): TimerTask {
        return object : TimerTask() {
            override fun run() {
                // 远程查询操作地点
                operLog.operLocation = AddressUtils.getRealAddressByIP(operLog.operIp)
                SpringUtils.getBean(ISysOperLogService::class.java).insertOperlog(operLog)
            }
        }
    }
}
