package com.ruoyi.project.system.mapper

import com.ruoyi.project.system.domain.SysDictType
import org.apache.ibatis.annotations.Mapper

/**
 * 字典表 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysDictTypeMapper {
    /**
     * 根据条件分页查询字典类型
     *
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    fun selectDictTypeList(dictType: SysDictType?): List<SysDictType>?

    /**
     * 根据所有字典类型
     *
     * @return 字典类型集合信息
     */
    fun selectDictTypeAll(): List<SysDictType?>?

    /**
     * 根据字典类型ID查询信息
     *
     * @param dictId 字典类型ID
     * @return 字典类型
     */
    fun selectDictTypeById(dictId: Long?): SysDictType?

    /**
     * 根据字典类型查询信息
     *
     * @param dictType 字典类型
     * @return 字典类型
     */
    fun selectDictTypeByType(dictType: String?): SysDictType?

    /**
     * 通过字典ID删除字典信息
     *
     * @param dictId 字典ID
     * @return 结果
     */
    fun deleteDictTypeById(dictId: Long?): Int

    /**
     * 批量删除字典类型信息
     *
     * @param dictIds 需要删除的字典ID
     * @return 结果
     */
    fun deleteDictTypeByIds(dictIds: Array<Long?>?): Int

    /**
     * 新增字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    fun insertDictType(dictType: SysDictType?): Int

    /**
     * 修改字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    fun updateDictType(dictType: SysDictType?): Int

    /**
     * 校验字典类型称是否唯一
     *
     * @param dictType 字典类型
     * @return 结果
     */
    fun checkDictTypeUnique(dictType: String?): SysDictType?
}
