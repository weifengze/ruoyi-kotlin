package com.ruoyi.project.system.mapper

import com.ruoyi.project.system.domain.SysRole
import org.apache.ibatis.annotations.Mapper

/**
 * 角色表 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysRoleMapper {
    /**
     * 根据条件分页查询角色数据
     *
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    fun selectRoleList(role: SysRole?): List<SysRole>?

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    fun selectRolePermissionByUserId(userId: Long?): List<SysRole?>

    /**
     * 查询所有角色
     *
     * @return 角色列表
     */
    fun selectRoleAll(): List<SysRole>?

    /**
     * 根据用户ID获取角色选择框列表
     *
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    fun selectRoleListByUserId(userId: Long?): List<Long?>?

    /**
     * 通过角色ID查询角色
     *
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    fun selectRoleById(roleId: Long?): SysRole?

    /**
     * 根据用户ID查询角色
     *
     * @param userName 用户名
     * @return 角色列表
     */
    fun selectRolesByUserName(userName: String?): List<SysRole?>?

    /**
     * 校验角色名称是否唯一
     *
     * @param roleName 角色名称
     * @return 角色信息
     */
    fun checkRoleNameUnique(roleName: String?): SysRole?

    /**
     * 校验角色权限是否唯一
     *
     * @param roleKey 角色权限
     * @return 角色信息
     */
    fun checkRoleKeyUnique(roleKey: String?): SysRole?

    /**
     * 修改角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    fun updateRole(role: SysRole?): Int

    /**
     * 新增角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    fun insertRole(role: SysRole?): Int

    /**
     * 通过角色ID删除角色
     *
     * @param roleId 角色ID
     * @return 结果
     */
    fun deleteRoleById(roleId: Long?): Int

    /**
     * 批量删除角色信息
     *
     * @param roleIds 需要删除的角色ID
     * @return 结果
     */
    fun deleteRoleByIds(roleIds: Array<Long?>?): Int
}
