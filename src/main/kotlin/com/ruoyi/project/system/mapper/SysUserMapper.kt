package com.ruoyi.project.system.mapper

import com.ruoyi.project.system.domain.SysUser
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param

/**
 * 用户表 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysUserMapper {
    /**
     * 根据条件分页查询用户列表
     *
     * @param sysUser 用户信息
     * @return 用户信息集合信息
     */
    fun selectUserList(sysUser: SysUser?): List<SysUser>?

    /**
     * 根据条件分页查询已配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    fun selectAllocatedList(user: SysUser?): List<SysUser>?

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    fun selectUnallocatedList(user: SysUser?): List<SysUser>?

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    fun selectUserByUserName(userName: String?): SysUser?

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    fun selectUserById(userId: Long?): SysUser?

    /**
     * 新增用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    fun insertUser(user: SysUser?): Int

    /**
     * 修改用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    fun updateUser(user: SysUser?): Int

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    fun updateUserAvatar(@Param("userName") userName: String?, @Param("avatar") avatar: String?): Int

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    fun resetUserPwd(@Param("userName") userName: String?, @Param("password") password: String?): Int

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    fun deleteUserById(userId: Long?): Int

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    fun deleteUserByIds(userIds: Array<Long?>?): Int

    /**
     * 校验用户名称是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    fun checkUserNameUnique(userName: String?): Int

    /**
     * 校验手机号码是否唯一
     *
     * @param phonenumber 手机号码
     * @return 结果
     */
    fun checkPhoneUnique(phonenumber: String?): SysUser?

    /**
     * 校验email是否唯一
     *
     * @param email 用户邮箱
     * @return 结果
     */
    fun checkEmailUnique(email: String?): SysUser?
}
