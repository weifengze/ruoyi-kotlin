package com.ruoyi.project.system.mapper

import com.ruoyi.project.system.domain.SysNotice
import org.apache.ibatis.annotations.Mapper

/**
 * 通知公告表 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysNoticeMapper {
    /**
     * 查询公告信息
     *
     * @param noticeId 公告ID
     * @return 公告信息
     */
    fun selectNoticeById(noticeId: Long?): SysNotice?

    /**
     * 查询公告列表
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    fun selectNoticeList(notice: SysNotice?): List<SysNotice?>?

    /**
     * 新增公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    fun insertNotice(notice: SysNotice?): Int

    /**
     * 修改公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    fun updateNotice(notice: SysNotice?): Int

    /**
     * 批量删除公告
     *
     * @param noticeId 公告ID
     * @return 结果
     */
    fun deleteNoticeById(noticeId: Long?): Int

    /**
     * 批量删除公告信息
     *
     * @param noticeIds 需要删除的公告ID
     * @return 结果
     */
    fun deleteNoticeByIds(noticeIds: Array<Long?>?): Int
}
