package com.ruoyi.project.system.mapper

import com.ruoyi.project.system.domain.SysRoleDept
import org.apache.ibatis.annotations.Mapper

/**
 * 角色与部门关联表 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysRoleDeptMapper {
    /**
     * 通过角色ID删除角色和部门关联
     *
     * @param roleId 角色ID
     * @return 结果
     */
    fun deleteRoleDeptByRoleId(roleId: Long?): Int

    /**
     * 批量删除角色部门关联信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    fun deleteRoleDept(ids: Array<Long?>?): Int

    /**
     * 查询部门使用数量
     *
     * @param deptId 部门ID
     * @return 结果
     */
    fun selectCountRoleDeptByDeptId(deptId: Long?): Int

    /**
     * 批量新增角色部门信息
     *
     * @param roleDeptList 角色部门列表
     * @return 结果
     */
    fun batchRoleDept(roleDeptList: List<SysRoleDept?>?): Int
}
