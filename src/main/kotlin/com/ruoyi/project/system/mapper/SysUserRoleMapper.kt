package com.ruoyi.project.system.mapper

import com.ruoyi.project.system.domain.SysUserRole
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param

/**
 * 用户与角色关联表 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysUserRoleMapper {
    /**
     * 通过用户ID删除用户和角色关联
     *
     * @param userId 用户ID
     * @return 结果
     */
    fun deleteUserRoleByUserId(userId: Long?): Int

    /**
     * 批量删除用户和角色关联
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    fun deleteUserRole(ids: Array<Long?>?): Int

    /**
     * 通过角色ID查询角色使用数量
     *
     * @param roleId 角色ID
     * @return 结果
     */
    fun countUserRoleByRoleId(roleId: Long?): Int

    /**
     * 批量新增用户角色信息
     *
     * @param userRoleList 用户角色列表
     * @return 结果
     */
    fun batchUserRole(userRoleList: List<SysUserRole?>?): Int

    /**
     * 删除用户和角色关联信息
     *
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
    fun deleteUserRoleInfo(userRole: SysUserRole?): Int

    /**
     * 批量取消授权用户角色
     *
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    fun deleteUserRoleInfos(@Param("roleId") roleId: Long?, @Param("userIds") userIds: Array<Long?>?): Int
}
