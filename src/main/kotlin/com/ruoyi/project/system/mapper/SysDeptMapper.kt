package com.ruoyi.project.system.mapper

import com.ruoyi.project.system.domain.SysDept
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param

/**
 * 部门管理 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysDeptMapper {
    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    fun selectDeptList(dept: SysDept?): List<SysDept>

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @param deptCheckStrictly 部门树选择项是否关联显示
     * @return 选中部门列表
     */
    fun selectDeptListByRoleId(
        @Param("roleId") roleId: Long?,
        @Param("deptCheckStrictly") deptCheckStrictly: Boolean
    ): List<Long?>?

    /**
     * 根据部门ID查询信息
     *
     * @param deptId 部门ID
     * @return 部门信息
     */
    fun selectDeptById(deptId: Long?): SysDept?

    /**
     * 根据ID查询所有子部门
     *
     * @param deptId 部门ID
     * @return 部门列表
     */
    fun selectChildrenDeptById(deptId: Long?): List<SysDept?>?

    /**
     * 根据ID查询所有子部门（正常状态）
     *
     * @param deptId 部门ID
     * @return 子部门数
     */
    fun selectNormalChildrenDeptById(deptId: Long?): Int

    /**
     * 是否存在子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    fun hasChildByDeptId(deptId: Long?): Int

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果
     */
    fun checkDeptExistUser(deptId: Long?): Int

    /**
     * 校验部门名称是否唯一
     *
     * @param deptName 部门名称
     * @param parentId 父部门ID
     * @return 结果
     */
    fun checkDeptNameUnique(@Param("deptName") deptName: String?, @Param("parentId") parentId: Long?): SysDept?

    /**
     * 新增部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    fun insertDept(dept: SysDept?): Int

    /**
     * 修改部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    fun updateDept(dept: SysDept?): Int

    /**
     * 修改所在部门正常状态
     *
     * @param deptIds 部门ID组
     */
    fun updateDeptStatusNormal(deptIds: Array<Long?>?)

    /**
     * 修改子元素关系
     *
     * @param depts 子元素
     * @return 结果
     */
    fun updateDeptChildren(@Param("depts") depts: List<SysDept?>?): Int

    /**
     * 删除部门管理信息
     *
     * @param deptId 部门ID
     * @return 结果
     */
    fun deleteDeptById(deptId: Long?): Int
}
