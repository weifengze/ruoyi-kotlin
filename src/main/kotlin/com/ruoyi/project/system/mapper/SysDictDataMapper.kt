package com.ruoyi.project.system.mapper

import com.ruoyi.project.system.domain.SysDictData
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param

/**
 * 字典表 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysDictDataMapper {
    /**
     * 根据条件分页查询字典数据
     *
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    fun selectDictDataList(dictData: SysDictData?): List<SysDictData>

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    fun selectDictDataByType(dictType: String?): List<SysDictData>?

    /**
     * 根据字典类型和字典键值查询字典数据信息
     *
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    fun selectDictLabel(@Param("dictType") dictType: String?, @Param("dictValue") dictValue: String?): String?

    /**
     * 根据字典数据ID查询信息
     *
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    fun selectDictDataById(dictCode: Long?): SysDictData?

    /**
     * 查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据
     */
    fun countDictDataByType(dictType: String?): Int

    /**
     * 通过字典ID删除字典数据信息
     *
     * @param dictCode 字典数据ID
     * @return 结果
     */
    fun deleteDictDataById(dictCode: Long?): Int

    /**
     * 批量删除字典数据信息
     *
     * @param dictCodes 需要删除的字典数据ID
     * @return 结果
     */
    fun deleteDictDataByIds(dictCodes: Array<Long?>?): Int

    /**
     * 新增字典数据信息
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    fun insertDictData(dictData: SysDictData?): Int

    /**
     * 修改字典数据信息
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    fun updateDictData(dictData: SysDictData?): Int

    /**
     * 同步修改字典类型
     *
     * @param oldDictType 旧字典类型
     * @param newDictType 新旧字典类型
     * @return 结果
     */
    fun updateDictDataType(@Param("oldDictType") oldDictType: String?, @Param("newDictType") newDictType: String?): Int
}
