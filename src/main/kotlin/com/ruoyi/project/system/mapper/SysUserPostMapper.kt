package com.ruoyi.project.system.mapper

import com.ruoyi.project.system.domain.SysUserPost
import org.apache.ibatis.annotations.Mapper

/**
 * 用户与岗位关联表 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysUserPostMapper {
    /**
     * 通过用户ID删除用户和岗位关联
     *
     * @param userId 用户ID
     * @return 结果
     */
    fun deleteUserPostByUserId(userId: Long?): Int

    /**
     * 通过岗位ID查询岗位使用数量
     *
     * @param postId 岗位ID
     * @return 结果
     */
    fun countUserPostById(postId: Long?): Int

    /**
     * 批量删除用户和岗位关联
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    fun deleteUserPost(ids: Array<Long?>?): Int

    /**
     * 批量新增用户岗位信息
     *
     * @param userPostList 用户角色列表
     * @return 结果
     */
    fun batchUserPost(userPostList: List<SysUserPost?>?): Int
}
