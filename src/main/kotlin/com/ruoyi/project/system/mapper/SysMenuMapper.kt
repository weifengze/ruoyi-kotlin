package com.ruoyi.project.system.mapper

import com.ruoyi.project.system.domain.SysMenu
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param

/**
 * 菜单表 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysMenuMapper {
    /**
     * 查询系统菜单列表
     *
     * @param menu 菜单信息
     * @return 菜单列表
     */
    fun selectMenuList(menu: SysMenu?): List<SysMenu>?

    /**
     * 根据用户所有权限
     *
     * @return 权限列表
     */
    fun selectMenuPerms(): List<String?>?

    /**
     * 根据用户查询系统菜单列表
     *
     * @param menu 菜单信息
     * @return 菜单列表
     */
    fun selectMenuListByUserId(menu: SysMenu?): List<SysMenu>?

    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    fun selectMenuPermsByUserId(userId: Long?): List<String>?

    /**
     * 根据角色ID查询权限
     *
     * @param roleId 角色ID
     * @return 权限列表
     */
    fun selectMenuPermsByRoleId(roleId: Long?): List<String?>?

    /**
     * 根据用户ID查询菜单
     *
     * @return 菜单列表
     */
    fun selectMenuTreeAll(): List<SysMenu>?

    /**
     * 根据用户ID查询菜单
     *
     * @param username 用户ID
     * @return 菜单列表
     */
    fun selectMenuTreeByUserId(userId: Long?): List<SysMenu>?

    /**
     * 根据角色ID查询菜单树信息
     *
     * @param roleId 角色ID
     * @param menuCheckStrictly 菜单树选择项是否关联显示
     * @return 选中菜单列表
     */
    fun selectMenuListByRoleId(
        @Param("roleId") roleId: Long?,
        @Param("menuCheckStrictly") menuCheckStrictly: Boolean
    ): List<Long?>?

    /**
     * 根据菜单ID查询信息
     *
     * @param menuId 菜单ID
     * @return 菜单信息
     */
    fun selectMenuById(menuId: Long?): SysMenu?

    /**
     * 是否存在菜单子节点
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    fun hasChildByMenuId(menuId: Long?): Int

    /**
     * 新增菜单信息
     *
     * @param menu 菜单信息
     * @return 结果
     */
    fun insertMenu(menu: SysMenu?): Int

    /**
     * 修改菜单信息
     *
     * @param menu 菜单信息
     * @return 结果
     */
    fun updateMenu(menu: SysMenu?): Int

    /**
     * 删除菜单管理信息
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    fun deleteMenuById(menuId: Long?): Int

    /**
     * 校验菜单名称是否唯一
     *
     * @param menuName 菜单名称
     * @param parentId 父菜单ID
     * @return 结果
     */
    fun checkMenuNameUnique(@Param("menuName") menuName: String?, @Param("parentId") parentId: Long?): SysMenu?
}
