package com.ruoyi.project.system.mapper

import com.ruoyi.project.system.domain.SysPost
import org.apache.ibatis.annotations.Mapper

/**
 * 岗位信息 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysPostMapper {
    /**
     * 查询岗位数据集合
     *
     * @param post 岗位信息
     * @return 岗位数据集合
     */
    fun selectPostList(post: SysPost?): List<SysPost>?

    /**
     * 查询所有岗位
     *
     * @return 岗位列表
     */
    fun selectPostAll(): List<SysPost?>?

    /**
     * 通过岗位ID查询岗位信息
     *
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    fun selectPostById(postId: Long?): SysPost?

    /**
     * 根据用户ID获取岗位选择框列表
     *
     * @param userId 用户ID
     * @return 选中岗位ID列表
     */
    fun selectPostListByUserId(userId: Long?): List<Long?>?

    /**
     * 查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    fun selectPostsByUserName(userName: String?): List<SysPost?>?

    /**
     * 删除岗位信息
     *
     * @param postId 岗位ID
     * @return 结果
     */
    fun deletePostById(postId: Long?): Int

    /**
     * 批量删除岗位信息
     *
     * @param postIds 需要删除的岗位ID
     * @return 结果
     */
    fun deletePostByIds(postIds: Array<Long>): Int

    /**
     * 修改岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    fun updatePost(post: SysPost?): Int

    /**
     * 新增岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    fun insertPost(post: SysPost?): Int

    /**
     * 校验岗位名称
     *
     * @param postName 岗位名称
     * @return 结果
     */
    fun checkPostNameUnique(postName: String?): SysPost?

    /**
     * 校验岗位编码
     *
     * @param postCode 岗位编码
     * @return 结果
     */
    fun checkPostCodeUnique(postCode: String?): SysPost?
}
