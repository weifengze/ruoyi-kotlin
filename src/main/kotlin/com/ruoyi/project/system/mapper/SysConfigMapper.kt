package com.ruoyi.project.system.mapper

import com.ruoyi.project.system.domain.SysConfig
import org.apache.ibatis.annotations.Mapper

/**
 * 参数配置 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysConfigMapper {
    /**
     * 查询参数配置信息
     *
     * @param config 参数配置信息
     * @return 参数配置信息
     */
    fun selectConfig(config: SysConfig?): SysConfig?

    /**
     * 查询参数配置列表
     *
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    fun selectConfigList(config: SysConfig?): List<SysConfig>?

    /**
     * 根据键名查询参数配置信息
     *
     * @param configKey 参数键名
     * @return 参数配置信息
     */
    fun checkConfigKeyUnique(configKey: String?): SysConfig?

    /**
     * 新增参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    fun insertConfig(config: SysConfig?): Int

    /**
     * 修改参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    fun updateConfig(config: SysConfig?): Int

    /**
     * 删除参数配置
     *
     * @param configId 参数ID
     * @return 结果
     */
    fun deleteConfigById(configId: Long?): Int

    /**
     * 批量删除参数信息
     *
     * @param configIds 需要删除的参数ID
     * @return 结果
     */
    fun deleteConfigByIds(configIds: Array<Long?>?): Int
}
