package com.ruoyi.project.system.controller

import com.ruoyi.common.utils.*
import com.ruoyi.common.utils.poi.ExcelUtil
import com.ruoyi.framework.aspectj.lang.annotation.*
import com.ruoyi.framework.aspectj.lang.enums.BusinessType
import com.ruoyi.framework.web.controller.BaseController
import com.ruoyi.framework.web.domain.AjaxResult
import com.ruoyi.framework.web.page.TableDataInfo
import com.ruoyi.project.system.domain.SysDictData
import com.ruoyi.project.system.service.ISysDictDataService
import com.ruoyi.project.system.service.ISysDictTypeService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse

/**
 * 数据字典信息
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/dict/data")
open class SysDictDataController : BaseController() {

    @Autowired
    private lateinit var dictDataService: ISysDictDataService

    @Autowired
    private lateinit var dictTypeService: ISysDictTypeService

    @PreAuthorize("@ss.hasPermi('system:dict:list')")
    @GetMapping("/list")
    fun list(dictData: SysDictData?): TableDataInfo {
        startPage()
        return getDataTable(dictDataService.selectDictDataList(dictData))
    }

    @Log(title = "字典数据", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('system:dict:export')")
    @PostMapping("/export")
    fun export(response: HttpServletResponse, dictData: SysDictData?) {
        val list = dictDataService.selectDictDataList(dictData)
        val util = ExcelUtil(SysDictData::class.java)
        util.exportExcel(response, list, "字典数据")
    }

    /**
     * 查询字典数据详细
     */
    @PreAuthorize("@ss.hasPermi('system:dict:query')")
    @GetMapping(value = ["/{dictCode}"])
    fun getInfo(@PathVariable dictCode: Long?): AjaxResult {
        return AjaxResult.success(dictDataService.selectDictDataById(dictCode))
    }

    /**
     * 根据字典类型查询字典数据信息
     */
    @GetMapping(value = ["/type/{dictType}"])
    fun dictType(@PathVariable dictType: String): AjaxResult {
        val data = dictTypeService.selectDictDataByType(dictType) ?: ArrayList()
        return AjaxResult.success(data)
    }

    /**
     * 新增字典类型
     */
    @PreAuthorize("@ss.hasPermi('system:dict:add')")
    @Log(title = "字典数据", businessType = BusinessType.INSERT)
    @PostMapping
    fun add(@Validated @RequestBody dict: SysDictData): AjaxResult {
        dict.createBy = username
        return toAjax(dictDataService.insertDictData(dict))
    }

    /**
     * 修改保存字典类型
     */
    @PreAuthorize("@ss.hasPermi('system:dict:edit')")
    @Log(title = "字典数据", businessType = BusinessType.UPDATE)
    @PutMapping
    fun edit(@Validated @RequestBody dict: SysDictData): AjaxResult {
        dict.updateBy = username
        return toAjax(dictDataService.updateDictData(dict))
    }

    /**
     * 删除字典类型
     */
    @PreAuthorize("@ss.hasPermi('system:dict:remove')")
    @Log(title = "字典类型", businessType = BusinessType.DELETE)
    @DeleteMapping("/{dictCodes}")
    fun remove(@PathVariable dictCodes: Array<Long>): AjaxResult {
        dictDataService.deleteDictDataByIds(dictCodes)
        return success()
    }
}
