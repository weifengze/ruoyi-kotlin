package com.ruoyi.project.system.controller

import com.ruoyi.common.constant.UserConstants
import com.ruoyi.common.utils.StringUtils
import com.ruoyi.framework.aspectj.lang.annotation.Log
import com.ruoyi.framework.aspectj.lang.enums.BusinessType
import com.ruoyi.framework.web.controller.BaseController
import com.ruoyi.framework.web.domain.AjaxResult
import com.ruoyi.project.system.domain.SysMenu
import com.ruoyi.project.system.service.ISysMenuService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

/**
 * 菜单信息
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/menu")
open class SysMenuController : BaseController() {
    @Autowired
    private val menuService: ISysMenuService? = null

    /**
     * 获取菜单列表
     */
    @PreAuthorize("@ss.hasPermi('system:menu:list')")
    @GetMapping("/list")
    fun list(menu: SysMenu?): AjaxResult {
        val menus = menuService!!.selectMenuList(menu, userId)
        return AjaxResult.success(menus)
    }

    /**
     * 根据菜单编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:menu:query')")
    @GetMapping(value = ["/{menuId}"])
    fun getInfo(@PathVariable menuId: Long?): AjaxResult {
        return AjaxResult.success(menuService!!.selectMenuById(menuId))
    }

    /**
     * 获取菜单下拉树列表
     */
    @GetMapping("/treeselect")
    fun treeselect(menu: SysMenu?): AjaxResult {
        val menus = menuService!!.selectMenuList(menu, userId)
        return AjaxResult.success(menuService.buildMenuTreeSelect(menus))
    }

    /**
     * 加载对应角色菜单列表树
     */
    @GetMapping(value = ["/roleMenuTreeselect/{roleId}"])
    fun roleMenuTreeselect(@PathVariable("roleId") roleId: Long?): AjaxResult {
        val menus = menuService!!.selectMenuList(userId)
        val ajax: AjaxResult = AjaxResult.success()
        ajax["checkedKeys"] = menuService.selectMenuListByRoleId(roleId)
        ajax["menus"] = menuService.buildMenuTreeSelect(menus)
        return ajax
    }

    /**
     * 新增菜单
     */
    @PreAuthorize("@ss.hasPermi('system:menu:add')")
    @Log(title = "菜单管理", businessType = BusinessType.INSERT)
    @PostMapping
    fun add(@Validated @RequestBody menu: SysMenu): AjaxResult {
        if (UserConstants.NOT_UNIQUE == menuService!!.checkMenuNameUnique(menu)) {
            return AjaxResult.Companion.error("新增菜单'" + menu.menuName + "'失败，菜单名称已存在")
        } else if (UserConstants.YES_FRAME == menu.isFrame && !StringUtils.ishttp(menu.path)) {
            return AjaxResult.Companion.error("新增菜单'" + menu.menuName + "'失败，地址必须以http(s)://开头")
        }
        menu.createBy = username
        return toAjax(menuService.insertMenu(menu))
    }

    /**
     * 修改菜单
     */
    @PreAuthorize("@ss.hasPermi('system:menu:edit')")
    @Log(title = "菜单管理", businessType = BusinessType.UPDATE)
    @PutMapping
    fun edit(@Validated @RequestBody menu: SysMenu): AjaxResult {
        if (UserConstants.NOT_UNIQUE == menuService!!.checkMenuNameUnique(menu)) {
            return AjaxResult.Companion.error("修改菜单'" + menu.menuName + "'失败，菜单名称已存在")
        } else if (UserConstants.YES_FRAME == menu.isFrame && !StringUtils.ishttp(menu.path)) {
            return AjaxResult.Companion.error("修改菜单'" + menu.menuName + "'失败，地址必须以http(s)://开头")
        } else if (menu.menuId == menu.parentId) {
            return AjaxResult.Companion.error("修改菜单'" + menu.menuName + "'失败，上级菜单不能选择自己")
        }
        menu.updateBy = username
        return toAjax(menuService.updateMenu(menu))
    }

    /**
     * 删除菜单
     */
    @PreAuthorize("@ss.hasPermi('system:menu:remove')")
    @Log(title = "菜单管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{menuId}")
    fun remove(@PathVariable("menuId") menuId: Long?): AjaxResult {
        if (menuService!!.hasChildByMenuId(menuId)) {
            return AjaxResult.Companion.error("存在子菜单,不允许删除")
        }
        return if (menuService.checkMenuExistRole(menuId)) {
            AjaxResult.Companion.error("菜单已分配,不允许删除")
        } else toAjax(menuService.deleteMenuById(menuId))
    }
}
