package com.ruoyi.project.system.controller

import com.ruoyi.common.constant.UserConstants
import com.ruoyi.common.utils.poi.ExcelUtil
import com.ruoyi.framework.aspectj.lang.annotation.*
import com.ruoyi.framework.aspectj.lang.enums.BusinessType
import com.ruoyi.framework.web.controller.BaseController
import com.ruoyi.framework.web.domain.AjaxResult
import com.ruoyi.framework.web.page.TableDataInfo
import com.ruoyi.project.system.domain.SysConfig
import com.ruoyi.project.system.service.ISysConfigService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse

/**
 * 参数配置 信息操作处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/config")
open class SysConfigController : BaseController() {
    @Autowired
    private val configService: ISysConfigService? = null

    /**
     * 获取参数配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:config:list')")
    @GetMapping("/list")
    fun list(config: SysConfig?): TableDataInfo {
        startPage()
        val list = configService!!.selectConfigList(config)
        return getDataTable(list)
    }

    @Log(title = "参数管理", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('system:config:export')")
    @PostMapping("/export")
    fun export(response: HttpServletResponse, config: SysConfig?) {
        val list = configService!!.selectConfigList(config)
        val util = ExcelUtil(SysConfig::class.java)
        util.exportExcel(response, list, "参数数据")
    }

    /**
     * 根据参数编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:config:query')")
    @GetMapping(value = ["/{configId}"])
    fun getInfo(@PathVariable configId: Long?): AjaxResult {
        return AjaxResult.Companion.success(configService!!.selectConfigById(configId))
    }

    /**
     * 根据参数键名查询参数值
     */
    @GetMapping(value = ["/configKey/{configKey}"])
    fun getConfigKey(@PathVariable configKey: String): AjaxResult {
        return AjaxResult.success(configService!!.selectConfigByKey(configKey))
    }

    /**
     * 新增参数配置
     */
    @PreAuthorize("@ss.hasPermi('system:config:add')")
    @Log(title = "参数管理", businessType = BusinessType.INSERT)
    @PostMapping
    fun add(@Validated @RequestBody config: SysConfig): AjaxResult {
        if (UserConstants.NOT_UNIQUE == configService!!.checkConfigKeyUnique(config)) {
            return AjaxResult.error("新增参数'" + config.configName + "'失败，参数键名已存在")
        }
        config.createBy = username
        return toAjax(configService.insertConfig(config))
    }

    /**
     * 修改参数配置
     */
    @PreAuthorize("@ss.hasPermi('system:config:edit')")
    @Log(title = "参数管理", businessType = BusinessType.UPDATE)
    @PutMapping
    fun edit(@Validated @RequestBody config: SysConfig): AjaxResult {
        if (UserConstants.NOT_UNIQUE == configService!!.checkConfigKeyUnique(config)) {
            return AjaxResult.Companion.error("修改参数'" + config.configName + "'失败，参数键名已存在")
        }
        config.updateBy = username
        return toAjax(configService.updateConfig(config))
    }

    /**
     * 删除参数配置
     */
    @PreAuthorize("@ss.hasPermi('system:config:remove')")
    @Log(title = "参数管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{configIds}")
    fun remove(@PathVariable configIds: Array<Long?>): AjaxResult {
        configService!!.deleteConfigByIds(configIds)
        return success()
    }

    /**
     * 刷新参数缓存
     */
    @PreAuthorize("@ss.hasPermi('system:config:remove')")
    @Log(title = "参数管理", businessType = BusinessType.CLEAN)
    @DeleteMapping("/refreshCache")
    fun refreshCache(): AjaxResult {
        configService!!.resetConfigCache()
        return AjaxResult.success()
    }
}
