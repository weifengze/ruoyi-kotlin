package com.ruoyi.project.system.controller

import com.ruoyi.framework.aspectj.lang.annotation.Log
import com.ruoyi.framework.aspectj.lang.enums.BusinessType
import com.ruoyi.framework.web.controller.BaseController
import com.ruoyi.framework.web.domain.AjaxResult
import com.ruoyi.framework.web.page.TableDataInfo
import com.ruoyi.project.system.domain.SysNotice
import com.ruoyi.project.system.service.ISysNoticeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

/**
 * 公告 信息操作处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/notice")
open class SysNoticeController : BaseController() {
    @Autowired
    private val noticeService: ISysNoticeService? = null

    /**
     * 获取通知公告列表
     */
    @PreAuthorize("@ss.hasPermi('system:notice:list')")
    @GetMapping("/list")
    fun list(notice: SysNotice?): TableDataInfo {
        startPage()
        val list = noticeService!!.selectNoticeList(notice)
        return getDataTable(list)
    }

    /**
     * 根据通知公告编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:notice:query')")
    @GetMapping(value = ["/{noticeId}"])
    fun getInfo(@PathVariable noticeId: Long?): AjaxResult {
        return AjaxResult.Companion.success(noticeService!!.selectNoticeById(noticeId))
    }

    /**
     * 新增通知公告
     */
    @PreAuthorize("@ss.hasPermi('system:notice:add')")
    @Log(title = "通知公告", businessType = BusinessType.INSERT)
    @PostMapping
    fun add(@Validated @RequestBody notice: SysNotice): AjaxResult {
        notice.createBy = username
        return toAjax(noticeService!!.insertNotice(notice))
    }

    /**
     * 修改通知公告
     */
    @PreAuthorize("@ss.hasPermi('system:notice:edit')")
    @Log(title = "通知公告", businessType = BusinessType.UPDATE)
    @PutMapping
    fun edit(@Validated @RequestBody notice: SysNotice): AjaxResult {
        notice.updateBy = username
        return toAjax(noticeService!!.updateNotice(notice))
    }

    /**
     * 删除通知公告
     */
    @PreAuthorize("@ss.hasPermi('system:notice:remove')")
    @Log(title = "通知公告", businessType = BusinessType.DELETE)
    @DeleteMapping("/{noticeIds}")
    fun remove(@PathVariable noticeIds: Array<Long?>?): AjaxResult {
        return toAjax(noticeService!!.deleteNoticeByIds(noticeIds))
    }
}
