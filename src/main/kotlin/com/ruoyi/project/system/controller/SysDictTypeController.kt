package com.ruoyi.project.system.controller

import com.ruoyi.common.constant.UserConstants
import com.ruoyi.common.utils.poi.ExcelUtil
import com.ruoyi.framework.aspectj.lang.annotation.*
import com.ruoyi.framework.aspectj.lang.enums.BusinessType
import com.ruoyi.framework.web.controller.BaseController
import com.ruoyi.framework.web.domain.AjaxResult
import com.ruoyi.framework.web.page.TableDataInfo
import com.ruoyi.project.system.domain.SysDictType
import com.ruoyi.project.system.service.ISysDictTypeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse

/**
 * 数据字典信息
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/dict/type")
open class SysDictTypeController : BaseController() {
    @Autowired
    private val dictTypeService: ISysDictTypeService? = null

    @PreAuthorize("@ss.hasPermi('system:dict:list')")
    @GetMapping("/list")
    fun list(dictType: SysDictType?): TableDataInfo {
        startPage()
        val list = dictTypeService!!.selectDictTypeList(dictType)
        return getDataTable(list)
    }

    @Log(title = "字典类型", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('system:dict:export')")
    @PostMapping("/export")
    fun export(response: HttpServletResponse, dictType: SysDictType?) {
        val list = dictTypeService!!.selectDictTypeList(dictType)
        val util = ExcelUtil(SysDictType::class.java)
        util.exportExcel(response, list, "字典类型")
    }

    /**
     * 查询字典类型详细
     */
    @PreAuthorize("@ss.hasPermi('system:dict:query')")
    @GetMapping(value = ["/{dictId}"])
    fun getInfo(@PathVariable dictId: Long?): AjaxResult {
        return AjaxResult.success(dictTypeService!!.selectDictTypeById(dictId))
    }

    /**
     * 新增字典类型
     */
    @PreAuthorize("@ss.hasPermi('system:dict:add')")
    @Log(title = "字典类型", businessType = BusinessType.INSERT)
    @PostMapping
    fun add(@Validated @RequestBody dict: SysDictType): AjaxResult {
        if (UserConstants.NOT_UNIQUE == dictTypeService!!.checkDictTypeUnique(dict)) {
            return AjaxResult.Companion.error("新增字典'" + dict.dictName + "'失败，字典类型已存在")
        }
        dict.createBy = username
        return toAjax(dictTypeService.insertDictType(dict))
    }

    /**
     * 修改字典类型
     */
    @PreAuthorize("@ss.hasPermi('system:dict:edit')")
    @Log(title = "字典类型", businessType = BusinessType.UPDATE)
    @PutMapping
    fun edit(@Validated @RequestBody dict: SysDictType): AjaxResult {
        if (UserConstants.NOT_UNIQUE == dictTypeService!!.checkDictTypeUnique(dict)) {
            return AjaxResult.error("修改字典'" + dict.dictName + "'失败，字典类型已存在")
        }
        dict.updateBy = username
        return toAjax(dictTypeService.updateDictType(dict))
    }

    /**
     * 删除字典类型
     */
    @PreAuthorize("@ss.hasPermi('system:dict:remove')")
    @Log(title = "字典类型", businessType = BusinessType.DELETE)
    @DeleteMapping("/{dictIds}")
    fun remove(@PathVariable dictIds: Array<Long>): AjaxResult {
        dictTypeService!!.deleteDictTypeByIds(dictIds)
        return success()
    }

    /**
     * 刷新字典缓存
     */
    @PreAuthorize("@ss.hasPermi('system:dict:remove')")
    @Log(title = "字典类型", businessType = BusinessType.CLEAN)
    @DeleteMapping("/refreshCache")
    fun refreshCache(): AjaxResult {
        dictTypeService!!.resetDictCache()
        return AjaxResult.Companion.success()
    }

    /**
     * 获取字典选择框列表
     */
    @GetMapping("/optionselect")
    fun optionselect(): AjaxResult {
        val dictTypes = dictTypeService!!.selectDictTypeAll()
        return AjaxResult.Companion.success(dictTypes)
    }
}
