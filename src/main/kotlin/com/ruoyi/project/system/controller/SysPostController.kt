package com.ruoyi.project.system.controller

import com.ruoyi.common.constant.UserConstants
import com.ruoyi.common.utils.poi.ExcelUtil
import com.ruoyi.framework.aspectj.lang.annotation.*
import com.ruoyi.framework.aspectj.lang.enums.BusinessType
import com.ruoyi.framework.web.controller.BaseController
import com.ruoyi.framework.web.domain.AjaxResult
import com.ruoyi.framework.web.page.TableDataInfo
import com.ruoyi.project.system.domain.SysPost
import com.ruoyi.project.system.service.ISysPostService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse

/**
 * 岗位信息操作处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/post")
open class SysPostController : BaseController() {
    @Autowired
    private val postService: ISysPostService? = null

    /**
     * 获取岗位列表
     */
    @PreAuthorize("@ss.hasPermi('system:post:list')")
    @GetMapping("/list")
    fun list(post: SysPost?): TableDataInfo {
        startPage()
        val list = postService!!.selectPostList(post)
        return getDataTable(list)
    }

    @Log(title = "岗位管理", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('system:post:export')")
    @PostMapping("/export")
    fun export(response: HttpServletResponse, post: SysPost?) {
        val list = postService!!.selectPostList(post)
        val util = ExcelUtil(SysPost::class.java)
        util.exportExcel(response, list, "岗位数据")
    }

    /**
     * 根据岗位编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:post:query')")
    @GetMapping(value = ["/{postId}"])
    fun getInfo(@PathVariable postId: Long?): AjaxResult {
        return AjaxResult.Companion.success(postService!!.selectPostById(postId))
    }

    /**
     * 新增岗位
     */
    @PreAuthorize("@ss.hasPermi('system:post:add')")
    @Log(title = "岗位管理", businessType = BusinessType.INSERT)
    @PostMapping
    fun add(@Validated @RequestBody post: SysPost): AjaxResult {
        if (UserConstants.NOT_UNIQUE == postService!!.checkPostNameUnique(post)) {
            return AjaxResult.Companion.error("新增岗位'" + post.postName + "'失败，岗位名称已存在")
        } else if (UserConstants.NOT_UNIQUE == postService.checkPostCodeUnique(post)) {
            return AjaxResult.Companion.error("新增岗位'" + post.postName + "'失败，岗位编码已存在")
        }
        post.createBy = username
        return toAjax(postService.insertPost(post))
    }

    /**
     * 修改岗位
     */
    @PreAuthorize("@ss.hasPermi('system:post:edit')")
    @Log(title = "岗位管理", businessType = BusinessType.UPDATE)
    @PutMapping
    fun edit(@Validated @RequestBody post: SysPost): AjaxResult {
        if (UserConstants.NOT_UNIQUE == postService!!.checkPostNameUnique(post)) {
            return AjaxResult.Companion.error("修改岗位'" + post.postName + "'失败，岗位名称已存在")
        } else if (UserConstants.NOT_UNIQUE == postService.checkPostCodeUnique(post)) {
            return AjaxResult.Companion.error("修改岗位'" + post.postName + "'失败，岗位编码已存在")
        }
        post.updateBy = username
        return toAjax(postService.updatePost(post))
    }

    /**
     * 删除岗位
     */
    @PreAuthorize("@ss.hasPermi('system:post:remove')")
    @Log(title = "岗位管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{postIds}")
    fun remove(@PathVariable postIds: Array<Long>): AjaxResult {
        return toAjax(postService!!.deletePostByIds(postIds))
    }

    /**
     * 获取岗位选择框列表
     */
    @GetMapping("/optionselect")
    fun optionselect(): AjaxResult {
        val posts = postService!!.selectPostAll()
        return AjaxResult.Companion.success(posts)
    }
}
