package com.ruoyi.project.system.controller

import com.ruoyi.common.utils.StringUtils
import com.ruoyi.framework.security.RegisterBody
import com.ruoyi.framework.security.service.SysRegisterService
import com.ruoyi.framework.web.controller.BaseController
import com.ruoyi.framework.web.domain.AjaxResult
import com.ruoyi.project.system.service.ISysConfigService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

/**
 * 注册验证
 *
 * @author ruoyi
 */
@RestController
open class SysRegisterController : BaseController() {
    @Autowired
    private val registerService: SysRegisterService? = null

    @Autowired
    private val configService: ISysConfigService? = null
    @PostMapping("/register")
    fun register(@RequestBody user: RegisterBody): AjaxResult {
        if ("true" != configService!!.selectConfigByKey("sys.account.registerUser")) {
            return error("当前系统没有开启注册功能！")
        }
        val msg = registerService!!.register(user)
        return if (StringUtils.isEmpty(msg)) success() else error(msg)
    }
}
