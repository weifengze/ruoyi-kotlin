package com.ruoyi.project.system.domain

import com.ruoyi.framework.web.domain.BaseEntity
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

/**
 * 菜单权限表 sys_menu
 *
 * @author ruoyi
 */
open class SysMenu : BaseEntity() {
    /** 菜单ID  */
    var menuId: Long? = null

    /** 菜单名称  */
    var menuName: String? = null

    /** 父菜单名称  */
    var parentName: String? = null

    /** 父菜单ID  */
    var parentId: Long? = null

    /** 显示顺序  */
    var orderNum: Int? = null

    /** 路由地址  */
    var path: String? = null

    /** 组件路径  */
    var component: String? = null

    /** 路由参数  */
    var query: String? = null

    /** 是否为外链（0是 1否）  */
    var isFrame: String? = null

    /** 是否缓存（0缓存 1不缓存）  */
    var isCache: String? = null

    /** 类型（M目录 C菜单 F按钮）  */
    var menuType: String? = null

    /** 显示状态（0显示 1隐藏）  */
    var visible: String? = null

    /** 菜单状态（0显示 1隐藏）  */
    var status: String? = null

    /** 权限字符串  */
    var perms: String? = null

    /** 菜单图标  */
    var icon: String? = null

    /** 子菜单  */
    var children: List<SysMenu> = ArrayList()
    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("menuId", menuId)
            .append("menuName", menuName)
            .append("parentId", parentId)
            .append("orderNum", orderNum)
            .append("path", path)
            .append("component", component)
            .append("isFrame", isFrame)
            .append("IsCache", isCache)
            .append("menuType", menuType)
            .append("visible", visible)
            .append("status ", status)
            .append("perms", perms)
            .append("icon", icon)
            .append("createBy", createBy)
            .append("createTime", createTime)
            .append("updateBy", updateBy)
            .append("updateTime", updateTime)
            .append("remark", remark)
            .toString()
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
