package com.ruoyi.project.system.domain

import com.ruoyi.common.xss.Xss
import com.ruoyi.framework.aspectj.lang.annotation.Excel
import com.ruoyi.framework.aspectj.lang.annotation.Excel.ColumnType
import com.ruoyi.framework.aspectj.lang.annotation.Excels
import com.ruoyi.framework.web.domain.BaseEntity
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle
import java.util.*

/**
 * 用户对象 sys_user
 *
 * @author ruoyi
 */
open class SysUser : BaseEntity {
    /** 用户ID  */
    @Excel(name = "用户序号", cellType = ColumnType.NUMERIC, prompt = "用户编号")
    var userId: Long? = null

    /** 部门ID  */
    @Excel(name = "部门编号", type = Excel.Type.IMPORT)
    var deptId: Long? = null

    /** 用户账号  */
    @get:Xss(message = "用户账号不能包含脚本字符")
    @Excel(name = "登录名称")
    var userName: String? = null

    /** 用户昵称  */
    @get:Xss(message = "用户昵称不能包含脚本字符")
    @Excel(name = "用户名称")
    var nickName: String? = null

    /** 用户邮箱  */
    @Excel(name = "用户邮箱")
    var email: String? = null

    /** 手机号码  */
    @Excel(name = "手机号码")
    var phonenumber: String? = null

    /** 用户性别  */
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    var sex: String? = null

    /** 用户头像  */
    var avatar: String? = null

    /** 密码  */
    var password: String? = null

    /** 帐号状态（0正常 1停用）  */
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    var status: String? = null

    /** 删除标志（0代表存在 2代表删除）  */
    var delFlag: String? = null

    /** 最后登录IP  */
    @Excel(name = "最后登录IP", type = Excel.Type.EXPORT)
    var loginIp: String? = null

    /** 最后登录时间  */
    @Excel(name = "最后登录时间", width = 30.toDouble(), dateFormat = "yyyy-MM-dd HH:mm:ss", type = Excel.Type.EXPORT)
    var loginDate: Date? = null

    /** 部门对象  */
    @Excels(
        Excel(name = "部门名称", targetAttr = "deptName", type = Excel.Type.EXPORT),
        Excel(name = "部门负责人", targetAttr = "leader", type = Excel.Type.EXPORT)
    )
    var dept: SysDept? = null

    /** 角色对象  */
    var roles: List<SysRole>? = null

    /** 角色组  */
    var roleIds: Array<Long>? = null

    /** 岗位组  */
    var postIds: Array<Long>? = null

    /** 角色ID  */
    var roleId: Long? = null

    constructor() {}
    constructor(userId: Long?) {
        this.userId = userId
    }

    val isAdmin: Boolean
        get() = SysUser.Companion.isAdmin(userId)

    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", userId)
            .append("deptId", deptId)
            .append("userName", userName)
            .append("nickName", nickName)
            .append("email", email)
            .append("phonenumber", phonenumber)
            .append("sex", sex)
            .append("avatar", avatar)
            .append("password", password)
            .append("status", status)
            .append("delFlag", delFlag)
            .append("loginIp", loginIp)
            .append("loginDate", loginDate)
            .append("createBy", createBy)
            .append("createTime", createTime)
            .append("updateBy", updateBy)
            .append("updateTime", updateTime)
            .append("remark", remark)
            .append("dept", dept)
            .toString()
    }

    companion object {
        private const val serialVersionUID = 1L
        fun isAdmin(userId: Long?): Boolean {
            return userId != null && 1L == userId
        }
    }
}
