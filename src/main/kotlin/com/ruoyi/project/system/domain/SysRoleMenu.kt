package com.ruoyi.project.system.domain

import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

/**
 * 角色和菜单关联 sys_role_menu
 *
 * @author ruoyi
 */
open class SysRoleMenu {
    /** 角色ID  */
    var roleId: Long? = null

    /** 菜单ID  */
    var menuId: Long? = null
    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("roleId", roleId)
            .append("menuId", menuId)
            .toString()
    }
}
