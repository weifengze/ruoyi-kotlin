package com.ruoyi.project.system.domain

import com.ruoyi.common.constant.UserConstants
import com.ruoyi.framework.aspectj.lang.annotation.Excel
import com.ruoyi.framework.aspectj.lang.annotation.Excel.ColumnType
import com.ruoyi.framework.web.domain.BaseEntity
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

/**
 * 字典数据表 sys_dict_data
 *
 * @author ruoyi
 */
open class SysDictData : BaseEntity() {
    /** 字典编码  */
    @Excel(name = "字典编码", cellType = ColumnType.NUMERIC)
    var dictCode: Long? = null

    /** 字典排序  */
    @Excel(name = "字典排序", cellType = ColumnType.NUMERIC)
    var dictSort: Long? = null

    /** 字典标签  */
    @Excel(name = "字典标签")
    var dictLabel: String? = null

    /** 字典键值  */
    @Excel(name = "字典键值")
    var dictValue: String? = null

    /** 字典类型  */
    @Excel(name = "字典类型")
    var dictType: String? = null

    /** 样式属性（其他样式扩展）  */
    var cssClass: String? = null

    /** 表格字典样式  */
    var listClass: String? = null

    /** 是否默认（Y是 N否）  */
    @Excel(name = "是否默认", readConverterExp = "Y=是,N=否")
    private var isDefault: String? = null

    /** 状态（0正常 1停用）  */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    var status: String? = null
    fun getDefault(): Boolean {
        return if (UserConstants.YES == isDefault) true else false
    }

    fun getIsDefault(): String? {
        return isDefault
    }

    fun setIsDefault(isDefault: String?) {
        this.isDefault = isDefault
    }

    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("dictCode", dictCode)
                .append("dictSort", dictSort)
                .append("dictLabel", dictLabel)
                .append("dictValue", dictValue)
                .append("dictType", dictType)
                .append("cssClass", cssClass)
                .append("listClass", listClass)
                .append("isDefault", getIsDefault())
                .append("status", status)
                .append("createBy", createBy)
                .append("createTime", createTime)
                .append("updateBy", updateBy)
                .append("updateTime", updateTime)
                .append("remark", remark)
                .toString()
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
