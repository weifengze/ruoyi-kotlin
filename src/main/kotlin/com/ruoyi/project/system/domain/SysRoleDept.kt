package com.ruoyi.project.system.domain

import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

/**
 * 角色和部门关联 sys_role_dept
 *
 * @author ruoyi
 */
open class SysRoleDept {
    /** 角色ID  */
    var roleId: Long? = null

    /** 部门ID  */
    var deptId: Long? = null
    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("roleId", roleId)
            .append("deptId", deptId)
            .toString()
    }
}
