package com.ruoyi.project.system.domain

import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

/**
 * 用户和岗位关联 sys_user_post
 *
 * @author ruoyi
 */
open class SysUserPost {
    /** 用户ID  */
    var userId: Long? = null

    /** 岗位ID  */
    var postId: Long? = null
    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", userId)
            .append("postId", postId)
            .toString()
    }
}
