package com.ruoyi.project.system.domain

import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

/**
 * 用户和角色关联 sys_user_role
 *
 * @author ruoyi
 */
open class SysUserRole {
    /** 用户ID  */
    var userId: Long? = null

    /** 角色ID  */
    var roleId: Long? = null
    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", userId)
            .append("roleId", roleId)
            .toString()
    }
}
