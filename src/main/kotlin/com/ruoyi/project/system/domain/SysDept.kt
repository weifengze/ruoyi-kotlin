package com.ruoyi.project.system.domain

import com.ruoyi.framework.web.domain.BaseEntity
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

/**
 * 部门表 sys_dept
 *
 * @author ruoyi
 */
open class SysDept : BaseEntity() {
    /** 部门ID  */
    var deptId: Long? = null

    /** 父部门ID  */
    var parentId: Long? = null

    /** 祖级列表  */
    var ancestors: String? = null

    /** 部门名称  */
    var deptName: String? = null

    /** 显示顺序  */
    var orderNum: Int? = null

    /** 负责人  */
    var leader: String? = null

    /** 联系电话  */
    var phone: String? = null

    /** 邮箱  */
    var email: String? = null

    /** 部门状态:0正常,1停用  */
    var status: String? = null

    /** 删除标志（0代表存在 2代表删除）  */
    var delFlag: String? = null

    /** 父部门名称  */
    var parentName: String? = null

    /** 子部门  */
    var children: List<SysDept> = ArrayList()
    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("deptId", deptId)
            .append("parentId", parentId)
            .append("ancestors", ancestors)
            .append("deptName", deptName)
            .append("orderNum", orderNum)
            .append("leader", leader)
            .append("phone", phone)
            .append("email", email)
            .append("status", status)
            .append("delFlag", delFlag)
            .append("createBy", createBy)
            .append("createTime", createTime)
            .append("updateBy", updateBy)
            .append("updateTime", updateTime)
            .toString()
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
