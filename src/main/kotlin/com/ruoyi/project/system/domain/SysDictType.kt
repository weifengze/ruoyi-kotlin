package com.ruoyi.project.system.domain

import com.ruoyi.framework.aspectj.lang.annotation.Excel
import com.ruoyi.framework.aspectj.lang.annotation.Excel.ColumnType
import com.ruoyi.framework.web.domain.BaseEntity
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

/**
 * 字典类型表 sys_dict_type
 *
 * @author ruoyi
 */
open class SysDictType : BaseEntity() {
    /** 字典主键  */
    @Excel(name = "字典主键", cellType = ColumnType.NUMERIC)
    var dictId: Long? = null

    /** 字典名称  */
    @Excel(name = "字典名称")
    var dictName: String? = null

    /** 字典类型  */
    @Excel(name = "字典类型")
    var dictType: String? = null

    /** 状态（0正常 1停用）  */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    var status: String? = null
    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("dictId", dictId)
            .append("dictName", dictName)
            .append("dictType", dictType)
            .append("status", status)
            .append("createBy", createBy)
            .append("createTime", createTime)
            .append("updateBy", updateBy)
            .append("updateTime", updateTime)
            .append("remark", remark)
            .toString()
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
