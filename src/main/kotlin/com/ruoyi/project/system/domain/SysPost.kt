package com.ruoyi.project.system.domain

import com.ruoyi.framework.aspectj.lang.annotation.Excel
import com.ruoyi.framework.aspectj.lang.annotation.Excel.ColumnType
import com.ruoyi.framework.web.domain.BaseEntity
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

/**
 * 岗位表 sys_post
 *
 * @author ruoyi
 */
open class SysPost : BaseEntity() {
    /** 岗位序号  */
    @Excel(name = "岗位序号", cellType = ColumnType.NUMERIC)
    var postId: Long? = null

    /** 岗位编码  */
    @Excel(name = "岗位编码")
    var postCode: String? = null

    /** 岗位名称  */
    @Excel(name = "岗位名称")
    var postName: String? = null

    /** 岗位排序  */
    @Excel(name = "岗位排序")
    var postSort: String? = null

    /** 状态（0正常 1停用）  */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    var status: String? = null

    /** 用户是否存在此岗位标识 默认不存在  */
    var isFlag = false
    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("postId", postId)
            .append("postCode", postCode)
            .append("postName", postName)
            .append("postSort", postSort)
            .append("status", status)
            .append("createBy", createBy)
            .append("createTime", createTime)
            .append("updateBy", updateBy)
            .append("updateTime", updateTime)
            .append("remark", remark)
            .toString()
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
