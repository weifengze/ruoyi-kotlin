package com.ruoyi.project.system.domain

import com.ruoyi.framework.aspectj.lang.annotation.Excel
import com.ruoyi.framework.aspectj.lang.annotation.Excel.ColumnType
import com.ruoyi.framework.web.domain.BaseEntity
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

/**
 * 参数配置表 sys_config
 *
 * @author ruoyi
 */
open class SysConfig : BaseEntity() {
    /** 参数主键  */
    @Excel(name = "参数主键", cellType = ColumnType.NUMERIC)
    var configId: Long? = null

    /** 参数名称  */
    @Excel(name = "参数名称")
    var configName: String? = null

    /** 参数键名  */
    @Excel(name = "参数键名")
    var configKey: String? = null

    /** 参数键值  */
    @Excel(name = "参数键值")
    var configValue: String? = null

    /** 系统内置（Y是 N否）  */
    @Excel(name = "系统内置", readConverterExp = "Y=是,N=否")
    var configType: String? = null
    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("configId", configId)
            .append("configName", configName)
            .append("configKey", configKey)
            .append("configValue", configValue)
            .append("configType", configType)
            .append("createBy", createBy)
            .append("createTime", createTime)
            .append("updateBy", updateBy)
            .append("updateTime", updateTime)
            .append("remark", remark)
            .toString()
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
