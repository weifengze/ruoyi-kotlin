package com.ruoyi.project.system.domain

import com.ruoyi.framework.aspectj.lang.annotation.Excel
import com.ruoyi.framework.aspectj.lang.annotation.Excel.ColumnType
import com.ruoyi.framework.web.domain.BaseEntity
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

/**
 * 角色表 sys_role
 *
 * @author ruoyi
 */
open class SysRole : BaseEntity {
    /** 角色ID  */
    @Excel(name = "角色序号", cellType = ColumnType.NUMERIC)
    var roleId: Long? = null

    /** 角色名称  */
    @Excel(name = "角色名称")
    var roleName: String? = null

    /** 角色权限  */
    @Excel(name = "角色权限")
    var roleKey: String? = null

    /** 角色排序  */
    @Excel(name = "角色排序")
    var roleSort: String? = null

    /** 数据范围（1：所有数据权限；2：自定义数据权限；3：本部门数据权限；4：本部门及以下数据权限；5：仅本人数据权限）  */
    @Excel(
        name = "数据范围",
        readConverterExp = "1=所有数据权限,2=自定义数据权限,3=本部门数据权限,4=本部门及以下数据权限,5=仅本人数据权限"
    )
    var dataScope: String? = null

    /** 菜单树选择项是否关联显示（ 0：父子不互相关联显示 1：父子互相关联显示）  */
    var isMenuCheckStrictly = false

    /** 部门树选择项是否关联显示（0：父子不互相关联显示 1：父子互相关联显示 ）  */
    var isDeptCheckStrictly = false

    /** 角色状态（0正常 1停用）  */
    @Excel(name = "角色状态", readConverterExp = "0=正常,1=停用")
    var status: String? = null

    /** 删除标志（0代表存在 2代表删除）  */
    var delFlag: String? = null

    /** 用户是否存在此角色标识 默认不存在  */
    var isFlag = false

    /** 菜单组  */
    var menuIds: List<Long>? = null

    /** 部门组（数据权限）  */
    var deptIds: List<Long>? = null

    /** 角色菜单权限  */
    var permissions: Set<String>? = null

    constructor() {}
    constructor(roleId: Long?) {
        this.roleId = roleId
    }

    val isAdmin: Boolean
        get() = isAdmin(roleId)

    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("roleId", roleId)
            .append("roleName", roleName)
            .append("roleKey", roleKey)
            .append("roleSort", roleSort)
            .append("dataScope", dataScope)
            .append("menuCheckStrictly", isMenuCheckStrictly)
            .append("deptCheckStrictly", isDeptCheckStrictly)
            .append("status", status)
            .append("delFlag", delFlag)
            .append("createBy", createBy)
            .append("createTime", createTime)
            .append("updateBy", updateBy)
            .append("updateTime", updateTime)
            .append("remark", remark)
            .toString()
    }

    companion object {
        private const val serialVersionUID = 1L
        fun isAdmin(roleId: Long?): Boolean {
            return roleId != null && 1L == roleId
        }
    }
}
