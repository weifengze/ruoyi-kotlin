package com.ruoyi.project.system.domain

import com.ruoyi.common.xss.Xss
import com.ruoyi.framework.web.domain.BaseEntity
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

/**
 * 通知公告表 sys_notice
 *
 * @author ruoyi
 */
open class SysNotice : BaseEntity() {
    /** 公告ID  */
    var noticeId: Long? = null

    /** 公告标题  */
    @get:Xss(message = "公告标题不能包含脚本字符")
    var noticeTitle: String? = null

    /** 公告类型（1通知 2公告）  */
    var noticeType: String? = null

    /** 公告内容  */
    var noticeContent: String? = null

    /** 公告状态（0正常 1关闭）  */
    var status: String? = null
    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("noticeId", noticeId)
            .append("noticeTitle", noticeTitle)
            .append("noticeType", noticeType)
            .append("noticeContent", noticeContent)
            .append("status", status)
            .append("createBy", createBy)
            .append("createTime", createTime)
            .append("updateBy", updateBy)
            .append("updateTime", updateTime)
            .append("remark", remark)
            .toString()
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
