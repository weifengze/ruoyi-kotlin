package com.ruoyi.project.system.service

import com.ruoyi.framework.security.LoginUser
import com.ruoyi.project.monitor.domain.SysUserOnline

/**
 * 在线用户 服务层
 *
 * @author ruoyi
 */
interface ISysUserOnlineService {
    /**
     * 通过登录地址查询信息
     *
     * @param ipaddr 登录地址
     * @param user 用户信息
     * @return 在线用户信息
     */
    fun selectOnlineByIpaddr(ipaddr: String?, user: LoginUser): SysUserOnline?

    /**
     * 通过用户名称查询信息
     *
     * @param userName 用户名称
     * @param user 用户信息
     * @return 在线用户信息
     */
    fun selectOnlineByUserName(userName: String?, user: LoginUser): SysUserOnline?

    /**
     * 通过登录地址/用户名称查询信息
     *
     * @param ipaddr 登录地址
     * @param userName 用户名称
     * @param user 用户信息
     * @return 在线用户信息
     */
    fun selectOnlineByInfo(ipaddr: String?, userName: String?, user: LoginUser): SysUserOnline?

    /**
     * 设置在线用户信息
     *
     * @param user 用户信息
     * @return 在线用户
     */
    fun loginUserToUserOnline(user: LoginUser): SysUserOnline?
}
