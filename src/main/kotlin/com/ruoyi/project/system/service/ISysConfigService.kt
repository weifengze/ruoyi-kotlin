package com.ruoyi.project.system.service

import com.ruoyi.project.system.domain.SysConfig

/**
 * 参数配置 服务层
 *
 * @author ruoyi
 */
interface ISysConfigService {
    /**
     * 查询参数配置信息
     *
     * @param configId 参数配置ID
     * @return 参数配置信息
     */
    fun selectConfigById(configId: Long?): SysConfig

    /**
     * 根据键名查询参数配置信息
     *
     * @param configKey 参数键名
     * @return 参数键值
     */
    fun selectConfigByKey(configKey: String): String

    /**
     * 获取验证码开关
     *
     * @return true开启，false关闭
     */
    fun selectCaptchaEnabled(): Boolean

    /**
     * 查询参数配置列表
     *
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    fun selectConfigList(config: SysConfig?): List<SysConfig>?

    /**
     * 新增参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    fun insertConfig(config: SysConfig): Int

    /**
     * 修改参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    fun updateConfig(config: SysConfig): Int

    /**
     * 批量删除参数信息
     *
     * @param configIds 需要删除的参数ID
     */
    fun deleteConfigByIds(configIds: Array<Long?>)

    /**
     * 加载参数缓存数据
     */
    fun loadingConfigCache()

    /**
     * 清空参数缓存数据
     */
    fun clearConfigCache()

    /**
     * 重置参数缓存数据
     */
    fun resetConfigCache()

    /**
     * 校验参数键名是否唯一
     *
     * @param config 参数信息
     * @return 结果
     */
    fun checkConfigKeyUnique(config: SysConfig): String?
}
