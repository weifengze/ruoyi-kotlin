package com.ruoyi.project.system.service

import com.ruoyi.project.system.domain.SysPost

/**
 * 岗位信息 服务层
 *
 * @author ruoyi
 */
interface ISysPostService {
    /**
     * 查询岗位信息集合
     *
     * @param post 岗位信息
     * @return 岗位列表
     */
    fun selectPostList(post: SysPost?): List<SysPost>?

    /**
     * 查询所有岗位
     *
     * @return 岗位列表
     */
    fun selectPostAll(): List<SysPost?>?

    /**
     * 通过岗位ID查询岗位信息
     *
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    fun selectPostById(postId: Long?): SysPost

    /**
     * 根据用户ID获取岗位选择框列表
     *
     * @param userId 用户ID
     * @return 选中岗位ID列表
     */
    fun selectPostListByUserId(userId: Long?): List<Long?>?

    /**
     * 校验岗位名称
     *
     * @param post 岗位信息
     * @return 结果
     */
    fun checkPostNameUnique(post: SysPost): String?

    /**
     * 校验岗位编码
     *
     * @param post 岗位信息
     * @return 结果
     */
    fun checkPostCodeUnique(post: SysPost): String?

    /**
     * 通过岗位ID查询岗位使用数量
     *
     * @param postId 岗位ID
     * @return 结果
     */
    fun countUserPostById(postId: Long?): Int

    /**
     * 删除岗位信息
     *
     * @param postId 岗位ID
     * @return 结果
     */
    fun deletePostById(postId: Long?): Int

    /**
     * 批量删除岗位信息
     *
     * @param postIds 需要删除的岗位ID
     * @return 结果
     */
    fun deletePostByIds(postIds: Array<Long>): Int

    /**
     * 新增保存岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    fun insertPost(post: SysPost?): Int

    /**
     * 修改保存岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    fun updatePost(post: SysPost?): Int
}
