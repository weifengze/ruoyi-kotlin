package com.ruoyi.project.system.service.impl

import com.ruoyi.common.utils.DictUtils
import com.ruoyi.project.system.domain.SysDictData
import com.ruoyi.project.system.mapper.SysDictDataMapper
import com.ruoyi.project.system.service.ISysDictDataService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * 字典 业务层处理
 *
 * @author ruoyi
 */
@Service
class SysDictDataServiceImpl : ISysDictDataService {
    @Autowired
    private val dictDataMapper: SysDictDataMapper? = null

    /**
     * 根据条件分页查询字典数据
     *
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    override fun selectDictDataList(dictData: SysDictData?): List<SysDictData> {
        return dictDataMapper!!.selectDictDataList(dictData)
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     *
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    override fun selectDictLabel(dictType: String?, dictValue: String?): String? {
        return dictDataMapper!!.selectDictLabel(dictType, dictValue)
    }

    /**
     * 根据字典数据ID查询信息
     *
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    override fun selectDictDataById(dictCode: Long?): SysDictData {
        return dictDataMapper!!.selectDictDataById(dictCode)!!
    }

    /**
     * 批量删除字典数据信息
     *
     * @param dictCodes 需要删除的字典数据ID
     */
    override fun deleteDictDataByIds(dictCodes: Array<Long>) {
        dictCodes.forEach {
            val data = selectDictDataById(it)
            dictDataMapper!!.deleteDictDataById(it)
            val dictDatas = dictDataMapper.selectDictDataByType(data.dictType)!!
            DictUtils.setDictCache(data.dictType!!, dictDatas)
        }
    }

    /**
     * 新增保存字典数据信息
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    override fun insertDictData(dictData: SysDictData): Int {
        val row = dictDataMapper!!.insertDictData(dictData)
        if (row > 0) {
            val dictDatas = dictDataMapper.selectDictDataByType(dictData.dictType)!!
            DictUtils.setDictCache(dictData.dictType!!, dictDatas)
        }
        return row
    }

    /**
     * 修改保存字典数据信息
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    override fun updateDictData(dictData: SysDictData): Int {
        val row = dictDataMapper!!.updateDictData(dictData)
        if (row > 0) {
            val dictDatas = dictDataMapper.selectDictDataByType(dictData.dictType)!!
            DictUtils.setDictCache(dictData.dictType!!, dictDatas)
        }
        return row
    }
}
