package com.ruoyi.project.system.service.impl

import com.ruoyi.project.system.domain.SysNotice
import com.ruoyi.project.system.mapper.SysNoticeMapper
import com.ruoyi.project.system.service.ISysNoticeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * 公告 服务层实现
 *
 * @author ruoyi
 */
@Service
class SysNoticeServiceImpl : ISysNoticeService {
    @Autowired
    private val noticeMapper: SysNoticeMapper? = null

    /**
     * 查询公告信息
     *
     * @param noticeId 公告ID
     * @return 公告信息
     */
    override fun selectNoticeById(noticeId: Long?): SysNotice? {
        return noticeMapper!!.selectNoticeById(noticeId)
    }

    /**
     * 查询公告列表
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    override fun selectNoticeList(notice: SysNotice?): List<SysNotice?>? {
        return noticeMapper!!.selectNoticeList(notice)
    }

    /**
     * 新增公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    override fun insertNotice(notice: SysNotice?): Int {
        return noticeMapper!!.insertNotice(notice)
    }

    /**
     * 修改公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    override fun updateNotice(notice: SysNotice?): Int {
        return noticeMapper!!.updateNotice(notice)
    }

    /**
     * 删除公告对象
     *
     * @param noticeId 公告ID
     * @return 结果
     */
    override fun deleteNoticeById(noticeId: Long?): Int {
        return noticeMapper!!.deleteNoticeById(noticeId)
    }

    /**
     * 批量删除公告信息
     *
     * @param noticeIds 需要删除的公告ID
     * @return 结果
     */
    override fun deleteNoticeByIds(noticeIds: Array<Long?>?): Int {
        return noticeMapper!!.deleteNoticeByIds(noticeIds)
    }
}
