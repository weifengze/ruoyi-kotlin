package com.ruoyi.project.system.service.impl

import com.ruoyi.framework.security.LoginUser
import com.ruoyi.project.monitor.domain.SysUserOnline
import com.ruoyi.project.system.service.ISysUserOnlineService
import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Service

/**
 * 在线用户 服务层处理
 *
 * @author ruoyi
 */
@Service
class SysUserOnlineServiceImpl : ISysUserOnlineService {
    /**
     * 通过登录地址查询信息
     *
     * @param ipaddr 登录地址
     * @param user 用户信息
     * @return 在线用户信息
     */
    override fun selectOnlineByIpaddr(ipaddr: String?, user: LoginUser): SysUserOnline? {
        return if (StringUtils.equals(ipaddr, user.ipaddr)) {
            loginUserToUserOnline(user)
        } else null
    }

    /**
     * 通过用户名称查询信息
     *
     * @param userName 用户名称
     * @param user 用户信息
     * @return 在线用户信息
     */
    override fun selectOnlineByUserName(userName: String?, user: LoginUser): SysUserOnline? {
        return if (StringUtils.equals(userName, user.username)) {
            loginUserToUserOnline(user)
        } else null
    }

    /**
     * 通过登录地址/用户名称查询信息
     *
     * @param ipaddr 登录地址
     * @param userName 用户名称
     * @param user 用户信息
     * @return 在线用户信息
     */
    override fun selectOnlineByInfo(ipaddr: String?, userName: String?, user: LoginUser): SysUserOnline? {
        return if (StringUtils.equals(ipaddr, user.ipaddr) && StringUtils.equals(userName, user.username)) {
            loginUserToUserOnline(user)
        } else null
    }

    /**
     * 设置在线用户信息
     *
     * @param user 用户信息
     * @return 在线用户
     */
    override fun loginUserToUserOnline(user: LoginUser): SysUserOnline? {
        if (com.ruoyi.common.utils.StringUtils.isNull(user) || com.ruoyi.common.utils.StringUtils.isNull(user.user)) {
            return null
        }
        val sysUserOnline = SysUserOnline()
        sysUserOnline.tokenId = user.token
        sysUserOnline.userName = user.username
        sysUserOnline.ipaddr = user.ipaddr
        sysUserOnline.loginLocation = user.loginLocation
        sysUserOnline.browser = user.browser
        sysUserOnline.os = user.os
        sysUserOnline.loginTime = user.loginTime
        if (com.ruoyi.common.utils.StringUtils.isNotNull(user.user?.dept)) {
            sysUserOnline.deptName = user.user?.dept?.deptName
        }
        return sysUserOnline
    }
}
