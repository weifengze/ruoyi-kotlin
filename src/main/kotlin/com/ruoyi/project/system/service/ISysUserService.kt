package com.ruoyi.project.system.service

import com.ruoyi.project.system.domain.SysUser

/**
 * 用户 业务层
 *
 * @author ruoyi
 */
interface ISysUserService {
    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    fun selectUserList(user: SysUser?): List<SysUser>?

    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    fun selectAllocatedList(user: SysUser?): List<SysUser>?

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    fun selectUnallocatedList(user: SysUser?): List<SysUser>?

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    fun selectUserByUserName(userName: String?): SysUser?

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    fun selectUserById(userId: Long?): SysUser?

    /**
     * 根据用户ID查询用户所属角色组
     *
     * @param userName 用户名
     * @return 结果
     */
    fun selectUserRoleGroup(userName: String?): String?

    /**
     * 根据用户ID查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    fun selectUserPostGroup(userName: String?): String?

    /**
     * 校验用户名称是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    fun checkUserNameUnique(userName: String?): String?

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    fun checkPhoneUnique(user: SysUser): String?

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    fun checkEmailUnique(user: SysUser): String?

    /**
     * 校验用户是否允许操作
     *
     * @param user 用户信息
     */
    fun checkUserAllowed(user: SysUser)

    /**
     * 校验用户是否有数据权限
     *
     * @param userId 用户id
     */
    fun checkUserDataScope(userId: Long?)

    /**
     * 新增用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    fun insertUser(user: SysUser): Int

    /**
     * 注册用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    fun registerUser(user: SysUser?): Boolean

    /**
     * 修改用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    fun updateUser(user: SysUser): Int

    /**
     * 用户授权角色
     *
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    fun insertUserAuth(userId: Long?, roleIds: Array<Long>)

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    fun updateUserStatus(user: SysUser?): Int

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    fun updateUserProfile(user: SysUser?): Int

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    fun updateUserAvatar(userName: String?, avatar: String?): Boolean

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    fun resetPwd(user: SysUser?): Int

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    fun resetUserPwd(userName: String?, password: String?): Int

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    fun deleteUserById(userId: Long?): Int

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    fun deleteUserByIds(userIds: Array<Long?>): Int

    /**
     * 导入用户数据
     *
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    fun importUser(userList: List<SysUser?>, isUpdateSupport: Boolean, operName: String?): String
}
