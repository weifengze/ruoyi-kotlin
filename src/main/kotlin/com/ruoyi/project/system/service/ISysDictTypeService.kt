package com.ruoyi.project.system.service

import com.ruoyi.project.system.domain.SysDictData
import com.ruoyi.project.system.domain.SysDictType

/**
 * 字典 业务层
 *
 * @author ruoyi
 */
interface ISysDictTypeService {
    /**
     * 根据条件分页查询字典类型
     *
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    fun selectDictTypeList(dictType: SysDictType?): List<SysDictType>?

    /**
     * 根据所有字典类型
     *
     * @return 字典类型集合信息
     */
    fun selectDictTypeAll(): List<SysDictType?>?

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    fun selectDictDataByType(dictType: String): List<SysDictData?>?

    /**
     * 根据字典类型ID查询信息
     *
     * @param dictId 字典类型ID
     * @return 字典类型
     */
    fun selectDictTypeById(dictId: Long?): SysDictType

    /**
     * 根据字典类型查询信息
     *
     * @param dictType 字典类型
     * @return 字典类型
     */
    fun selectDictTypeByType(dictType: String?): SysDictType?

    /**
     * 批量删除字典信息
     *
     * @param dictIds 需要删除的字典ID
     */
    fun deleteDictTypeByIds(dictIds: Array<Long>)

    /**
     * 加载字典缓存数据
     */
    fun loadingDictCache()

    /**
     * 清空字典缓存数据
     */
    fun clearDictCache()

    /**
     * 重置字典缓存数据
     */
    fun resetDictCache()

    /**
     * 新增保存字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    fun insertDictType(dictType: SysDictType): Int

    /**
     * 修改保存字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    fun updateDictType(dictType: SysDictType): Int

    /**
     * 校验字典类型称是否唯一
     *
     * @param dictType 字典类型
     * @return 结果
     */
    fun checkDictTypeUnique(dictType: SysDictType): String?
}
