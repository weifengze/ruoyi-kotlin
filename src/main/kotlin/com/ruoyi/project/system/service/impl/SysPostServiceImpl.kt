package com.ruoyi.project.system.service.impl

import com.ruoyi.common.constant.UserConstants
import com.ruoyi.common.exception.ServiceException
import com.ruoyi.common.utils.StringUtils
import com.ruoyi.project.system.domain.SysPost
import com.ruoyi.project.system.mapper.SysPostMapper
import com.ruoyi.project.system.mapper.SysUserPostMapper
import com.ruoyi.project.system.service.ISysPostService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * 岗位信息 服务层处理
 *
 * @author ruoyi
 */
@Service
class SysPostServiceImpl : ISysPostService {
    @Autowired
    private val postMapper: SysPostMapper? = null

    @Autowired
    private val userPostMapper: SysUserPostMapper? = null

    /**
     * 查询岗位信息集合
     *
     * @param post 岗位信息
     * @return 岗位信息集合
     */
    override fun selectPostList(post: SysPost?): List<SysPost>? {
        return postMapper!!.selectPostList(post)
    }

    /**
     * 查询所有岗位
     *
     * @return 岗位列表
     */
    override fun selectPostAll(): List<SysPost?>? {
        return postMapper!!.selectPostAll()
    }

    /**
     * 通过岗位ID查询岗位信息
     *
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    override fun selectPostById(postId: Long?): SysPost {
        return postMapper!!.selectPostById(postId)!!
    }

    /**
     * 根据用户ID获取岗位选择框列表
     *
     * @param userId 用户ID
     * @return 选中岗位ID列表
     */
    override fun selectPostListByUserId(userId: Long?): List<Long?>? {
        return postMapper!!.selectPostListByUserId(userId)
    }

    /**
     * 校验岗位名称是否唯一
     *
     * @param post 岗位信息
     * @return 结果
     */
    override fun checkPostNameUnique(post: SysPost): String {
        val postId = if (StringUtils.isNull(post.postId)) -1L else post.postId
        val info = postMapper!!.checkPostNameUnique(post.postName)
        return if (StringUtils.isNotNull(info) && info!!.postId!!.toLong() != postId!!.toLong()) {
            UserConstants.NOT_UNIQUE
        } else UserConstants.UNIQUE
    }

    /**
     * 校验岗位编码是否唯一
     *
     * @param post 岗位信息
     * @return 结果
     */
    override fun checkPostCodeUnique(post: SysPost): String {
        val postId = if (StringUtils.isNull(post.postId)) -1L else post.postId
        val info = postMapper!!.checkPostCodeUnique(post.postCode)
        return if (StringUtils.isNotNull(info) && info!!.postId!!.toLong() != postId!!.toLong()) {
            UserConstants.NOT_UNIQUE
        } else UserConstants.UNIQUE
    }

    /**
     * 通过岗位ID查询岗位使用数量
     *
     * @param postId 岗位ID
     * @return 结果
     */
    override fun countUserPostById(postId: Long?): Int {
        return userPostMapper!!.countUserPostById(postId)
    }

    /**
     * 删除岗位信息
     *
     * @param postId 岗位ID
     * @return 结果
     */
    override fun deletePostById(postId: Long?): Int {
        return postMapper!!.deletePostById(postId)
    }

    /**
     * 批量删除岗位信息
     *
     * @param postIds 需要删除的岗位ID
     * @return 结果
     */
    override fun deletePostByIds(postIds: Array<Long>): Int {
        for (postId in postIds) {
            val post = selectPostById(postId)
            if (countUserPostById(postId) > 0) {
                throw ServiceException(String.format("%1\$s已分配,不能删除", post.postName))
            }
        }
        return postMapper!!.deletePostByIds(postIds)
    }

    /**
     * 新增保存岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    override fun insertPost(post: SysPost?): Int {
        return postMapper!!.insertPost(post)
    }

    /**
     * 修改保存岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    override fun updatePost(post: SysPost?): Int {
        return postMapper!!.updatePost(post)
    }
}
