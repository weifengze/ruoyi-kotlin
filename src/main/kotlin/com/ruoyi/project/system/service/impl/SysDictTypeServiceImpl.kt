package com.ruoyi.project.system.service.impl

import com.ruoyi.common.constant.UserConstants
import com.ruoyi.common.exception.ServiceException
import com.ruoyi.common.utils.DictUtils
import com.ruoyi.common.utils.StringUtils
import com.ruoyi.project.system.domain.SysDictData
import com.ruoyi.project.system.domain.SysDictType
import com.ruoyi.project.system.mapper.SysDictDataMapper
import com.ruoyi.project.system.mapper.SysDictTypeMapper
import com.ruoyi.project.system.service.ISysDictTypeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.stream.Collectors
import javax.annotation.PostConstruct

/**
 * 字典 业务层处理
 *
 * @author ruoyi
 */
@Service
class SysDictTypeServiceImpl : ISysDictTypeService {
    @Autowired
    private val dictTypeMapper: SysDictTypeMapper? = null

    @Autowired
    private val dictDataMapper: SysDictDataMapper? = null

    /**
     * 项目启动时，初始化字典到缓存
     */
    @PostConstruct
    fun init() {
        loadingDictCache()
    }

    /**
     * 根据条件分页查询字典类型
     *
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    override fun selectDictTypeList(dictType: SysDictType?): List<SysDictType>? {
        return dictTypeMapper!!.selectDictTypeList(dictType)
    }

    /**
     * 根据所有字典类型
     *
     * @return 字典类型集合信息
     */
    override fun selectDictTypeAll(): List<SysDictType?>? {
        return dictTypeMapper!!.selectDictTypeAll()
    }

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    override fun selectDictDataByType(dictType: String): List<SysDictData?>? {
        var dictDatas = DictUtils.getDictCache(dictType)
        if (StringUtils.isNotEmpty(dictDatas)) {
            return dictDatas
        }
        dictDatas = dictDataMapper!!.selectDictDataByType(dictType)!!
        if (StringUtils.isNotEmpty(dictDatas)) {
            DictUtils.setDictCache(dictType, dictDatas)
            return dictDatas
        }
        return null
    }

    /**
     * 根据字典类型ID查询信息
     *
     * @param dictId 字典类型ID
     * @return 字典类型
     */
    override fun selectDictTypeById(dictId: Long?): SysDictType {
        return dictTypeMapper!!.selectDictTypeById(dictId)!!
    }

    /**
     * 根据字典类型查询信息
     *
     * @param dictType 字典类型
     * @return 字典类型
     */
    override fun selectDictTypeByType(dictType: String?): SysDictType? {
        return dictTypeMapper!!.selectDictTypeByType(dictType)
    }

    /**
     * 批量删除字典类型信息
     *
     * @param dictIds 需要删除的字典ID
     */
    override fun deleteDictTypeByIds(dictIds: Array<Long>) {
        for (dictId in dictIds) {
            val dictType = selectDictTypeById(dictId)
            if (dictDataMapper!!.countDictDataByType(dictType.dictType) > 0) {
                throw ServiceException("${dictType.dictName}已分配,不能删除")
            }
            dictTypeMapper!!.deleteDictTypeById(dictId)
            DictUtils.removeDictCache(dictType.dictType!!)
        }
    }

    /**
     * 加载字典缓存数据
     */
    override fun loadingDictCache() {
        val dictData = SysDictData()
        dictData.status = "0"
        val selectDictDataList = dictDataMapper!!.selectDictDataList(dictData)
        selectDictDataList.groupBy { it.dictType!! }
            .forEach { (key, value) -> DictUtils.setDictCache(key, value.sortedBy { it.dictSort }) }
    }

    /**
     * 清空字典缓存数据
     */
    override fun clearDictCache() {
        DictUtils.clearDictCache()
    }

    /**
     * 重置字典缓存数据
     */
    override fun resetDictCache() {
        clearDictCache()
        loadingDictCache()
    }

    /**
     * 新增保存字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    override fun insertDictType(dictType: SysDictType): Int {
        val row = dictTypeMapper!!.insertDictType(dictType)
        if (row > 0) {
            DictUtils.setDictCache(dictType.dictType!!, listOf(null))
        }
        return row
    }

    /**
     * 修改保存字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    @Transactional
    override fun updateDictType(dictType: SysDictType): Int {
        val oldDict = dictTypeMapper!!.selectDictTypeById(dictType.dictId)
        dictDataMapper!!.updateDictDataType(oldDict!!.dictType, dictType.dictType)
        val row = dictTypeMapper.updateDictType(dictType)
        if (row > 0) {
            val dictDatas = dictDataMapper.selectDictDataByType(dictType.dictType)
            DictUtils.setDictCache(dictType.dictType!!, dictDatas!!)
        }
        return row
    }

    /**
     * 校验字典类型称是否唯一
     *
     * @param dict 字典类型
     * @return 结果
     */
    override fun checkDictTypeUnique(dict: SysDictType): String? {
        val dictId = if (StringUtils.isNull(dict.dictId)) -1L else dict.dictId
        val dictType = dictTypeMapper!!.checkDictTypeUnique(dict.dictType)
        return when {
            StringUtils.isNotNull(dictType) && dictType!!.dictId!!.toLong() != dictId!!.toLong() -> {
                UserConstants.NOT_UNIQUE
            }

            else -> UserConstants.UNIQUE
        }
    }
}
