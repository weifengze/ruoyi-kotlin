package com.ruoyi.project.system.service.impl

import com.ruoyi.common.constant.UserConstants
import com.ruoyi.common.exception.ServiceException
import com.ruoyi.common.utils.SecurityUtils
import com.ruoyi.common.utils.bean.BeanValidators
import com.ruoyi.common.utils.spring.SpringUtils
import com.ruoyi.framework.aspectj.lang.annotation.DataScope
import com.ruoyi.project.system.domain.*
import com.ruoyi.project.system.mapper.*
import com.ruoyi.project.system.service.ISysConfigService
import com.ruoyi.project.system.service.ISysUserService
import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.CollectionUtils
import java.util.stream.Collectors
import javax.validation.*

/**
 * 用户 业务层处理
 *
 * @author ruoyi
 */
@Service
class SysUserServiceImpl : ISysUserService {
    companion object {
        private val log = LoggerFactory.getLogger(SysUserServiceImpl::class.java)
    }

    @Autowired
    private val userMapper: SysUserMapper? = null

    @Autowired
    private val roleMapper: SysRoleMapper? = null

    @Autowired
    private val postMapper: SysPostMapper? = null

    @Autowired
    private val userRoleMapper: SysUserRoleMapper? = null

    @Autowired
    private val userPostMapper: SysUserPostMapper? = null

    @Autowired
    private val configService: ISysConfigService? = null

    @Autowired
    protected var validator: Validator? = null

    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @DataScope(deptAlias = "d", userAlias = "u")
    override fun selectUserList(user: SysUser?): List<SysUser>? {
        return userMapper!!.selectUserList(user)
    }

    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @DataScope(deptAlias = "d", userAlias = "u")
    override fun selectAllocatedList(user: SysUser?): List<SysUser>? {
        return userMapper!!.selectAllocatedList(user)
    }

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @DataScope(deptAlias = "d", userAlias = "u")
    override fun selectUnallocatedList(user: SysUser?): List<SysUser>? {
        return userMapper!!.selectUnallocatedList(user)
    }

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    override fun selectUserByUserName(userName: String?): SysUser? {
        return userMapper!!.selectUserByUserName(userName)
    }

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    override fun selectUserById(userId: Long?): SysUser? {
        return userMapper!!.selectUserById(userId)
    }

    /**
     * 查询用户所属角色组
     *
     * @param userName 用户名
     * @return 结果
     */
    override fun selectUserRoleGroup(userName: String?): String? {
        val list = roleMapper!!.selectRolesByUserName(userName)
        return if (list?.isEmpty()!!) {
            StringUtils.EMPTY
        } else list.map { it!!.roleName }.joinToString(",")
    }

    /**
     * 查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    override fun selectUserPostGroup(userName: String?): String? {
        val list = postMapper!!.selectPostsByUserName(userName)
        return if (list?.isEmpty()!!) {
            StringUtils.EMPTY
        } else list.map { it!!.postName }.joinToString(",")
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    override fun checkUserNameUnique(userName: String?): String? {
        val count = userMapper!!.checkUserNameUnique(userName)
        return if (count > 0) {
            UserConstants.NOT_UNIQUE
        } else UserConstants.UNIQUE
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return
     */
    override fun checkPhoneUnique(user: SysUser): String? {
        val userId = if (com.ruoyi.common.utils.StringUtils.isNull(user.userId)) -1L else user.userId
        val info = userMapper!!.checkPhoneUnique(user.phonenumber)
        return if (com.ruoyi.common.utils.StringUtils.isNotNull(info) && info!!.userId != userId!!.toLong()) {
            UserConstants.NOT_UNIQUE
        } else UserConstants.UNIQUE
    }

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return
     */
    override fun checkEmailUnique(user: SysUser): String? {
        val userId = if (com.ruoyi.common.utils.StringUtils.isNull(user.userId)) -1L else user.userId
        val info = userMapper!!.checkEmailUnique(user.email)
        return if (com.ruoyi.common.utils.StringUtils.isNotNull(info) && info!!.userId != userId) {
            UserConstants.NOT_UNIQUE
        } else UserConstants.UNIQUE
    }

    /**
     * 校验用户是否允许操作
     *
     * @param user 用户信息
     */
    override fun checkUserAllowed(user: SysUser) {
        if (com.ruoyi.common.utils.StringUtils.isNotNull(user.userId) && user.isAdmin) {
            throw ServiceException("不允许操作超级管理员用户")
        }
    }

    /**
     * 校验用户是否有数据权限
     *
     * @param userId 用户id
     */
    override fun checkUserDataScope(userId: Long?) {
        if (!SysUser.isAdmin(SecurityUtils.getUserId())) {
            val user = SysUser()
            user.userId = userId
            val users = SpringUtils.getAopProxy(this).selectUserList(user)
            if (com.ruoyi.common.utils.StringUtils.isEmpty(users)) {
                throw ServiceException("没有权限访问用户数据！")
            }
        }
    }

    /**
     * 新增保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Transactional
    override fun insertUser(user: SysUser): Int {
        // 新增用户信息
        val rows = userMapper!!.insertUser(user)
        // 新增用户岗位关联
        insertUserPost(user)
        // 新增用户与角色管理
        insertUserRole(user)
        return rows
    }

    /**
     * 注册用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    override fun registerUser(user: SysUser?): Boolean {
        return userMapper!!.insertUser(user) > 0
    }

    /**
     * 修改保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Transactional
    override fun updateUser(user: SysUser): Int {
        val userId = user.userId
        // 删除用户与角色关联
        userRoleMapper!!.deleteUserRoleByUserId(userId)
        // 新增用户与角色管理
        insertUserRole(user)
        // 删除用户与岗位关联
        userPostMapper!!.deleteUserPostByUserId(userId)
        // 新增用户与岗位管理
        insertUserPost(user)
        return userMapper!!.updateUser(user)
    }

    /**
     * 用户授权角色
     *
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    @Transactional
    override fun insertUserAuth(userId: Long?, roleIds: Array<Long>) {
        userRoleMapper!!.deleteUserRoleByUserId(userId)
        insertUserRole(userId, roleIds)
    }

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    override fun updateUserStatus(user: SysUser?): Int {
        return userMapper!!.updateUser(user)
    }

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    override fun updateUserProfile(user: SysUser?): Int {
        return userMapper!!.updateUser(user)
    }

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    override fun updateUserAvatar(userName: String?, avatar: String?): Boolean {
        return userMapper!!.updateUserAvatar(userName, avatar) > 0
    }

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    override fun resetPwd(user: SysUser?): Int {
        return userMapper!!.updateUser(user)
    }

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    override fun resetUserPwd(userName: String?, password: String?): Int {
        return userMapper!!.resetUserPwd(userName, password)
    }

    /**
     * 新增用户角色信息
     *
     * @param user 用户对象
     */
    fun insertUserRole(user: SysUser) {
        this.insertUserRole(user.userId, user.roleIds!!)
    }

    /**
     * 新增用户岗位信息
     *
     * @param user 用户对象
     */
    fun insertUserPost(user: SysUser) {
        val posts = user.postIds
        if (com.ruoyi.common.utils.StringUtils.isNotEmpty(listOf(posts))) {
            // 新增用户与岗位管理
            val list: MutableList<SysUserPost> = ArrayList(posts!!.size)
            for (postId in posts) {
                val up = SysUserPost()
                up.userId = user.userId
                up.postId = postId
                list.add(up)
            }
            userPostMapper!!.batchUserPost(list)
        }
    }

    /**
     * 新增用户角色信息
     *
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    fun insertUserRole(userId: Long?, roleIds: Array<Long>) {
        // 新增用户与角色管理
        val list: MutableList<SysUserRole> = ArrayList(roleIds.size)
        for (roleId in roleIds) {
            val ur = SysUserRole()
            ur.userId = userId
            ur.roleId = roleId
            list.add(ur)
        }
        userRoleMapper!!.batchUserRole(list)

    }

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    @Transactional
    override fun deleteUserById(userId: Long?): Int {
        // 删除用户与角色关联
        userRoleMapper!!.deleteUserRoleByUserId(userId)
        // 删除用户与岗位表
        userPostMapper!!.deleteUserPostByUserId(userId)
        return userMapper!!.deleteUserById(userId)
    }

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    @Transactional
    override fun deleteUserByIds(userIds: Array<Long?>): Int {
        for (userId in userIds) {
            checkUserAllowed(SysUser(userId))
            checkUserDataScope(userId)
        }
        // 删除用户与角色关联
        userRoleMapper!!.deleteUserRole(userIds)
        // 删除用户与岗位关联
        userPostMapper!!.deleteUserPost(userIds)
        return userMapper!!.deleteUserByIds(userIds)
    }

    /**
     * 导入用户数据
     *
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    override fun importUser(userList: List<SysUser?>, isUpdateSupport: Boolean, operName: String?): String {
        if (com.ruoyi.common.utils.StringUtils.isNull(userList) || userList.isEmpty()) {
            throw ServiceException("导入用户数据不能为空！")
        }
        var successNum = 0
        var failureNum = 0
        val successMsg = StringBuilder()
        val failureMsg = StringBuilder()
        val password = configService!!.selectConfigByKey("sys.user.initPassword")
        for (user in userList) {
            try {
                // 验证是否存在这个用户
                val u = userMapper!!.selectUserByUserName(user!!.userName)
                if (com.ruoyi.common.utils.StringUtils.isNull(u)) {
                    BeanValidators.validateWithException(validator, user)
                    user.password = SecurityUtils.encryptPassword(password)
                    user.createBy = operName
                    insertUser(user)
                    successNum++
                    successMsg.append("<br/>" + successNum + "、账号 " + user.userName + " 导入成功")
                } else if (isUpdateSupport) {
                    BeanValidators.validateWithException(validator, user)
                    user.updateBy = operName
                    updateUser(user)
                    successNum++
                    successMsg.append("<br/>" + successNum + "、账号 " + user.userName + " 更新成功")
                } else {
                    failureNum++
                    failureMsg.append("<br/>" + failureNum + "、账号 " + user.userName + " 已存在")
                }
            } catch (e: Exception) {
                failureNum++
                val msg = "<br/>" + failureNum + "、账号 " + user!!.userName + " 导入失败："
                failureMsg.append(msg + e.message)
                log.error(msg, e)
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 $failureNum 条数据格式不正确，错误如下：")
            throw ServiceException(failureMsg.toString())
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 $successNum 条，数据如下：")
        }
        return successMsg.toString()
    }
}
