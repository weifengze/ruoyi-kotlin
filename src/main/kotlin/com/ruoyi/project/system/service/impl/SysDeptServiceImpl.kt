package com.ruoyi.project.system.service.impl

import com.alibaba.fastjson2.JSON
import com.ruoyi.common.constant.UserConstants
import com.ruoyi.common.core.text.Convert
import com.ruoyi.common.exception.ServiceException
import com.ruoyi.common.utils.*
import com.ruoyi.common.utils.spring.SpringUtils
import com.ruoyi.framework.aspectj.lang.annotation.DataScope
import com.ruoyi.framework.web.domain.TreeSelect
import com.ruoyi.project.system.domain.SysDept
import com.ruoyi.project.system.domain.SysUser
import com.ruoyi.project.system.mapper.SysDeptMapper
import com.ruoyi.project.system.mapper.SysRoleMapper
import com.ruoyi.project.system.service.ISysDeptService
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.stream.Collectors

/**
 * 部门管理 服务实现
 *
 * @author ruoyi
 */
@Service
class SysDeptServiceImpl : ISysDeptService {
    @Autowired
    private val deptMapper: SysDeptMapper? = null

    @Autowired
    private val roleMapper: SysRoleMapper? = null

    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    @DataScope(deptAlias = "d")
    override fun selectDeptList(dept: SysDept?): List<SysDept?> {
        return deptMapper!!.selectDeptList(dept)
    }

    /**
     * 查询部门树结构信息
     *
     * @param dept 部门信息
     * @return 部门树信息集合
     */
    override fun selectDeptTreeList(dept: SysDept?): List<TreeSelect> {
        val buildDeptTreeSelect = buildDeptTreeSelect(deptMapper!!.selectDeptList(dept))
        println(JSON.toJSONString(buildDeptTreeSelect))
        return buildDeptTreeSelect
    }

    /**
     * 构建前端所需要树结构
     *
     * @param depts 部门列表
     * @return 树结构列表
     */
    override fun buildDeptTree(depts: List<SysDept>): List<SysDept> {
        var returnList: MutableList<SysDept> = ArrayList()
        val tempList: MutableList<Long> = ArrayList()
        for (dept in depts) {
            tempList.add(dept.deptId!!)
        }
        for (dept in depts) {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dept.parentId)) {
                recursionFn(depts, dept)
                returnList.add(dept)
            }
        }
        if (returnList.isEmpty()) {
            returnList = depts.toMutableList()
        }
        return returnList
    }

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    override fun buildDeptTreeSelect(depts: List<SysDept>): List<TreeSelect> {
        val deptTrees = buildDeptTree(depts)
        println(JSON.toJSONString(deptTrees.map { TreeSelect(it) }.toList()))
        return deptTrees.stream().map { dept: SysDept? -> TreeSelect(dept!!) }.collect(Collectors.toList())
    }

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    override fun selectDeptListByRoleId(roleId: Long?): List<Long?>? {
        val role = roleMapper!!.selectRoleById(roleId)!!
        return deptMapper!!.selectDeptListByRoleId(roleId, role.isDeptCheckStrictly)
    }

    /**
     * 根据部门ID查询信息
     *
     * @param deptId 部门ID
     * @return 部门信息
     */
    override fun selectDeptById(deptId: Long?): SysDept? {
        return deptMapper!!.selectDeptById(deptId)
    }

    /**
     * 根据ID查询所有子部门（正常状态）
     *
     * @param deptId 部门ID
     * @return 子部门数
     */
    override fun selectNormalChildrenDeptById(deptId: Long?): Int {
        return deptMapper!!.selectNormalChildrenDeptById(deptId)
    }

    /**
     * 是否存在子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    override fun hasChildByDeptId(deptId: Long?): Boolean {
        val result = deptMapper!!.hasChildByDeptId(deptId)
        return result > 0
    }

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    override fun checkDeptExistUser(deptId: Long?): Boolean {
        val result = deptMapper!!.checkDeptExistUser(deptId)
        return result > 0
    }

    /**
     * 校验部门名称是否唯一
     *
     * @param dept 部门信息
     * @return 结果
     */
    override fun checkDeptNameUnique(dept: SysDept): String {
        val deptId = if (com.ruoyi.common.utils.StringUtils.isNull(dept.deptId)) -1L else dept.deptId
        val info = deptMapper!!.checkDeptNameUnique(dept.deptName, dept.parentId)!!
        return if (com.ruoyi.common.utils.StringUtils.isNotNull(info) && info.deptId!!.toLong() != deptId!!.toLong()) {
            UserConstants.NOT_UNIQUE
        } else UserConstants.UNIQUE
    }

    /**
     * 校验部门是否有数据权限
     *
     * @param deptId 部门id
     */
    override fun checkDeptDataScope(deptId: Long?) {
        if (!SysUser.isAdmin(SecurityUtils.getUserId())) {
            val dept = SysDept()
            dept.deptId = deptId
            val depts = SpringUtils.getAopProxy(this).selectDeptList(dept)
            if (depts.isEmpty()) {
                throw ServiceException("没有权限访问部门数据！")
            }
        }
    }

    /**
     * 新增保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    override fun insertDept(dept: SysDept): Int {
        val info = deptMapper!!.selectDeptById(dept.parentId)!!
        // 如果父节点不为正常状态,则不允许新增子节点
        if (UserConstants.DEPT_NORMAL != info.status) {
            throw ServiceException("部门停用，不允许新增")
        }
        dept.ancestors = info.ancestors + "," + dept.parentId
        return deptMapper.insertDept(dept)
    }

    /**
     * 修改保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    override fun updateDept(dept: SysDept): Int {
        val newParentDept = deptMapper!!.selectDeptById(dept.parentId)
        val oldDept = deptMapper.selectDeptById(dept.deptId)
        if (com.ruoyi.common.utils.StringUtils.isNotNull(newParentDept) && com.ruoyi.common.utils.StringUtils.isNotNull(
                oldDept
            )
        ) {
            val newAncestors = newParentDept?.ancestors + "," + (newParentDept?.deptId)
            val oldAncestors = oldDept?.ancestors!!
            dept.ancestors = newAncestors
            updateDeptChildren(dept.deptId, newAncestors, oldAncestors)
        }
        val result = deptMapper.updateDept(dept)
        if (UserConstants.DEPT_NORMAL == dept.status && com.ruoyi.common.utils.StringUtils.isNotEmpty(dept.ancestors)
            && !StringUtils.equals("0", dept.ancestors)
        ) {
            // 如果该部门是启用状态，则启用该部门的所有上级部门
            updateParentDeptStatusNormal(dept)
        }
        return result
    }

    /**
     * 修改该部门的父级部门状态
     *
     * @param dept 当前部门
     */
    private fun updateParentDeptStatusNormal(dept: SysDept) {
        val ancestors = dept.ancestors!!
        val deptIds = Convert.toLongArray(ancestors)
        deptMapper!!.updateDeptStatusNormal(deptIds)
    }

    /**
     * 修改子元素关系
     *
     * @param deptId 被修改的部门ID
     * @param newAncestors 新的父ID集合
     * @param oldAncestors 旧的父ID集合
     */
    fun updateDeptChildren(deptId: Long?, newAncestors: String?, oldAncestors: String) {
        val children = deptMapper!!.selectChildrenDeptById(deptId)!!
        children.forEach { child ->
            child!!.ancestors = child.ancestors!!.replaceFirst(oldAncestors.toRegex(), newAncestors!!)
        }
        if (children.isNotEmpty()) {
            deptMapper.updateDeptChildren(children)
        }
    }

    /**
     * 删除部门管理信息
     *
     * @param deptId 部门ID
     * @return 结果
     */
    override fun deleteDeptById(deptId: Long?): Int {
        return deptMapper!!.deleteDeptById(deptId)
    }

    /**
     * 递归列表
     */
    private fun recursionFn(list: List<SysDept>, t: SysDept) {
        // 得到子节点列表
        val childList = getChildList(list, t)
        t.children = childList
        childList
            .asSequence()
            .filter { hasChild(list, it) }
            .forEach { recursionFn(list, it) }
    }

    /**
     * 得到子节点列表
     */
    private fun getChildList(list: List<SysDept>, t: SysDept): List<SysDept> {
        val tlist: MutableList<SysDept> = ArrayList()
        val it = list.iterator()
        while (it.hasNext()) {
            val n = it.next()
            if (com.ruoyi.common.utils.StringUtils.isNotNull(n.parentId) && n.parentId!!.toLong() == t.deptId!!.toLong()) {
                tlist.add(n)
            }
        }
        return tlist
    }

    /**
     * 判断是否有子节点
     */
    private fun hasChild(list: List<SysDept>, t: SysDept): Boolean {
        return getChildList(list, t).isNotEmpty()
    }
}
