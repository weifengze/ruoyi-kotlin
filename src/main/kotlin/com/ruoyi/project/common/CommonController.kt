package com.ruoyi.project.commonimport

import com.ruoyi.common.constant.*
import com.ruoyi.common.utils.*
import com.ruoyi.common.utils.file.FileUploadUtils
import com.ruoyi.common.utils.file.FileUtils
import com.ruoyi.framework.config.*
import com.ruoyi.framework.web.domain.AjaxResult
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * 通用请求处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/common")
open class CommonController {

    companion object {
        private val log = LoggerFactory.getLogger(CommonController::class.java)
        private const val FILE_DELIMETER = ","
    }

    @Autowired
    private val serverConfig: ServerConfig? = null

    /**
     * 通用下载请求
     *
     * @param fileName 文件名称
     * @param delete 是否删除
     */
    @GetMapping("/download")
    fun fileDownload(fileName: String, delete: Boolean, response: HttpServletResponse, request: HttpServletRequest?) {
        try {
            if (!FileUtils.checkAllowDownload(fileName)) {
                throw Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName))
            }
            val realFileName = System.currentTimeMillis().toString() + fileName.substring(fileName.indexOf("_") + 1)
            val filePath: String = RuoYiConfig.getDownloadPath() + fileName
            response.contentType = MediaType.APPLICATION_OCTET_STREAM_VALUE
            FileUtils.setAttachmentResponseHeader(response, realFileName)
            FileUtils.writeBytes(filePath, response.outputStream)
            if (delete) {
                FileUtils.deleteFile(filePath)
            }
        } catch (e: Exception) {
            log.error("下载文件失败", e)
        }
    }

    /**
     * 通用上传请求（单个）
     */
    @PostMapping("/upload")
    @Throws(Exception::class)
    fun uploadFile(file: MultipartFile): AjaxResult {
        return try {
            // 上传文件路径
            val filePath: String = RuoYiConfig.getUploadPath()
            // 上传并返回新文件名称
            val fileName = FileUploadUtils.upload(filePath, file)
            val url = serverConfig!!.url + fileName
            val ajax: AjaxResult = AjaxResult.success()
            ajax["url"] = url
            ajax["fileName"] = fileName
            ajax["newFileName"] = FileUtils.getName(fileName)
            ajax["originalFilename"] = file.originalFilename
            ajax
        } catch (e: Exception) {
            AjaxResult.error(e.message)
        }
    }

    /**
     * 通用上传请求（多个）
     */
    @PostMapping("/uploads")
    @Throws(Exception::class)
    fun uploadFiles(files: List<MultipartFile>): AjaxResult {
        return try {
            // 上传文件路径
            val filePath: String = RuoYiConfig.Companion.getUploadPath()
            val urls: MutableList<String> = ArrayList()
            val fileNames: MutableList<String?> = ArrayList()
            val newFileNames: MutableList<String?> = ArrayList()
            val originalFilenames: MutableList<String> = ArrayList()
            for (file in files) {
                // 上传并返回新文件名称
                val fileName = FileUploadUtils.upload(filePath, file)
                val url = serverConfig!!.url + fileName
                urls.add(url)
                fileNames.add(fileName)
                newFileNames.add(FileUtils.getName(fileName))
                originalFilenames.add(file.originalFilename!!)
            }
            val ajax: AjaxResult = AjaxResult.success()
            ajax["urls"] =  org.apache.commons.lang3.StringUtils.join(urls, FILE_DELIMETER)
            ajax["fileNames"] =  org.apache.commons.lang3.StringUtils.join(fileNames, FILE_DELIMETER)
            ajax["newFileNames"] =  org.apache.commons.lang3.StringUtils.join(newFileNames, FILE_DELIMETER)
            ajax["originalFilenames"] =  org.apache.commons.lang3.StringUtils.join(originalFilenames, FILE_DELIMETER)
            ajax
        } catch (e: Exception) {
            AjaxResult.error(e.message)
        }
    }

    /**
     * 本地资源通用下载
     */
    @GetMapping("/download/resource")
    @Throws(Exception::class)
    fun resourceDownload(resource: String, request: HttpServletRequest?, response: HttpServletResponse) {
        try {
            if (!FileUtils.checkAllowDownload(resource)) {
                throw Exception(StringUtils.format("资源文件({})非法，不允许下载。 ", resource))
            }
            // 本地资源路径
            val localPath: String? = RuoYiConfig.getProfile()
            // 数据库资源地址
            val downloadPath = localPath +  org.apache.commons.lang3.StringUtils.substringAfter(resource, Constants.RESOURCE_PREFIX)
            // 下载名称
            val downloadName =  org.apache.commons.lang3.StringUtils.substringAfterLast(downloadPath, "/")
            response.contentType = MediaType.APPLICATION_OCTET_STREAM_VALUE
            FileUtils.setAttachmentResponseHeader(response, downloadName)
            FileUtils.writeBytes(downloadPath, response.outputStream)
        } catch (e: Exception) {
            log.error("下载文件失败", e)
        }
    }

}
