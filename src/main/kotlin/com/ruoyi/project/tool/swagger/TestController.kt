package com.ruoyi.project.tool.swagger

import com.ruoyi.common.utils.*
import com.ruoyi.framework.web.controller.BaseController
import com.ruoyi.framework.web.domain.R
import io.swagger.annotations.*
import org.springframework.web.bind.annotation.*

/**
 * swagger 用户测试方法
 *
 * @author ruoyi
 */
@Api("用户信息管理")
@RestController
@RequestMapping("/test/user")
class TestController : BaseController() {

    companion object {
        private val users: MutableMap<Int, UserEntity> = LinkedHashMap()
    }

    init {
        users[1] = UserEntity(1, "admin", "admin123", "15888888888")
        users[2] = UserEntity(2, "ry", "admin123", "15666666666")
    }

    @ApiOperation("获取用户列表")
    @GetMapping("/list")
    fun userList(): R<List<UserEntity>> {
        val userList: List<UserEntity> = ArrayList<UserEntity>(users.values)
        return R.ok(userList)
    }

    @ApiOperation("获取用户详细")
    @ApiImplicitParam(
        name = "userId",
        value = "用户ID",
        required = true,
        dataType = "int",
        paramType = "path",
        dataTypeClass = Int::class
    )
    @GetMapping("/{userId}")
    fun getUser(@PathVariable userId: Int?): R<UserEntity> {
        return if (users.isNotEmpty() && users.containsKey(userId)) {
            R.ok(users[userId]!!)
        } else {
            R.fail("用户不存在")
        }
    }

    @ApiOperation("新增用户")
    @ApiImplicitParams(
        ApiImplicitParam(
            name = "userId",
            value = "用户id",
            dataType = "Integer",
            dataTypeClass = Int::class
        ),
        ApiImplicitParam(name = "username", value = "用户名称", dataType = "String", dataTypeClass = String::class),
        ApiImplicitParam(name = "password", value = "用户密码", dataType = "String", dataTypeClass = String::class),
        ApiImplicitParam(name = "mobile", value = "用户手机", dataType = "String", dataTypeClass = String::class)
    )
    @PostMapping("/save")
    fun save(user: UserEntity): R<String> {
        if (StringUtils.isNull(user) || StringUtils.isNull(user.userId)) {
            return R.fail("用户ID不能为空")
        }
        users[user.userId!!] = user
        return R.ok()
    }

    @ApiOperation("更新用户")
    @PutMapping("/update")
    fun update(@RequestBody user: UserEntity): R<String> {
        if (StringUtils.isNull(user) || StringUtils.isNull(user.userId)) {
            return R.fail("用户ID不能为空")
        }
        if (users.isEmpty() || !users.containsKey(user.userId)) {
            return R.fail("用户不存在")
        }
        users.remove(user.userId)
        users[user.userId!!] = user
        return R.ok()
    }

    @ApiOperation("删除用户信息")
    @ApiImplicitParam(
        name = "userId",
        value = "用户ID",
        required = true,
        dataType = "int",
        paramType = "path",
        dataTypeClass = Int::class
    )
    @DeleteMapping("/{userId}")
    fun delete(@PathVariable userId: Int?): R<String> {
        return if (users.isNotEmpty() && users.containsKey(userId)) {
            users.remove(userId)
            R.ok()
        } else {
            R.fail("用户不存在")
        }
    }
}

@ApiModel(value = "UserEntity", description = "用户实体")
class UserEntity(
    @ApiModelProperty("用户ID") var userId: Int?,
    @ApiModelProperty("用户名称") var username: String?,
    @ApiModelProperty("用户密码") var password: String?,
    @ApiModelProperty("用户手机") var mobile: String?
) {
}
