package com.ruoyi.project.monitor.domain

import com.ruoyi.framework.aspectj.lang.annotation.Excel
import com.ruoyi.framework.web.domain.BaseEntity
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle
import java.util.*

/**
 * 定时任务调度日志表 sys_job_log
 *
 * @author ruoyi
 */
open class SysJobLog : BaseEntity() {
    /** ID  */
    @Excel(name = "日志序号")
    var jobLogId: Long? = null

    /** 任务名称  */
    @Excel(name = "任务名称")
    var jobName: String? = null

    /** 任务组名  */
    @Excel(name = "任务组名")
    var jobGroup: String? = null

    /** 调用目标字符串  */
    @Excel(name = "调用目标字符串")
    var invokeTarget: String? = null

    /** 日志信息  */
    @Excel(name = "日志信息")
    var jobMessage: String? = null

    /** 执行状态（0正常 1失败）  */
    @Excel(name = "执行状态", readConverterExp = "0=正常,1=失败")
    var status: String? = null

    /** 异常信息  */
    @Excel(name = "异常信息")
    var exceptionInfo: String? = null

    /** 开始时间  */
    var startTime: Date? = null

    /** 停止时间  */
    var stopTime: Date? = null
    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("jobLogId", jobLogId)
            .append("jobName", jobName)
            .append("jobGroup", jobGroup)
            .append("jobMessage", jobMessage)
            .append("status", status)
            .append("exceptionInfo", exceptionInfo)
            .append("startTime", startTime)
            .append("stopTime", stopTime)
            .toString()
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
