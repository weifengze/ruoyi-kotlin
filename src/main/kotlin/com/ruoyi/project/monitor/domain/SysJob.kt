package com.ruoyi.project.monitor.domain

import com.fasterxml.jackson.annotation.JsonFormat
import com.ruoyi.common.constant.ScheduleConstants
import com.ruoyi.common.utils.job.CronUtils
import com.ruoyi.framework.aspectj.lang.annotation.Excel
import com.ruoyi.framework.web.domain.BaseEntity
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle
import java.util.*

/**
 * 定时任务调度表 sys_job
 *
 * @author ruoyi
 */
open class SysJob : BaseEntity() {
    /** 任务ID  */
    @Excel(name = "任务序号", cellType = Excel.ColumnType.NUMERIC)
    var jobId: Long? = null

    /** 任务名称  */
    @Excel(name = "任务名称")
    var jobName: String? = null

    /** 任务组名  */
    @Excel(name = "任务组名")
    var jobGroup: String? = null

    /** 调用目标字符串  */
    @Excel(name = "调用目标字符串")
    var invokeTarget: String? = null

    /** cron执行表达式  */
    @Excel(name = "执行表达式 ")
    var cronExpression: String? = null

    /** cron计划策略  */
    @Excel(name = "计划策略 ", readConverterExp = "0=默认,1=立即触发执行,2=触发一次执行,3=不触发立即执行")
    var misfirePolicy = ScheduleConstants.MISFIRE_DEFAULT

    /** 是否并发执行（0允许 1禁止）  */
    @Excel(name = "并发执行", readConverterExp = "0=允许,1=禁止")
    var concurrent: String? = null

    /** 任务状态（0正常 1暂停）  */
    @Excel(name = "任务状态", readConverterExp = "0=正常,1=暂停")
    var status: String? = null

    @get:JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    val nextValidTime: Date?
        get() = if (StringUtils.isNotEmpty(cronExpression)) {
            CronUtils.getNextExecution(cronExpression)
        } else null

    override fun toString(): String {
        return ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("jobId", jobId)
            .append("jobName", jobName)
            .append("jobGroup", jobGroup)
            .append("cronExpression", cronExpression)
            .append("nextValidTime", nextValidTime)
            .append("misfirePolicy", misfirePolicy)
            .append("concurrent", concurrent)
            .append("status", status)
            .append("createBy", createBy)
            .append("createTime", createTime)
            .append("updateBy", updateBy)
            .append("updateTime", updateTime)
            .append("remark", remark)
            .toString()
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
