package com.ruoyi.project.monitor.domain

import com.fasterxml.jackson.annotation.JsonFormat
import com.ruoyi.framework.aspectj.lang.annotation.Excel
import com.ruoyi.framework.web.domain.BaseEntity
import java.util.*

/**
 * 系统访问记录表 sys_logininfor
 *
 * @author ruoyi
 */
open class SysLogininfor : BaseEntity() {
    /** ID  */
    @Excel(name = "序号", cellType = Excel.ColumnType.NUMERIC)
    var infoId: Long? = null

    /** 用户账号  */
    @Excel(name = "用户账号")
    var userName: String? = null

    /** 登录状态 0成功 1失败  */
    @Excel(name = "登录状态", readConverterExp = "0=成功,1=失败")
    var status: String? = null

    /** 登录IP地址  */
    @Excel(name = "登录地址")
    var ipaddr: String? = null

    /** 登录地点  */
    @Excel(name = "登录地点")
    var loginLocation: String? = null

    /** 浏览器类型  */
    @Excel(name = "浏览器")
    var browser: String? = null

    /** 操作系统  */
    @Excel(name = "操作系统")
    var os: String? = null

    /** 提示消息  */
    @Excel(name = "提示消息")
    var msg: String? = null

    /** 访问时间  */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "访问时间", width = 30.toDouble(), dateFormat = "yyyy-MM-dd HH:mm:ss")
    var loginTime: Date? = null

    companion object {
        private const val serialVersionUID = 1L
    }
}
