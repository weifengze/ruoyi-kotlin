package com.ruoyi.project.monitor.service.impl

import com.ruoyi.project.monitor.domain.SysLogininfor
import com.ruoyi.project.monitor.mapper.SysLogininforMapper
import com.ruoyi.project.monitor.service.ISysLogininforService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * 系统访问日志情况信息 服务层处理
 *
 * @author ruoyi
 */
@Service
open class SysLogininforServiceImpl : ISysLogininforService {
    @Autowired
    private val logininforMapper: SysLogininforMapper? = null

    /**
     * 新增系统登录日志
     *
     * @param logininfor 访问日志对象
     */
    override fun insertLogininfor(logininfor: SysLogininfor?) {
        logininforMapper!!.insertLogininfor(logininfor)
    }

    /**
     * 查询系统登录日志集合
     *
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    override fun selectLogininforList(logininfor: SysLogininfor?): List<SysLogininfor>? {
        return logininforMapper!!.selectLogininforList(logininfor)
    }

    /**
     * 批量删除系统登录日志
     *
     * @param infoIds 需要删除的登录日志ID
     * @return 结果
     */
    override fun deleteLogininforByIds(infoIds: Array<Long?>?): Int {
        return logininforMapper!!.deleteLogininforByIds(infoIds)
    }

    /**
     * 清空系统登录日志
     */
    override fun cleanLogininfor() {
        logininforMapper!!.cleanLogininfor()
    }
}
