package com.ruoyi.project.monitor.service.impl

import com.ruoyi.project.monitor.domain.SysJobLog
import com.ruoyi.project.monitor.mapper.SysJobLogMapper
import com.ruoyi.project.monitor.service.ISysJobLogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * 定时任务调度日志信息 服务层
 *
 * @author ruoyi
 */
@Service
open class SysJobLogServiceImpl : ISysJobLogService {
    @Autowired
    private val jobLogMapper: SysJobLogMapper? = null

    /**
     * 获取quartz调度器日志的计划任务
     *
     * @param jobLog 调度日志信息
     * @return 调度任务日志集合
     */
    override fun selectJobLogList(jobLog: SysJobLog?): List<SysJobLog>? {
        return jobLogMapper!!.selectJobLogList(jobLog)
    }

    /**
     * 通过调度任务日志ID查询调度信息
     *
     * @param jobLogId 调度任务日志ID
     * @return 调度任务日志对象信息
     */
    override fun selectJobLogById(jobLogId: Long?): SysJobLog? {
        return jobLogMapper!!.selectJobLogById(jobLogId)
    }

    /**
     * 新增任务日志
     *
     * @param jobLog 调度日志信息
     */
    override fun addJobLog(jobLog: SysJobLog?) {
        jobLogMapper!!.insertJobLog(jobLog)
    }

    /**
     * 批量删除调度日志信息
     *
     * @param logIds 需要删除的数据ID
     * @return 结果
     */
    override fun deleteJobLogByIds(logIds: Array<Long?>?): Int {
        return jobLogMapper!!.deleteJobLogByIds(logIds)
    }

    /**
     * 删除任务日志
     *
     * @param jobId 调度日志ID
     */
    override fun deleteJobLogById(jobId: Long?): Int {
        return jobLogMapper!!.deleteJobLogById(jobId)
    }

    /**
     * 清空任务日志
     */
    override fun cleanJobLog() {
        jobLogMapper!!.cleanJobLog()
    }
}
