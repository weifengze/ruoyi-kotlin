package com.ruoyi.project.monitor.service

import com.ruoyi.common.exception.job.TaskException
import com.ruoyi.project.monitor.domain.SysJob
import org.quartz.SchedulerException

/**
 * 定时任务调度信息信息 服务层
 *
 * @author ruoyi
 */
interface ISysJobService {
    /**
     * 获取quartz调度器的计划任务
     *
     * @param job 调度信息
     * @return 调度任务集合
     */
    fun selectJobList(job: SysJob?): List<SysJob>

    /**
     * 通过调度任务ID查询调度信息
     *
     * @param jobId 调度任务ID
     * @return 调度任务对象信息
     */
    fun selectJobById(jobId: Long?): SysJob

    /**
     * 暂停任务
     *
     * @param job 调度信息
     * @return 结果
     */
    @Throws(SchedulerException::class)
    fun pauseJob(job: SysJob): Int

    /**
     * 恢复任务
     *
     * @param job 调度信息
     * @return 结果
     */
    @Throws(SchedulerException::class)
    fun resumeJob(job: SysJob): Int

    /**
     * 删除任务后，所对应的trigger也将被删除
     *
     * @param job 调度信息
     * @return 结果
     */
    @Throws(SchedulerException::class)
    fun deleteJob(job: SysJob): Int

    /**
     * 批量删除调度信息
     *
     * @param jobIds 需要删除的任务ID
     * @return 结果
     */
    @Throws(SchedulerException::class)
    fun deleteJobByIds(jobIds: Array<Long?>)

    /**
     * 任务调度状态修改
     *
     * @param job 调度信息
     * @return 结果
     */
    @Throws(SchedulerException::class)
    fun changeStatus(job: SysJob): Int

    /**
     * 立即运行任务
     *
     * @param job 调度信息
     * @return 结果
     */
    @Throws(SchedulerException::class)
    fun run(job: SysJob): Boolean

    /**
     * 新增任务
     *
     * @param job 调度信息
     * @return 结果
     */
    @Throws(SchedulerException::class, TaskException::class)
    fun insertJob(job: SysJob): Int

    /**
     * 更新任务
     *
     * @param job 调度信息
     * @return 结果
     */
    @Throws(SchedulerException::class, TaskException::class)
    fun updateJob(job: SysJob): Int

    /**
     * 校验cron表达式是否有效
     *
     * @param cronExpression 表达式
     * @return 结果
     */
    fun checkCronExpressionIsValid(cronExpression: String?): Boolean
}
