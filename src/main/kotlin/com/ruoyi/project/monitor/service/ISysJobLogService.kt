package com.ruoyi.project.monitor.service

import com.ruoyi.project.monitor.domain.SysJobLog

/**
 * 定时任务调度日志信息信息 服务层
 *
 * @author ruoyi
 */
interface ISysJobLogService {
    /**
     * 获取quartz调度器日志的计划任务
     *
     * @param jobLog 调度日志信息
     * @return 调度任务日志集合
     */
    fun selectJobLogList(jobLog: SysJobLog?): List<SysJobLog>?

    /**
     * 通过调度任务日志ID查询调度信息
     *
     * @param jobLogId 调度任务日志ID
     * @return 调度任务日志对象信息
     */
    fun selectJobLogById(jobLogId: Long?): SysJobLog?

    /**
     * 新增任务日志
     *
     * @param jobLog 调度日志信息
     */
    fun addJobLog(jobLog: SysJobLog?)

    /**
     * 批量删除调度日志信息
     *
     * @param logIds 需要删除的日志ID
     * @return 结果
     */
    fun deleteJobLogByIds(logIds: Array<Long?>?): Int

    /**
     * 删除任务日志
     *
     * @param jobId 调度日志ID
     * @return 结果
     */
    fun deleteJobLogById(jobId: Long?): Int

    /**
     * 清空任务日志
     */
    fun cleanJobLog()
}
