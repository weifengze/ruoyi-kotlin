package com.ruoyi.project.monitor.service.implimport

import com.ruoyi.common.constant.ScheduleConstants
import com.ruoyi.common.exception.job.TaskException
import com.ruoyi.common.utils.job.CronUtils
import com.ruoyi.common.utils.job.ScheduleUtils
import com.ruoyi.project.monitor.domain.SysJob
import com.ruoyi.project.monitor.mapper.SysJobMapper
import com.ruoyi.project.monitor.service.ISysJobService
import org.quartz.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import javax.annotation.PostConstruct

/**
 * 定时任务调度信息 服务层
 *
 * @author ruoyi
 */
@Service
open class SysJobServiceImpl : ISysJobService {
    @Autowired
    private val scheduler: Scheduler? = null

    @Autowired
    private val jobMapper: SysJobMapper? = null

    /**
     * 项目启动时，初始化定时器 主要是防止手动修改数据库导致未同步到定时任务处理（注：不能手动修改数据库ID和任务组名，否则会导致脏数据）
     */
    @PostConstruct
    @Throws(SchedulerException::class, TaskException::class)
    fun init() {
        scheduler!!.clear()
        val jobList = jobMapper!!.selectJobAll()
        for (job in jobList) {
            ScheduleUtils.createScheduleJob(scheduler, job)
        }
    }

    /**
     * 获取quartz调度器的计划任务列表
     *
     * @param job 调度信息
     * @return
     */
    override fun selectJobList(job: SysJob?): List<SysJob> {
        return jobMapper!!.selectJobList(job)
    }

    /**
     * 通过调度任务ID查询调度信息
     *
     * @param jobId 调度任务ID
     * @return 调度任务对象信息
     */
    override fun selectJobById(jobId: Long?): SysJob {
        return jobMapper!!.selectJobById(jobId)
    }

    /**
     * 暂停任务
     *
     * @param job 调度信息
     */
    @Transactional(rollbackFor = [Exception::class])
    @Throws(SchedulerException::class)
    override fun pauseJob(job: SysJob): Int {
        val jobId = job.jobId
        val jobGroup = job.jobGroup
        job.status = ScheduleConstants.Status.PAUSE.value
        val rows = jobMapper!!.updateJob(job)
        if (rows > 0) {
            scheduler!!.pauseJob(ScheduleUtils.getJobKey(jobId!!, jobGroup))
        }
        return rows
    }

    /**
     * 恢复任务
     *
     * @param job 调度信息
     */
    @Transactional(rollbackFor = [Exception::class])
    @Throws(SchedulerException::class)
    override fun resumeJob(job: SysJob): Int {
        val jobId = job.jobId
        val jobGroup = job.jobGroup
        job.status = ScheduleConstants.Status.NORMAL.value
        val rows = jobMapper!!.updateJob(job)
        if (rows > 0) {
            scheduler!!.resumeJob(ScheduleUtils.getJobKey(jobId!!, jobGroup))
        }
        return rows
    }

    /**
     * 删除任务后，所对应的trigger也将被删除
     *
     * @param job 调度信息
     */
    @Transactional(rollbackFor = [Exception::class])
    @Throws(SchedulerException::class)
    override fun deleteJob(job: SysJob): Int {
        val jobId = job.jobId
        val jobGroup = job.jobGroup
        val rows = jobMapper!!.deleteJobById(jobId)
        if (rows > 0) {
            scheduler!!.deleteJob(ScheduleUtils.getJobKey(jobId!!, jobGroup))
        }
        return rows
    }

    /**
     * 批量删除调度信息
     *
     * @param jobIds 需要删除的任务ID
     * @return 结果
     */
    @Transactional(rollbackFor = [Exception::class])
    @Throws(SchedulerException::class)
    override fun deleteJobByIds(jobIds: Array<Long?>) {
        for (jobId in jobIds) {
            val job = jobMapper!!.selectJobById(jobId)
            deleteJob(job)
        }
    }

    /**
     * 任务调度状态修改
     *
     * @param job 调度信息
     */
    @Transactional(rollbackFor = [Exception::class])
    @Throws(SchedulerException::class)
    override fun changeStatus(job: SysJob): Int {
        var rows = 0
        val status = job.status
        if (ScheduleConstants.Status.NORMAL.value == status) {
            rows = resumeJob(job)
        } else if (ScheduleConstants.Status.PAUSE.value == status) {
            rows = pauseJob(job)
        }
        return rows
    }

    /**
     * 立即运行任务
     *
     * @param job 调度信息
     */
    @Transactional(rollbackFor = [Exception::class])
    @Throws(SchedulerException::class)
    override fun run(job: SysJob): Boolean {
        var result = false
        val jobId = job.jobId
        val jobGroup = job.jobGroup
        val properties = selectJobById(job.jobId)
        // 参数
        val dataMap = JobDataMap()
        dataMap[ScheduleConstants.TASK_PROPERTIES] = properties
        val jobKey = ScheduleUtils.getJobKey(jobId!!, jobGroup)
        if (scheduler!!.checkExists(jobKey)) {
            result = true
            scheduler.triggerJob(jobKey, dataMap)
        }
        return result
    }

    /**
     * 新增任务
     *
     * @param job 调度信息 调度信息
     */
    @Transactional(rollbackFor = [Exception::class])
    @Throws(SchedulerException::class, TaskException::class)
    override fun insertJob(job: SysJob): Int {
        job.status = ScheduleConstants.Status.PAUSE.value
        val rows = jobMapper!!.insertJob(job)
        if (rows > 0) {
            ScheduleUtils.createScheduleJob(scheduler, job)
        }
        return rows
    }

    /**
     * 更新任务的时间表达式
     *
     * @param job 调度信息
     */
    @Transactional(rollbackFor = [Exception::class])
    @Throws(SchedulerException::class, TaskException::class)
    override fun updateJob(job: SysJob): Int {
        val properties = selectJobById(job.jobId)
        val rows = jobMapper!!.updateJob(job)
        if (rows > 0) {
            updateSchedulerJob(job, properties.jobGroup)
        }
        return rows
    }

    /**
     * 更新任务
     *
     * @param job 任务对象
     * @param jobGroup 任务组名
     */
    @Throws(SchedulerException::class, TaskException::class)
    fun updateSchedulerJob(job: SysJob, jobGroup: String?) {
        val jobId = job.jobId
        // 判断是否存在
        val jobKey = ScheduleUtils.getJobKey(jobId!!, jobGroup)
        if (scheduler!!.checkExists(jobKey)) {
            // 防止创建时存在数据问题 先移除，然后在执行创建操作
            scheduler.deleteJob(jobKey)
        }
        ScheduleUtils.createScheduleJob(scheduler, job)
    }

    /**
     * 校验cron表达式是否有效
     *
     * @param cronExpression 表达式
     * @return 结果
     */
    override fun checkCronExpressionIsValid(cronExpression: String?): Boolean {
        return CronUtils.isValid(cronExpression)
    }
}
