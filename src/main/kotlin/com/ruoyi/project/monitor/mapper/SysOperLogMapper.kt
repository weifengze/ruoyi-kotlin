package com.ruoyi.project.monitor.mapper

import com.ruoyi.project.monitor.domain.SysOperLog
import org.apache.ibatis.annotations.Mapper

/**
 * 操作日志 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysOperLogMapper {
    /**
     * 新增操作日志
     *
     * @param operLog 操作日志对象
     */
    fun insertOperlog(operLog: SysOperLog?)

    /**
     * 查询系统操作日志集合
     *
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    fun selectOperLogList(operLog: SysOperLog?): List<SysOperLog>?

    /**
     * 批量删除系统操作日志
     *
     * @param operIds 需要删除的操作日志ID
     * @return 结果
     */
    fun deleteOperLogByIds(operIds: Array<Long?>?): Int

    /**
     * 查询操作日志详细
     *
     * @param operId 操作ID
     * @return 操作日志对象
     */
    fun selectOperLogById(operId: Long?): SysOperLog?

    /**
     * 清空操作日志
     */
    fun cleanOperLog()
}
