package com.ruoyi.project.monitor.mapper

import com.ruoyi.project.monitor.domain.SysJobLog
import org.apache.ibatis.annotations.Mapper

/**
 * 调度任务日志信息 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysJobLogMapper {
    /**
     * 获取quartz调度器日志的计划任务
     *
     * @param jobLog 调度日志信息
     * @return 调度任务日志集合
     */
    fun selectJobLogList(jobLog: SysJobLog?): List<SysJobLog>?

    /**
     * 查询所有调度任务日志
     *
     * @return 调度任务日志列表
     */
    fun selectJobLogAll(): List<SysJobLog?>?

    /**
     * 通过调度任务日志ID查询调度信息
     *
     * @param jobLogId 调度任务日志ID
     * @return 调度任务日志对象信息
     */
    fun selectJobLogById(jobLogId: Long?): SysJobLog?

    /**
     * 新增任务日志
     *
     * @param jobLog 调度日志信息
     * @return 结果
     */
    fun insertJobLog(jobLog: SysJobLog?): Int

    /**
     * 批量删除调度日志信息
     *
     * @param logIds 需要删除的数据ID
     * @return 结果
     */
    fun deleteJobLogByIds(logIds: Array<Long?>?): Int

    /**
     * 删除任务日志
     *
     * @param jobId 调度日志ID
     * @return 结果
     */
    fun deleteJobLogById(jobId: Long?): Int

    /**
     * 清空任务日志
     */
    fun cleanJobLog()
}
