package com.ruoyi.project.monitor.mapper

import com.ruoyi.project.monitor.domain.SysJob
import org.apache.ibatis.annotations.Mapper

/**
 * 调度任务信息 数据层
 *
 * @author ruoyi
 */
@Mapper
interface SysJobMapper {
    /**
     * 查询调度任务日志集合
     *
     * @param job 调度信息
     * @return 操作日志集合
     */
    fun selectJobList(job: SysJob?): List<SysJob>

    /**
     * 查询所有调度任务
     *
     * @return 调度任务列表
     */
    fun selectJobAll(): List<SysJob>

    /**
     * 通过调度ID查询调度任务信息
     *
     * @param jobId 调度ID
     * @return 角色对象信息
     */
    fun selectJobById(jobId: Long?): SysJob

    /**
     * 通过调度ID删除调度任务信息
     *
     * @param jobId 调度ID
     * @return 结果
     */
    fun deleteJobById(jobId: Long?): Int

    /**
     * 批量删除调度任务信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    fun deleteJobByIds(ids: Array<Long?>?): Int

    /**
     * 修改调度任务信息
     *
     * @param job 调度任务信息
     * @return 结果
     */
    fun updateJob(job: SysJob?): Int

    /**
     * 新增调度任务信息
     *
     * @param job 调度任务信息
     * @return 结果
     */
    fun insertJob(job: SysJob?): Int
}
