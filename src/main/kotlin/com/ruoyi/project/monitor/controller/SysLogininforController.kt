package com.ruoyi.project.monitor.controller

import com.ruoyi.common.utils.poi.ExcelUtil
import com.ruoyi.framework.aspectj.lang.annotation.Log
import com.ruoyi.framework.aspectj.lang.enums.BusinessType
import com.ruoyi.framework.security.service.SysPasswordService
import com.ruoyi.framework.web.controller.BaseController
import com.ruoyi.framework.web.domain.AjaxResult
import com.ruoyi.framework.web.page.TableDataInfo
import com.ruoyi.project.monitor.domain.SysLogininfor
import com.ruoyi.project.monitor.service.ISysLogininforService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse

/**
 * 系统访问记录
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/monitor/logininfor")
open class SysLogininforController : BaseController() {
    @Autowired
    private val logininforService: ISysLogininforService? = null

    @Autowired
    private val passwordService: SysPasswordService? = null
    @PreAuthorize("@ss.hasPermi('monitor:logininfor:list')")
    @GetMapping("/list")
    fun list(logininfor: SysLogininfor?): TableDataInfo {
        startPage()
        val list = logininforService!!.selectLogininforList(logininfor)
        return getDataTable(list)
    }

    @Log(title = "登录日志", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('monitor:logininfor:export')")
    @PostMapping("/export")
    fun export(response: HttpServletResponse, logininfor: SysLogininfor?) {
        val list = logininforService!!.selectLogininforList(logininfor)
        val util = ExcelUtil(SysLogininfor::class.java)
        util.exportExcel(response, list, "登录日志")
    }

    @PreAuthorize("@ss.hasPermi('monitor:logininfor:remove')")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{infoIds}")
    fun remove(@PathVariable infoIds: Array<Long?>?): AjaxResult {
        return toAjax(logininforService!!.deleteLogininforByIds(infoIds))
    }

    @PreAuthorize("@ss.hasPermi('monitor:logininfor:remove')")
    @Log(title = "登录日志", businessType = BusinessType.CLEAN)
    @DeleteMapping("/clean")
    fun clean(): AjaxResult {
        logininforService!!.cleanLogininfor()
        return success()
    }

    @PreAuthorize("@ss.hasPermi('monitor:logininfor:unlock')")
    @Log(title = "账户解锁", businessType = BusinessType.OTHER)
    @GetMapping("/unlock/{userName}")
    fun unlock(@PathVariable("userName") userName: String): AjaxResult {
        passwordService!!.clearLoginRecordCache(userName)
        return success()
    }
}
