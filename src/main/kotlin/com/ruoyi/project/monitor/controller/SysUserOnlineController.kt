package com.ruoyi.project.monitor.controller

import com.ruoyi.common.constant.CacheConstants
import com.ruoyi.framework.aspectj.lang.annotation.*
import com.ruoyi.framework.aspectj.lang.enums.BusinessType
import com.ruoyi.framework.redis.RedisCache
import com.ruoyi.framework.security.LoginUser
import com.ruoyi.framework.web.controller.BaseController
import com.ruoyi.framework.web.domain.AjaxResult
import com.ruoyi.framework.web.page.TableDataInfo
import com.ruoyi.project.monitor.domain.SysUserOnline
import com.ruoyi.project.system.service.ISysUserOnlineService
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

/**
 * 在线用户监控
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/monitor/online")
open class SysUserOnlineController : BaseController() {
    @Autowired
    private val userOnlineService: ISysUserOnlineService? = null

    @Autowired
    private val redisCache: RedisCache? = null

    @PreAuthorize("@ss.hasPermi('monitor:online:list')")
    @GetMapping("/list")
    fun list(ipaddr: String?, userName: String?): TableDataInfo {
        val keys = redisCache!!.keys(CacheConstants.LOGIN_TOKEN_KEY + "*")
        val userOnlineList: MutableList<SysUserOnline?> = ArrayList()
        for (key in keys) {
            val user: LoginUser = redisCache.getCacheObject(key.toString()) as LoginUser
            when {
                com.ruoyi.common.utils.StringUtils.isNotEmpty(ipaddr) && com.ruoyi.common.utils.StringUtils.isNotEmpty(
                        userName
                ) -> {
                    if (StringUtils.equals(ipaddr, user.ipaddr) && StringUtils.equals(userName, user.username)) {
                        userOnlineList.add(userOnlineService!!.selectOnlineByInfo(ipaddr, userName, user))
                    }
                }
                com.ruoyi.common.utils.StringUtils.isNotEmpty(ipaddr) -> {
                    if (StringUtils.equals(ipaddr, user.ipaddr)) {
                        userOnlineList.add(userOnlineService!!.selectOnlineByIpaddr(ipaddr, user))
                    }
                }
                com.ruoyi.common.utils.StringUtils.isNotEmpty(userName) && com.ruoyi.common.utils.StringUtils.isNotNull(
                        user.user
                ) -> {
                    if (StringUtils.equals(userName, user.username)) {
                        userOnlineList.add(userOnlineService!!.selectOnlineByUserName(userName, user))
                    }
                }
                else -> {
                    userOnlineList.add(userOnlineService!!.loginUserToUserOnline(user))
                }
            }
        }
        Collections.reverse(userOnlineList)
        userOnlineList.removeAll(setOf<Any?>(null))
        return getDataTable(userOnlineList)
    }

    /**
     * 强退用户
     */
    @PreAuthorize("@ss.hasPermi('monitor:online:forceLogout')")
    @Log(title = "在线用户", businessType = BusinessType.FORCE)
    @DeleteMapping("/{tokenId}")
    fun forceLogout(@PathVariable tokenId: String): AjaxResult {
        redisCache!!.deleteObject(CacheConstants.LOGIN_TOKEN_KEY + tokenId)
        return AjaxResult.success()
    }
}
