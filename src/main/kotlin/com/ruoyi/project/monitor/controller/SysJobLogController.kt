package com.ruoyi.project.monitor.controller

import com.ruoyi.common.utils.poi.ExcelUtil
import com.ruoyi.framework.aspectj.lang.annotation.*
import com.ruoyi.framework.aspectj.lang.enums.BusinessType
import com.ruoyi.framework.web.controller.BaseController
import com.ruoyi.framework.web.domain.AjaxResult
import com.ruoyi.framework.web.page.TableDataInfo
import com.ruoyi.project.monitor.domain.SysJobLog
import com.ruoyi.project.monitor.service.ISysJobLogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse

/**
 * 调度日志操作处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/monitor/jobLog")
  open class SysJobLogController : BaseController() {
    @Autowired
    private val jobLogService: ISysJobLogService? = null

    /**
     * 查询定时任务调度日志列表
     */
    @PreAuthorize("@ss.hasPermi('monitor:job:list')")
    @GetMapping("/list")
    fun list(sysJobLog: SysJobLog?): TableDataInfo {
        startPage()
        val list = jobLogService!!.selectJobLogList(sysJobLog)
        return getDataTable(list)
    }

    /**
     * 导出定时任务调度日志列表
     */
    @PreAuthorize("@ss.hasPermi('monitor:job:export')")
    @Log(title = "任务调度日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    fun export(response: HttpServletResponse, sysJobLog: SysJobLog?) {
        val list = jobLogService!!.selectJobLogList(sysJobLog)
        val util = ExcelUtil(SysJobLog::class.java)
        util.exportExcel(response, list, "调度日志")
    }

    /**
     * 根据调度编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('monitor:job:query')")
    @GetMapping(value = ["/{configId}"])
    fun getInfo(@PathVariable jobLogId: Long?): AjaxResult {
        return AjaxResult.success(jobLogService!!.selectJobLogById(jobLogId))
    }

    /**
     * 删除定时任务调度日志
     */
    @PreAuthorize("@ss.hasPermi('monitor:job:remove')")
    @Log(title = "定时任务调度日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{jobLogIds}")
    fun remove(@PathVariable jobLogIds: Array<Long?>?): AjaxResult {
        return toAjax(jobLogService!!.deleteJobLogByIds(jobLogIds))
    }

    /**
     * 清空定时任务调度日志
     */
    @PreAuthorize("@ss.hasPermi('monitor:job:remove')")
    @Log(title = "调度日志", businessType = BusinessType.CLEAN)
    @DeleteMapping("/clean")
    fun clean(): AjaxResult {
        jobLogService!!.cleanJobLog()
        return AjaxResult.Companion.success()
    }
}
