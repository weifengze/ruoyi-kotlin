package com.ruoyi.project.monitor.controller

import com.ruoyi.common.utils.poi.ExcelUtil
import com.ruoyi.framework.aspectj.lang.annotation.*
import com.ruoyi.framework.aspectj.lang.enums.BusinessType
import com.ruoyi.framework.web.controller.BaseController
import com.ruoyi.framework.web.domain.AjaxResult
import com.ruoyi.framework.web.page.TableDataInfo
import com.ruoyi.project.monitor.domain.SysOperLog
import com.ruoyi.project.monitor.service.ISysOperLogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse

/**
 * 操作日志记录
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/monitor/operlog")
open class SysOperlogController : BaseController() {
    @Autowired
    private val operLogService: ISysOperLogService? = null
    @PreAuthorize("@ss.hasPermi('monitor:operlog:list')")
    @GetMapping("/list")
    fun list(operLog: SysOperLog?): TableDataInfo {
        startPage()
        val list = operLogService!!.selectOperLogList(operLog)
        return getDataTable(list)
    }

    @Log(title = "操作日志", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('monitor:operlog:export')")
    @PostMapping("/export")
    fun export(response: HttpServletResponse, operLog: SysOperLog?) {
        val list = operLogService!!.selectOperLogList(operLog)
        val util = ExcelUtil(SysOperLog::class.java)
        util.exportExcel(response, list, "操作日志")
    }

    @Log(title = "操作日志", businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
    @DeleteMapping("/{operIds}")
    fun remove(@PathVariable operIds: Array<Long?>?): AjaxResult {
        return toAjax(operLogService!!.deleteOperLogByIds(operIds))
    }

    @Log(title = "操作日志", businessType = BusinessType.CLEAN)
    @PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
    @DeleteMapping("/clean")
    fun clean(): AjaxResult {
        operLogService!!.cleanOperLog()
        return AjaxResult.Companion.success()
    }
}
